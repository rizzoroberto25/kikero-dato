<?php
date_default_timezone_set("Europe/Berlin");
header('Content-type: text/html; charset=UTF-8');

define("ROOT", str_replace("\\", "/",  __DIR__ ). "/");
if(!isset($isDatoCron)) {
	$isDatoCron = false;
}

$path = str_replace( $_SERVER['DOCUMENT_ROOT'], "", ROOT);
define("PATH", $path);
/*
$dominio=str_replace("http://", "", $_SERVER['HTTP_HOST']);
$dominio=str_replace("https://", "", $_SERVER['HTTP_HOST']);
$dominio=str_replace("www.", "", $_SERVER['HTTP_HOST']);
*/
$dominio = $_SERVER['HTTP_HOST'];
define("DOMINIO", $dominio);

include(__DIR__."/env/Vars.php");
define("SLUG_PANEL", $SlugPanel);
$Recaptcha['pk']="";
$Recaptcha['sk']="";

if(!$isDatoCron) session_start();

define("TEMPDIR", ROOT."temp_foto/");

define("REG_EXP_PASSWORD", "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[_£%!#@?])([a-zA-Z0-9_£%!#@?]{8,15})$/");
define("UPLOAD_MAX_FILESIZE", preg_filter('/[^0-9]/','',ini_get("upload_max_filesize")));

define("TEXT_REPLACE", "%%||-|%%");
define("LINGUA_CSV_FILE_PATH", ROOT."docs/csv/lingue-sigle.csv");

include_once(ROOT."Core/init.php");
include_once(ROOT."Core/Classi/Render.php");
