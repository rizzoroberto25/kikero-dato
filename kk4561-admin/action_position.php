<?php
include_once("HeadersPhp.php");
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;

if(!isset($_GET['controller']) || !isset($_GET['id']) || !isset($_GET['bond_position']) || !isset($_GET['position_value'])) { die("Campi mancanti");
    header("Location: ".$_SERVER['HTTP_REFERER']."?action_not_forbidden=1&message=required_fields_empty");
    exit();
}

$controller = (string) $_GET['controller'];
$id = (int) $_GET['id'];
$position_value = (int) $_GET['position_value'];

$tokenFirstVerb = $controller;
include_once("action_checks.php");
$ruoloActions = "admin,editor";

if($Action == "set_position") {
    if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$_SERVER['HTTP_REFERER']."?action_not_forbidden=1");
	}
    $controller_name = "Core\\Classi\\Controllers\\".strtoupper($controller)."_CONTROLLER";
    $Controller = new $controller_name($id);
    //$Controller = new SHOP_CONTROLLER($id);
    if($post['bond_position'] !== 0) {
        $Controller->Model->bondPosition = $post['bond_position'];
        $Controller->Model->bondPositionValue = $post['bond_position_value'];
    }
    $Controller->Model->setPosition($position_value);
    header("Location: ".$_SERVER['HTTP_REFERER']);
    exit();
}