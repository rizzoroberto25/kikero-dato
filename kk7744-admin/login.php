<?php
include("../config.php");
include_once("check-logged.php");
if($UtenteSession->ctl_ruolo(RUOLI_DEFAULT)) {
	
	header("Location:".PATH.$SlugPanel);
}

use Core\Classi\UTILITY;
use Core\Classi\LINGUA;
if(!empty($_POST['action']) && $_POST['action'] == "login") {
	
	$_SESSION['errors'] = NULL;
	if(!UTILITY::hasValue($_POST['username']) || !UTILITY::hasValue($_POST['password'])) {
		$_SESSION['errors'] = array("required_fields");
		header("Location: ".PATH.$SlugPanel."/login.php");
		exit();
	}
	
	$login = $UtenteSession->login($_POST['username'], $_POST['password']);
	
	switch($login) {
		case "dato_errato_nick":
			$_SESSION['errors'] = array("check_failed");
			//header("Location: ".PATH."dato-admin/login.php");
			break;
			
		case "dato_errato_password":
			$_SESSION['errors'] = array("check_failed");
						
			//header("Location: ".PATH."dato-admin/login.php");
			break;
			
		case "failed":
			
			$_SESSION['errors'] = array("check_failed");
			//header("Location: ".PATH."dato-admin/login.php");
			break;
			
		case "OK":
			if(!$UtenteSession->ctl_ruolo(RUOLI_DEFAULT)) {
				$_SESSION['errors'] = array("ruolo_utente_non_idoneo");
				
			} else {
				unset($_SESSION['errors']);
				header("Location: ".PATH.$SlugPanel."/");
			}
			break;
			
		default:
			$_SESSION['errors'] = array("check_failed");
			//header("Location: ".PATH."dato-admin/login.php");
			break;
	}

}
$Lingua = new LINGUA($_SESSION['lingua']);
$Lingua->loadTranslations("admin", "admin"); 
//UTILITY::codePrint($Lingua, "Oggetto lingua"); die("Per Viola");
include(ROOT.$SlugPanel."/contents/login.php");
?>

