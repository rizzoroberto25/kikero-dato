<?php
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;

if(strpos($_SERVER['HTTP_REFERER'], DOMINIO) === false) {
	header("Location: ".PATH);
}

if(!isset($_POST['action']) && !isset($_GET['action'])) {
	header("Location: ".PATH);
}

if(isset($_GET['action'])) {
	$Action = $_GET['action'];
	$post = $_GET;
} else {
	$Action = $_POST['action'];
	$post = $_POST;
}
$tokenFirstVerb = !empty($tokenFirstVerb) ? $tokenFirstVerb : "";
$checkVerbsToken = array($tokenFirstVerb, ".", $post['action']);

if(!UTILITY::checkRequestFromMyDomain()) header("Location: ".PATH);

if(!UTILITY::checkFormToken($checkVerbsToken, $post['token'])) {
	//die("Controllo token fallito");
	header("Location: ".PATH);
	exit();
}


