<?php
include_once("../HeadersPhp.php");
use Core\Classi\UTILITY;
use Core\Classi\Controllers\EVENTO_CONTROLLER;
include_once(ROOT."Core/Classi/Admin/Pagination_Html.php");
use Core\Classi\Admin\PAGINATION_HTML;
use Core\Classi\Models\TESTO;
use Core\Classi\Controllers\IMG;
include_once(ROOT."Core/Classi/Admin/FixedButtonsController.php");
use Core\Classi\Admin\FIXED_BUTTONS_CONTROLLER as FixedButtons;
use Core\Classi\CRYPTO;

if(isset($_GET['remove_filters'])) {
	unset($_SESSION['event']['filters']);
}

$EventoController = new EVENTO_CONTROLLER();

$page = 1;
if(isset($_GET['page'])) {
	$page = (int) $_GET['page'];
}
$limit = 10;
if(!empty($EventoController->getSession('event.limit'))) {
	$limit = $EventoController->getSession('event.limit');
}
if(isset($_GET['limit'])) {
	$limit = (int) $_GET['limit'];
}
$EventoController->setSession(['event'=>['limit'=>$limit]]);

$EventoController->setPaginationRules($limit, $page);

$filters_session = $EventoController->getSession("event.filters");
$filters = NULL;
if(!empty($filters_session)) { 
	if(!empty($filters_session['titolo'])) {
		$filters[] = "LOWER(et.testo) LIKE(\"%".strtolower($filters_session['titolo'])."%\")";
	}
	if(!empty($filters_session['sottotitolo'])) {
		$filters[] = "e.id IN(SELECT e.id as id FROM eventi as e JOIN eventi_testi as et ON e.sottotitolo=et.id WHERE LOWER(et.testo) LIKE(\"%".strtolower($filters_session['sottotitolo'])."%\"))";
	}
	if(!empty($filters_session['descrizione'])) {
		$filters[] = "e.id IN(SELECT e.id as id FROM eventi as e JOIN eventi_testi as et ON e.testo=et.id WHERE LOWER(et.testo) LIKE(\"%".strtolower($filters_session['descrizione'])."%\"))";
	}
	if(!empty($filters_session['location'])) {
		$filters[] = "LOWER(e.location) LIKE(\"%".strtolower($filters_session['location'])."%\")";
	}
}
$Eventi = $EventoController->Model->listaEventi($filters, LINGUA_PR);

$paginazione = $EventoController->getPagination();

$Paginazione_html = NULL;
if(!empty($paginazione) && is_array($paginazione)) {

	$pagination_rules = array(
		'page' => $page,
		'totPages' => $paginazione['tot_pagine'],
		'currentUrl' => $PathPanel."event/lista.php",
		'hasHtaccess' => false,
	);
	$Paginazione = new PAGINATION_HTML($pagination_rules);
	$Paginazione_html = $Paginazione->getHtml();
}

if(is_array($Eventi) && count($Eventi) > 0) {
	$Testo = new TESTO("eventi", LINGUA_PR);
	foreach($Eventi as $k=>$evento) {
		if(!empty($evento['sottotitolo']) && is_numeric($evento['sottotitolo']) && $evento['sottotitolo']>0) {
			$Eventi[$k]['sottotitolo_testo'] = $Testo->getTesto($evento['sottotitolo']);
		}

		$evento['thumb'] = PATH."img/kikero/bw-logo2.png";
		if($evento['img']>0) {
			$Img = new IMG($evento['img']);
			$thumb = $Img->getImage();
			if(!is_null($thumb) && file_exists(ROOT_UPLOAD."img/300/".$thumb)) {
				$evento['thumb'] = PATH_UPLOAD."img/300/".$thumb;
			}
		}
		$Eventi[$k]['thumb'] = $evento['thumb'];
	}
}

$FixedButtons = new FixedButtons();
if(!empty($EventoController->getSession("event.filters"))) {
	$FixedButtons->addRemoveFiltersButton($PathPanel."event/lista");
}
$FixedButtons->addSearchButton(['url'=>'contents/event/filters.php', 'title'=>'Cerca tra gli eventi', ]);
$FixedButtons->addPlusButton($PathPanel."event/create.php?page=".$page);
$htmlFixedButtons = $FixedButtons->getHtmlButtons();

$tokenStringa = "event.delete";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$tokenDelete = $Crypto->getToken();

$tokenStringa = "event.publish";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$tokenPublish = $Crypto->getToken();

$tokenStringa = "Evento.set_position";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$tokenPosition = $Crypto->getToken();

$CP = "event/lista";
$pageTitle = "Lista Eventi";

include_once($RootPanel."template.php");