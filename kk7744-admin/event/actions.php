<?php
include_once("../HeadersPhp.php");
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;
use Core\Classi\Controllers\EVENTO_CONTROLLER;

$tokenFirstVerb = "event";
include_once("../action_checks.php");
$ruoloActions = "admin,editor";

if($Action == "store") {
	
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$PathPanel."users/create.php"."?action_not_forbidden=1");
	}

    //UTILITY::codePrint($post, "POST"); die();
	$EventoController = new EVENTO_CONTROLLER();

	$files = NULL;
	if(is_uploaded_file($_FILES['event_image']['tmp_name'])) {
		$files = $_FILES;
	}
	$EventoController->storeEvento($post, $files);
	$Response = $EventoController->getResponse();
	$_SESSION['Response'] = $Response;
	
	if(!$Response['result']) {
		header("Location:".$PathPanel."event/create.php?show_response=1");
	}
	
	header("Location:".$PathPanel."event/edit.php?id=".$Response['data']->id."&show_response=1");

}

if($Action == "update") {
	
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$_SERVER['HTTP_REFERER']."?action_not_forbidden=1");
	}
	if(!isset($post['evento_id'])) {
		header("Location: ".$_SERVER['HTTP_REFERER']."?action_not_forbidden=1");
	}
	
	$event_id = (int) $post['evento_id'];
	$EventoController = new EVENTO_CONTROLLER($event_id);

	$files = NULL;
	if(is_uploaded_file($_FILES['event_image']['tmp_name'])) {
		$files = $_FILES;
	}

	$EventoController->updateEvento($post, $files);
	$Response = $EventoController->getResponse();
	$_SESSION['Response'] = $Response;
	//UTILITY::codePrint($Response, "store response"); die();
	if(!$Response['result']) {
		header("Location:".$PathPanel."event/edit.php?id=".$event_id."&show_response=1");
	}
	
	header("Location:".$PathPanel."event/edit.php?id=".$event_id."&show_response=1");

}

if($Action == "filter_list") {
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$PathPanel."users/create.php"."?action_not_forbidden=1");
	}
	
	unset($post['action']);
	unset($post['token']);

	if(isset($_SESSION['event']['filters'])) {
		unset($_SESSION['event']['filters']);
	}

	$EventoController = new EVENTO_CONTROLLER();
	$EventoController->setSession(["event"=>["filters"=>$post]]);
	//$_SESSION['event']['filters'] = $post;
	header("Location:".$PathPanel."event/lista.php");
}

if($Action == "delete") {
	
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$_SERVER['HTTP_REFERER']."?action_not_forbidden=1");
	}
	if(!isset($post['id'])) {
		header("Location: ".$_SERVER['HTTP_REFERER']."?action_not_forbidden=1");
	}
	
	$event_id = (int) $post['id'];
	$EventoController = new EVENTO_CONTROLLER($event_id);

	$EventoController->deleteEvento();
	$Response = $EventoController->getResponse();
	$_SESSION['Response'] = $Response;

	header("Location:".$PathPanel."event/lista.php?page=".$post['page']."&show_response=1");

}

if($Action == "publish") {
	
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$_SERVER['HTTP_REFERER']."?action_not_forbidden=1");
	}
	if(!isset($post['id'])) {
		header("Location: ".$_SERVER['HTTP_REFERER']."?action_not_forbidden=1");
	}
	$event_id = (int) $post['id'];
	$value = (int) $post['value'];
	$EventoController = new EVENTO_CONTROLLER($event_id);
	$EventoController->publish_record($value);
	die(json_encode($EventoController->getResponse()));
}

