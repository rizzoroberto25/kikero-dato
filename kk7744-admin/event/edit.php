<?php
include_once("../HeadersPhp.php");
include_once(ROOT."Core/Classi/Admin/TestoController.php");
include_once(ROOT."Core/Classi/Admin/InputController.php");
include_once(ROOT."Core/Classi/Admin/ImageController.php");
include_once(ROOT."Core/Classi/Admin/GeoController.php");

use Core\Classi\CRYPTO;
use Core\Classi\UTILITY;
use Core\Classi\Controllers\EVENTO_CONTROLLER;
use Core\Classi\Models\TESTO;
use Core\Classi\Controllers\IMG as ImgController;

if(empty($_GET['id'])) {
	exit();
}

$id = (int) $_GET['id'];
$page = 1;
if(isset($_GET['page'])) {
	$page = (int) $_GET['page'];
}

$tokenStringa = "event.update";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$tokenForm = $Crypto->getToken();

$EventoController = new EVENTO_CONTROLLER($id);
$Testo = new TESTO("eventi");
$getTesto = $Testo->getTextes($EventoController->Model->titolo);
$EventoTitolo = ($getTesto!==false) ? $getTesto : "";
$getTesto = $Testo->getTextes($EventoController->Model->sottotitolo);
$EventoSottotitolo = ($getTesto!==false) ? $getTesto : "";
$getTesto = $Testo->getTextes($EventoController->Model->testo);
$EventoTesto = ($getTesto!==false) ? $getTesto : "";
$EventoImg = NULL;
if(!empty($EventoController->Model->img)) {
	$ImgController = new ImgController($EventoController->Model->img);
	$Img = $ImgController->getTheImage("600");
	if(!is_null($Img)) {
		$EventoImg = $Img;
	}
}

$EventoLocation = NULL;
if(!empty($EventoController->Model->geo) && UTILITY::isValidJson($EventoController->Model->geo)) {
	$EventoLocation = json_decode($EventoController->Model->geo);
}

//Date evento
$date_from = !empty($EventoController->Model->date_from)?str_replace(" ", "T", $EventoController->Model->date_from):"";
$date_to = !empty($EventoController->Model->date_to)?str_replace(" ", "T", $EventoController->Model->date_to):"";

$CP = "event/edit";
$pageTitle = "Modifica Evento";

$Evento = $EventoController->Model;

$tokenStringa = "media.gallery";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$tokenGallery = $Crypto->getToken();

$ImgController = new ImgController();
$Gallery = $ImgController->getModelGallery("eventi", $id);

include_once($RootPanel."template.php");