<?php
if(!defined("ROOT")) header("Location:login.php");

?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DATO Panel :: login</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="<?php echo PATH; ?>img/kikero/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="<?php echo PATH; ?>img/kikero/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="<?php echo PATH; ?>img/kikero/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?php echo PATH; ?>img/kikero/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?php echo PATH; ?>img/kikero/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?php echo PATH; ?>img/kikero/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="<?php echo PATH; ?>img/kikero/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="<?php echo PATH; ?>img/kikero/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="<?php echo PATH; ?>img/kikero/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="<?php echo PATH; ?>img/kikero/favicon/mstile-310x310.png" />


    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/selectFX/css/cs-skin-elastic.css">

    <link rel="stylesheet" href="<?php echo $PathPanel; ?>assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body class="bg-dark">


    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="index.html">
                        <img class="align-content" src="<?php echo PATH; ?>img/kikero/kikero-colored.png" alt="">
                    </a>
                </div>
                <div class="login-form">
				<?php
				if(!empty($_SESSION['errors']) && is_array(($_SESSION['errors']))) {
					foreach($_SESSION['errors'] as $error) {
						echo "<div class=\"alert alert-danger\" role=\"alert\">
								".$Lingua->Translations['admin']->login->{$error}."
							</div>";
					}
				}
				?>
                    <form id="f_login" action="login.php" method="post">
						<input type="hidden" name="action" value="login">
                        <div class="form-group">
                            <label>Email address</label>
                            <input type="email" class="form-control" name="username" placeholder="Email">
                        </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                                <div class="checkbox">
                                    <label>
                                <input type="checkbox"> Remember Me
                            </label>
                                    <label class="pull-right">
                                <a href="<?php echo $PathPanel; ?>recover-password.php">Forgotten Password?</a>
                            </label>

                                </div>
                                <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
                                <!--
                                <div class="social-login-content">
                                    <div class="social-button">
                                        <button type="button" class="btn social facebook btn-flat btn-addon mb-3"><i class="ti-facebook"></i>Sign in with facebook</button>
                                        <button type="button" class="btn social twitter btn-flat btn-addon mt-2"><i class="ti-twitter"></i>Sign in with twitter</button>
                                    </div>
                                </div>
                                -->
                                <div class="register-link m-t-15 text-center">
                                    <p>Don't have account ? <a href="#"> Sign Up Here</a></p>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script src="<?php echo $PathPanel; ?>vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo $PathPanel; ?>vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo $PathPanel; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo $PathPanel; ?>assets/js/main.js"></script>


</body>

</html>