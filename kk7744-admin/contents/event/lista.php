<?php
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;

if(!empty($_GET['show_response']) && $_GET['show_response'] == 1 && !empty($_SESSION['Response'])) {
	$responseType = "danger";
	if($_SESSION['Response']['result']) {
		$responseType = "success";
		$responseMessage = "Operazione riuscita";
	} else {
		$responseMessage = $Lingua->Translations['admin']->event->errors->{$_SESSION['Response']['error']};
        if(empty($responseMessage)) {
            $responseMessage = "Errore non definito.";
        }
	}
?>
				<div class="row">
					<div class="col-12">
						<div class="sufee-alert alert with-close alert-<?php echo $responseType; ?> alert-dismissible fade show" role="alert">
							<?php echo $responseMessage; ?>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
						</div>
					</div>
				</div>
<?php	
}

if(!empty($Eventi) && is_array($Eventi) && count($Eventi)>0) {
?>
        
        <div class="row">

            <div class="col-md-12">
                <div class="card loader-box">
                    <!--
                    <div class="card-header">
                        <strong class="card-title">Data Table</strong>
                    </div>
                    -->
                    <div class="card-body">
                    
                        <div class="row">
                        
                            <div class="col-sm-12 col-md-6">
                            
                            </div>
                        
                            <div class="col-sm-12 col-md-6">
                                
                            </div>
                            
                        </div>
                        
                        <div class="row">
                    
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Evento ID</th>
                                    <th>Foto</th>
                                    <th>Titolo Evento</th>
                                    <th>Luogo</th>
                                    <th>Posizione</th>
                                    <th>Pubblicato</th>
                                    <th>Azioni</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
foreach($Eventi as $evento) {
?>
                                <tr>
                                    <td><?php echo $evento['id'];?>
                                    <td><img class="align-self-center list-thumb rounded-foglia mr-3" style="width:50px; height:50px; object-fit:cover;" src="<?php echo $evento['thumb']; ?>" /></td>
                                    <td><h4><?php echo $evento['titolo'];?></h4><?php echo $evento['sottotitolo_testo'];?></td>
                                    <td><?php echo $evento['location'];?></td>

                                    <td class="position-manage" data-controller='Evento' data-token='<?php echo $tokenPosition; ?>' data-id='<?php echo $evento['id']; ?>' data-bond_position=''>
                                        <?php echo $evento['position']; ?>
                                        <input type="number" name="position" style="width:80px;" min="1" max="<?php echo count($Eventi); ?>" />
                                        <button class="btn btn-warning btn-sm" onclick="set_position(this);" style="margin-top:-4px;">Sposta</button> 
                                    </td>

                                    <td>
										<label class="switch switch-text switch-success">
                                        <input type="checkbox" name="published" value="1" class="switch-input page-loader" <?php if($evento['published']) echo "checked=\"true\""; ?> onclick="switch_action(this);" data-action="publish" data-token="<?php echo $tokenPublish;?>" data-parameters='{"id":"<?php echo $evento['id']; ?>","page":"<?php echo $page;?>"}' data-url="<?php echo $PathPanel; ?>event/actions.php"> 
                                        <span data-on="On" data-off="Off" class="switch-label"></span> <span class="switch-handle"></span></label>
                                    </td>
                                    <td>
                                        <a href="<?php echo $PathPanel; ?>event/edit.php?id=<?php echo $evento['id']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i> Modifica</a> 
                                        <a href="javascript:void(0)" class="btn btn-danger" onclick="confirmAction(this);" data-action="delete" data-token="<?php echo $tokenDelete; ?>" data-url="<?php echo $PathPanel; ?>event/actions.php" data-parameters='{"id":"<?php echo $evento['id']; ?>","page":"<?php echo $page;?>"}'><i class="fa fa-trash"></i> Elimina</a>
                                    </td>
                                </tr>
<?php 
}
?>


                            </tbody>

                        </table>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-5"></div>
                            
                            <div class="col-sm-12 col-md-7">
                            <?php
                            if(!is_null($Paginazione_html)) {
                                print($Paginazione_html);
                            }
                            ?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

<?php
} else {
    if($page>1) {
        $prevPage = $page-1;
?>
<script>
location.href = "<?php echo $PathPanel."event/lista.php?page=".$prevPage;?>";
</script>
<?php
    } else {
?>
<h3>Non ci sono eventi in database</h3>
<?php
    }
}
?>
<?php
echo $htmlFixedButtons;
?>