<?php
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;
use Core\Classi\Admin\TESTO_CONTROLLER;
use Core\Classi\Admin\INPUT_CONTROLLER;
use Core\Classi\Admin\IMAGE_CONTROLLER;
use Core\Classi\Admin\GEO_CONTROLLER;

if(!empty($_GET['show_response']) && $_GET['show_response'] == 1 && !empty($_SESSION['Response'])) {
	$responseType = "danger";
	if($_SESSION['Response']['result']) {
		$responseType = "success";
		$responseMessage = "Nuovo evento creato";
	} else {
		$responseMessage = $Lingua->Translations['admin']->event->errors->{$_SESSION['Response']['error']};
	}
?>
				<div class="row">
					<div class="col-12">
						<div class="sufee-alert alert with-close alert-<?php echo $responseType; ?> alert-dismissible fade show" role="alert">
							<?php echo $responseMessage; ?>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
						</div>
					</div>
				</div>
<?php	
}

$flashPost = NULL;
if(isset($_SESSION['flash']['post'])) {
	$flashPost = $_SESSION['flash']['post'];
}
?>
<script type="text/javascript" src="<?php echo $PathPanel; ?>vendors/ckeditor/ckeditor.js"></script>

                <div class="row">
					<form name="f_event" class="parent-form kk-validation" method="post" action="<?php echo $PathPanel;?>event/actions.php" enctype="multipart/form-data">
					<input type="hidden" name="action" value="store" />
					<input type="hidden" name="token" value="<?php echo $tokenForm; ?>" />
                    <div class="col-12">
					
						<div class="card">
                            <div class="card-header">
                                <strong>Crea nuovo Evento</strong>
                            </div>

                            <div class="card-body card-block">

								<div class="row">

									<div class="col-12 col-md-8">

	<?php
	$TestoController = new TESTO_CONTROLLER($SiteLanguages);
	$testoObjects = [
		["field"=>"titolo", "label"=>"Titolo evento", "type" =>"text-input", "required" => true, "reg_exp" => false ],
		["field"=>"sottotitolo", "label"=>"Sottotitolo evento", "type" =>"text-input", "required" => false, "reg_exp" => false ],
		["field"=>"descrizione", "label"=>"Descrizione", "type" =>"cke", "required" => false, "reg_exp" => false ],
	];
	$TestoController->printTestiManager("testi", $testoObjects);
	?>
									</div>

									<div class="col-12 col-md-4">
	<?php
	$ImageController = new IMAGE_CONTROLLER();
	$ImageController->printImageUploader("event_image", "Inserisci immagine principale");
	?>

	<?php
	$InputController = new INPUT_CONTROLLER();
	$date_attrs = [
		"type"=>"datetime-local",
	];
	?>

										<div class="col-12">
											<h4 style="padding-bottom:12px;">Date dell'evento</h4>
											<div class="form-group">
											<label>A partire da</label>
											<?php $InputController->printInput("date_from", [], $date_attrs); ?>
											</div>

											<div class="form-group">
											<label>Fino a</label>
											<?php $InputController->printInput("date_to", [], $date_attrs); ?>
											</div>
										</div>

										
																			

									</div>
								</div>

								<div class="row">
									<div class="col-12 col-md-6" style="padding-top:40px;">
										<h4 style="padding-bottom:12px;">Indica la location</h4>
										
<?php
$GeoController = new GEO_CONTROLLER();
$GeoController->printGeolocationForm();
?>

									</div>

									<div class="col-0 col-md-2"></div>

									<div class="col-12 col-md-4" style="padding-top:40px;">
										<h4 style="padding-bottom:12px;">Link</h4>
										<div class="form-group">
											<label>Il link dell'evento</label><br />
											<?php $InputController->printInput("link"); ?>
										</div>
									</div>

								</div>

                            </div>

                            <div class="card-footer">
							<!--<button type="button" class="btn btn-primary btn-sm validate_form_button">-->
							<button type="button" class="btn btn-success btn-sm validate_form_button">
								<i class="fa fa-floppy-o"></i> Salva l'evento
							</button>
							</div>

                        </div>

                    </div>
                </div>