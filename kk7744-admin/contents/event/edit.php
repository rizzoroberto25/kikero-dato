<?php
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;
use Core\Classi\Admin\TESTO_CONTROLLER;
use Core\Classi\Admin\INPUT_CONTROLLER;
use Core\Classi\Admin\IMAGE_CONTROLLER;
use Core\Classi\Admin\GEO_CONTROLLER;

$flashPost = NULL;
if(isset($_SESSION['flash']['post'])) {
	$flashPost = $_SESSION['flash']['post'];
}
?>
<script type="text/javascript" src="<?php echo $PathPanel; ?>vendors/ckeditor/ckeditor.js"></script>
				<form name="f_event" class="parent-form kk-validation" method="post" action="<?php echo $PathPanel;?>event/actions.php" enctype="multipart/form-data">
					<input type="hidden" name="action" value="update" />
                    <input type="hidden" name="evento_id" value="<?php echo $id; ?>" />
					<input type="hidden" name="token" value="<?php echo $tokenForm; ?>" />
                <div class="row">
					
                    <div class="col-12">
					
						<div class="card">
                            <div class="card-header">
                                <strong>Modifica questo evento</strong>
                            </div>

                            <div class="card-body card-block">

								<div class="row">

									<div class="col-12 col-md-8">

	<?php
	$TestoController = new TESTO_CONTROLLER($SiteLanguages);
	$testoObjects = [
		["field"=>"titolo", "label"=>"Titolo evento", "type" =>"text-input", "required" => true, "reg_exp" => false, "values"=>$EventoTitolo, ],
		["field"=>"sottotitolo", "label"=>"Sottotitolo evento", "type" =>"text-input", "required" => false, "reg_exp" => false, "values"=>$EventoSottotitolo, ],
		["field"=>"testo", "label"=>"Descrizione", "type" =>"cke", "required" => false, "reg_exp" => false, "values"=>$EventoTesto, ],
	];
	$TestoController->printTestiManager("testi", $testoObjects);
	?>
									</div>

									<div class="col-12 col-md-4">

									<div class="form-group">
										<div class="col-md-12">
											<label for="select" class=" form-control-label">Pubblicato</label>
											<br />
											<label class="switch switch-text switch-success"><input type="checkbox" name="published" value="1" class="switch-input" <?php if($Evento->published) echo "checked=\"true\""; ?>> <span data-on="On" data-off="Off" class="switch-label"></span> <span class="switch-handle"></span></label>
										</div>
									</div>
	<?php
	$image_preview = !is_null($EventoImg) ? PATH.$EventoImg : NULL;
	$ImageController = new IMAGE_CONTROLLER($image_preview);
	$ImageController->printImageUploader("event_image", "Inserisci immagine principale");
	?>

	<?php
	$InputController = new INPUT_CONTROLLER();
	$date_attrs = [
		"type"=>"datetime-local",
		"value" => $date_from,
	];
	?>

										<div class="col-12" style="padding-top:40px;">
											<h4 style="padding-bottom:12px;">Date dell'evento</h4>
											<div class="form-group">
											<label>A partire da</label>
											<?php $InputController->printInput("date_from", [], $date_attrs); ?>
											</div>
	<?php
	$date_attrs['value'] = $date_to;
	?>
											<div class="form-group">
											<label>Fino a</label>
											<?php $InputController->printInput("date_to", [], $date_attrs); ?>
											</div>
										</div>
																			

									</div>

								</div>

								<div class="row">
									<div class="col-12 col-md-6" style="padding-top:40px;">
										<h4 style="padding-bottom:12px;">Indica la location</h4>
										
<?php
$GeoController = new GEO_CONTROLLER();
$GeoController->printGeolocationForm($EventoLocation);
?>

									</div>
	
									<div class="col-0 col-md-2"></div>

									<div class="col-12 col-md-4" style="padding-top:40px;">
										<h4 style="padding-bottom:12px;">Link</h4>
										<div class="form-group">
											<label>Il link dell'evento</label><br />
											<?php $InputController->printInput("link",[],["value"=>$Evento->link]); ?>
										</div>
									</div>

								</div>

                            </div>

                            <div class="card-footer">
							<!--<button type="button" class="btn btn-primary btn-sm validate_form_button">-->
							<button type="button" class="btn btn-success btn-sm validate_form_button page-loader">
								<i class="fa fa-floppy-o"></i> Salva modifiche all'evento
							</button>
						</div>

                        </div>

                    </div>
					
                </div>
				</form>


				<div class="row gallery-row-parent" style="margin-bottom:100px;">
					<input type="hidden" name="token-gallery" value="<?php echo $tokenGalleryPosition; ?>" />
<?php
$ImageController->print_gallery_controller("eventi", $id, $tokenGallery, $Gallery);
?>	
				</div>