<?php
include_once("../../HeadersPhp.php");
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;
use Core\Classi\CRYPTO;
$tokenStringa = "event.filter_list";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$token = $Crypto->getToken();
?>

<form name="f_filter_list" method="post" action="<?php echo $PathPanel; ?>event/actions.php">
    <input type="hidden" name="action" value="filter_list" />
    <input type="hidden" name="token" value="<?php echo $token; ?>" />

    <div class="row">
        <div class="form-group col-md-12">
            <label>Titolo</label><br />
            <input type="text" name="titolo" class="form-control" />
        </div>

        <div class="form-group col-md-12">
            <label>Sottotitolo</label><br />
            <input type="text" name="sottotitolo" class="form-control" />
        </div>

        <div class="form-group col-md-12">
            <label>Descrizione</label><br />
            <input type="text" name="descrizione" class="form-control" />
        </div>

        <div class="form-group col-md-12">
            <label>Location</label><br />
            <input type="text" name="location" class="form-control" />
        </div>

        <div class="form-group col-12">
            <button type="submit" class="btn btn-success page-loader">
                <i class="fa fa-search"></i> Cerca
            </button>
        </div>
    </div>


</form>


