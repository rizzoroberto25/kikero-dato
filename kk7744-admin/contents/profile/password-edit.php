<?php
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;

if(!empty($_GET['show_response']) && $_GET['show_response'] == 1 && !empty($_SESSION['Response'])) {
	$responseType = "danger";
	if($_SESSION['Response']['result']) {
		$responseType = "success";
		$responseMessage = "Password salvata con successo";
	} else {
		$responseMessage = $Lingua->Translations['admin']->profile->errors->{$_SESSION['Response']['error']};
	}
?>
				<div class="row">
					<div class="col-12">
						<div class="sufee-alert alert with-close alert-<?php echo $responseType; ?> alert-dismissible fade show" role="alert">
							<?php echo $responseMessage; ?>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
						</div>
					</div>
				</div>
<?php	
}
?>

				<div class="row">
					<form name="f_profile" class="parent-form kk-validation" method="post" action="<?php echo $PathPanel;?>profile/actions.php" enctype="multipart/form-data">
					<input type="hidden" name="action" value="update_password" />
					<input type="hidden" name="token" value="<?php echo $tokenForm; ?>" />
                    <div class="col-12 col-md-6">
					
						<div class="card">
                            <div class="card-header">
                                <strong>Crea una nuova password</strong>
							</div>

							<div class="card-body card-block">

								<div class="row">
									<div class="form-group col-12">
										
										<label class="form-control-label">Vecchia password</label>
										<div class="input-group">
											<div class="input-group-addon"><i class="fa fa-key"></i></div>
											<input type="password" class="form-control required" name="old_password" />
										</div>
										<!--<small class="form-text text-muted">ex. 999-99-9999</small>-->
										<div class="error-message alert alert-danger danger"></div>
									</div>

								</div>

								<div class="row">
									<div class="form-group col-12">
										
										<label class="form-control-label">Nuova password</label>
										<div class="input-group">
											<div class="input-group-addon"><i class="fa fa-key"></i></div>
											<input type="password" class="form-control required regexp_val" data-regexp="password" name="new_password" />
										</div>
										<!--<small class="form-text text-muted">ex. 999-99-9999</small>-->
										<div class="error-message alert alert-danger danger"></div>
									</div>

								</div>

								<div class="row">
									<div class="form-group col-12">
										
										<label class="form-control-label">Ripeti password</label>
										<div class="input-group">
											<div class="input-group-addon"><i class="fa fa-key"></i></div>
											<input type="password" class="form-control required" name="rip_password" />
										</div>
										<!--<small class="form-text text-muted">ex. 999-99-9999</small>-->
										<div class="error-message alert alert-danger danger"></div>
									</div>

								</div>

							</div>

							<div class="card-footer">
								<!--<button type="button" class="btn btn-primary btn-sm validate_form_button">-->
								<button type="button" class="btn btn-success btn-sm validate_newpassword_button">
									<i class="fa fa-floppy-o"></i> Salva la Password
								</button>
							</div>
							
						</div>
					</div>

				</div>