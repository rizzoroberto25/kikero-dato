<?php
include_once("../../HeadersPhp.php");
if(!defined("ROOT")) exit();
if(!isset($_GET['img_id'])) exit();
use Core\Classi\UTILITY;
use Core\Classi\Controllers\IMG as ImgController;
$ImgId = (int) $_GET['img_id'];
$page = (int) $_GET['page'];
$ImgController = new ImgController($ImgId);

use Core\Classi\CRYPTO;
$tokenStringa = "media.update_imgtag";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$token = $Crypto->getToken();
?>

<form name="f_imgtag_update" method="post" action="<?php echo $PathPanel; ?>media/actions.aj.php">
    
    <input type="hidden" name="action" value="update_imgtag" />
    <input type="hidden" name="token" value="<?php echo $token; ?>" />
    <input type="hidden" name="img_id" value="<?php echo $ImgId; ?>" />
    <input type="hidden" name="page" value="<?php echo $page; ?>" />

    <div class="row">
        <div class="form-group col-12 col-md-9">

            <input type="text" class="form-control" name="tag_value" value="<?php echo $ImgController->Model->tag; ?>" />

        </div>

        <div class="form-group col-12 col-md-3">
            <button type="submit" class="btn btn-success page-loader">
                <i class="fa fa-floppy-o"></i> Salva
            </button>
        </div>
    </div>

</form>
