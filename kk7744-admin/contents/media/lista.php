<?php
include_once(ROOT."Core/Classi/Admin/Dropzone.php");
use Core\Classi\Admin\DROPZONE;
?>

<?php
if(!empty($_GET['show_response']) && $_GET['show_response'] == 1 && !empty($_SESSION['Response'])) {
	$responseType = "danger";
	if($_SESSION['Response']['result']) {
		$responseType = "success";
		$responseMessage = "Modifiche media saltvate";
	} else {
		$responseMessage = $Lingua->Translations['admin']->user->errors->{$_SESSION['Response']['error']};
	}
?>
				<div class="row">
					<div class="col-12">
						<div class="sufee-alert alert with-close alert-<?php echo $responseType; ?> alert-dismissible fade show" role="alert">
                        <?php echo $responseMessage; ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
						</div>
					</div>
				</div>
<?php	
}
?>

<?php
if(!is_null($AllImgs) && count($AllImgs)>0) {
?>
<style>
.media-img .card-header {
    height:70px;
    
}
.media-img .card-header .img-tag {
    font-size:12px;
}
</style>
<div class="row">
<?php
    foreach($AllImgs as $img) {

        $pathToFile = PATH_UPLOAD."img/";
        if($img['avatar']) {
            $pathToFile .= "avatar/";
        }
        $imgTag = $img['tag'];
        if(strlen($imgTag)>40) {
            $imgTag = substr($imgTag, 0, 40). "...";
        }
?>
    <div class="col-12 col-md-6 col-lg-2 media-img deleteobject">
        <div class="card loader-box">
            <div class="card-header" data-toggle="tooltip" data-placement="bottom" title="<?php echo $img['tag']; ?>">
            <strong class="img-tag"><?php echo $imgTag; ?></strong>
            </div>

            <div class="card-body card-block">
            <a href="<?php echo $pathToFile; ?>800/<?php echo $img['filename'];?>" class="fancybox" rel="gallery_<?php echo $img['id']; ?>"><img src="<?php echo $pathToFile; ?>crop/<?php echo $img['filename'];?>" /></a>
            </div>

            <div class="card-footer clearfix">
                <div class="pull-left">
                    <button type="button" class="btn btn-primary" onclick="media_edit(<?php echo $img['id']; ?>);" data-toggle="modal" data-target="#AjaxModal">
                        <i class="fa fa-pencil"></i>
                    </button>
                </div>
                <div class="pull-right">
                    <form name="f_delete_img<?php echo $img['id']; ?>" method="post" action="<?php echo $PathPanel; ?>media/actions.aj.php">
                        <input type="hidden" name="action" value="delete_img" />
                        <input type="hidden" name="img_id" value="<?php echo $img['id']; ?>" />
                        <input type="hidden" name="token" value="<?php echo $tokenDeleting; ?>" />
                    <button type="button" class="btn btn-danger confirmation-button" data-formname="f_delete_img<?php echo $img['id']; ?>" data-isajax="1" data-hasloader="1" data-confirmtext="La cancellazione dell'immagine è irreversibile" data-actiontype="delete">
                        <i class="fa fa-trash"></i>
                    </button>
                    </form>
                </div>
            </div>

        </div>
   
    </div>
<?php
    }
?>

</div>


<div class="row">
    <div class="col-sm-12 col-md-5"></div>
    
    <div class="col-sm-12 col-md-7">
    <?php
    if(!is_null($Paginazione_html)) {
        print($Paginazione_html);
    }
    ?>
    </div>
</div>

<?php
} else {
    if($page > 1) {
        $newPage = $page - 1;
?>
    <script>
    location.href = '<?php echo $PathPanel;?>media/lista.php?page=<?php echo $newPage; ?>';
    </script>
<?php
    }
}
?>
<div class="row margin-top margin-bottom" data-margin_top="50" data-margin_bottom="50" id="media-uploader">
    <div class="col-12">

        <div class="card">
            <div class="card-header">
                <strong>Inserisci nuove immagini</strong>
            </div>
            
            <div class="card-body card-block">
<?php
$Dropzone = new DROPZONE();
$Dropzone->getDropzone($tokenDropzone);
?>
            </div>
        </div>

        <div class="card-footer clearfix">
            <div class="pull-left">
            <form name="f_media_uploader" method="post" action="<?php echo $PathPanel; ?>media/actions.aj.php">
                <input type="hidden" name="action" value="new_images" />
                <input type="hidden" name="token" value="<?php echo $tokenDropzone; ?>" />
                <button type="submit" class="btn btn-success btn-sm page-loader">
                    <i class="fa fa-floppy-o"></i> Carica le nuove immagini
                </button>
            
            </form>
            </div>

            <div class="pull-right">
                <button type="button" class="btn btn-warning btn-sm" onclick="$('#media-uploader').removeClass('active');">
                    <i class="fa fa-floppy-o"></i> Chiudi senza salvare
                </button>
            </div>
        </div>


    </div>
</div>

<div id="pulsanti_fixed">
    
    <a href="javascript:void(0)" class="actionButton" title="Aggiungi immagini" onclick="$('#media-uploader').addClass('active');"><i class="fa fa-plus"></i></a>

</div>

<script>
function media_edit(elm_id) {
    var Caller = new AjaxLoader('contents/media/aj-media-edit.php');
    Caller.setData("img_id="+elm_id+"&page=<?php echo $page; ?>");
    Caller.setHeadTitle("Modifica il Tag dell'immagine");
    Caller.call();
}
</script>