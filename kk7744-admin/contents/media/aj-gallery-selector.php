<?php
include_once("../../HeadersPhp.php");
if(!defined("ROOT")) exit();

use Core\Classi\Models\IMG;
use Core\Classi\Controllers\IMG as ImgController;
use Core\Classi\UTILITY;
include_once(ROOT."Core/Classi/Admin/Pagination_Html.php");
use Core\Classi\Admin\PAGINATION_HTML;

if(empty($_GET['container_id'])) die();
$BoxContainer = $_GET['container_id'];

$Img = new IMG();
$ImgController = new ImgController();

$page = 1;
if(isset($_GET['page'])) {
    $page = (int) $_GET['page'];
}
$next_page = $page+1;
$limit = 12;
$Img->limit = $limit;
$Img->order = " ORDER BY id DESC";
$Img->page = $page;
$AllImgs = $Img->lista();
?>

<?php
if(!is_null($AllImgs) && count($AllImgs)>0) {
?>

<?php
if($page == 1) {
?>
<script>
var next_page = <?php echo $page+1; ?>;
var BoxContainer = $("#<?php echo $BoxContainer; ?>");
</script>
<style>
.media-img .card-header {
    height:70px;
    
}
.media-img .card-header .img-tag {
    font-size:12px;
}
</style>
<div class="row gallery-list" id="row_<?php echo $BoxContainer; ?>">
<?php
}
?>
<?php
    foreach($AllImgs as $img) {

        $pathToFile = PATH_UPLOAD."img/";
        if($img['avatar']) {
            $pathToFile .= "avatar/";
        }
        $imgTag = $img['tag'];
        if(strlen($imgTag)>40) {
            $imgTag = substr($imgTag, 0, 40). "...";
        }
?>
    <div class="col-12 col-md-4 media-img">
        <div class="card loader-box">
            <div class="card-header" data-toggle="tooltip" data-placement="bottom" title="<?php echo $img['tag']; ?>">
            <strong class="img-tag"><?php echo $imgTag; ?></strong>
            </div>

            <div class="card-body card-block">
            <a href="<?php echo $pathToFile; ?>800/<?php echo $img['filename'];?>" class="fancybox image-item" rel="gallery_<?php echo $img['id']; ?>"><img src="<?php echo $pathToFile; ?>crop/<?php echo $img['filename'];?>" data-imgid="<?php echo $img['id']; ?>" /></a>
            </div>

            <div class="card-footer clearfix text-center">
                <!--
                <button type="button" class="btn btn-primary pull-center" onclick="selectThisImg(this);" data-target="#AjaxModal">Usa questa</button>
                -->
                <input type="checkbox" class="img-checkbox" data-imgid="<?php echo $img['id']; ?>" name="pictures[<?php echo $img['id']; ?>]" />
            </div>

        </div>

    </div>
<?php
    }
?>
<script>
if($(".fancybox").length > 0) {
    $(".fancybox").fancybox();
}
</script>

<?php
if($page == 1) {
?>
    <div class="progress mb-3" style="width:90%; margin:0 auto; display:none;">
        <div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
</div>
<div class="row" id="more_images_<?php echo $BoxContainer; ?>">
    <div class="col-6 text-left"><button type="button" class="btn btn-primary more-img-button" onclick="load_other_imgs();"><i class="menu-icon fa fa-plus-circle"></i> Carica altre</button></div>

    <div class="col-6 text-right"><button type="button" class="btn btn-success" onclick="save4gallery(this);"><i class="menu-icon fa fa-floppy-o"></i> Aggiungi a Gallery</button></div>
</div>

<script>
function load_other_imgs() {
    console.log("Progress bar");
    $("#row_<?php echo $BoxContainer; ?> .progress.mb-3").css("display", "flex");

    setTimeout(function() {
        $.ajax({
			url : valrootPanel+"contents/media/aj-gallery-selector.php",
			type: "get",
			data: "container_id=<?php echo $BoxContainer; ?>&page="+next_page,
			dataType: "html",
			success: function(response) {
                //codePrint(response);
                $("#row_<?php echo $BoxContainer; ?> .progress.mb-3").css("display", "none");
                if(response.trim() != "no_more_imgs") {
                    
                    $("#row_<?php echo $BoxContainer; ?> .progress.mb-3").before(response);
                    next_page++;
                } else {
                
                    $("#more_images_<?php echo $BoxContainer; ?>").find("button.more-img-button").remove();
                }
			},
			error: function(event) {
				codePrint(event, "Errore chiamata");
			}
		});

    }, 1000)

}

</script>
<?php
}
?>

<?php
} else {
    if($page == 1) {
        echo "Nessuna immagine in galleria";
    } else {
        echo "no_more_imgs";
    }
}
?>