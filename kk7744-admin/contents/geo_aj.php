<?php
include_once("../HeadersPhp.php");
include_once(ROOT."Core/Classi/Admin/GeoController.php");
include_once(ROOT."Core/Classi/Models/GeoPlace.php");
use Core\Classi\UTILITY;
use Core\Classi\Admin\GEO_CONTROLLER;
use Core\Classi\Models\GEO_PLACE;

if(isset($_GET['action'])) {

    $Action = $_GET['action'];

    switch($Action) {

        case "cerca_comune":
            
            $GeoPlace = new GEO_PLACE($_GET['nation']);
            $results = $GeoPlace->searchComuni($_GET['stringa']);
            foreach($results as $result) {
                $label = $result['name']." (".$result['admin2_code'].")";
                $comuni[] = ['id'=>$result['geonameid'], 'label'=>$label, 'value'=>$label];
            }
            echo json_encode($comuni);
            break;

    }

}
