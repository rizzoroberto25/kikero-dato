<?php
if(!defined("ROOT")) header("Location:login.php");

?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DATO Panel :: Recover Password</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">


    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/selectFX/css/cs-skin-elastic.css">

    <link rel="stylesheet" href="<?php echo $PathPanel; ?>assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>



</head>

<body class="bg-dark">


    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="index.html">
                        <img class="align-content" src="<?php echo PATH; ?>img/kikero/kikero-colored.png" alt="">
                    </a>
                </div>
                <div class="login-form">
				<?php
				if(!empty($_SESSION['errors']) && is_array(($_SESSION['errors']))) {
					foreach($_SESSION['errors'] as $error) {
						echo "<div class=\"alert alert-danger\" role=\"alert\">
								".$Lingua->Translations['admin']->login->{$error}."
							</div>";
					}
				}
				?>
                    <p>
                    Fill in the following form.<br />
                    You'll receive an email with instructions for next step.
                    </p>
                    <form id="f_login" action="login.php" method="post">
						<input type="hidden" name="action" value="login">
                        <div class="form-group">
                            <label>Write your email</label>
                            <input type="email" class="form-control" name="email" placeholder="Email">
                        </div>
 
                                <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Send</button>

                                <div class="register-link m-t-15 text-center">
                                    <p><a href="login.php"> go to Login</a></p>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script src="<?php echo $PathPanel; ?>vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo $PathPanel; ?>vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo $PathPanel; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo $PathPanel; ?>assets/js/main.js"></script>


</body>

</html>