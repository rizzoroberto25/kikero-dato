<?php
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;
use Core\Classi\Controllers\IMG;
//UTILITY::codePrint($listaUtenti, "Tutti gli utenti");
if(!empty($listaUtenti) && is_array($listaUtenti) && count($listaUtenti)>0) {
?>

				<div class="row">

                    <div class="col-md-12">
                        <div class="card loader-box">
							<!--
                            <div class="card-header">
                                <strong class="card-title">Data Table</strong>
                            </div>
							-->
                            <div class="card-body">
							
								<div class="row">
								
									<div class="col-sm-12 col-md-6">
									
									</div>
								
									<div class="col-sm-12 col-md-6">
										
									</div>
									
								</div>
								
								<div class="row">
							
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>User ID</th>
											<th>Foto</th>
                                            <th>Cognome Nome</th>
                                            <th>Email</th>
                                            <th>Ruolo</th>
											<th>Accessi</th>
											<th>Azioni</th>
                                        </tr>
                                    </thead>
                                    <tbody>
<?php
foreach($listaUtenti as $utente) {
	//<img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="images/admin.jpg">
	$userImg = "<i class='ti-user text-primary border-primary'></i>"; //echo "<p>ROOT_UPLOAD.\"img/600/\"".$utente['foto']."</p>";
	if(!empty($utente['foto'])) {
		$Img = new IMG($utente['foto']);
		$avatar = $Img->getImage();
	}
	
	if(!empty($utente['foto']) && file_exists(ROOT_UPLOAD."img/avatar/300/".$avatar)) {
		$userImg = '<img class="align-self-center list-thumb rounded-circle mr-3" alt="" src="'.PATH_UPLOAD."img/avatar/300/".$avatar.'">';
	}
	
?>
										<tr>
											<td><?php echo $utente['id']; ?></td>
											<td><?php echo $userImg; ?></td>
											<td><?php echo $utente['cognome']; ?> <?php echo $utente['nome']; ?></td>
											<td><?php echo $utente['email']; ?></td>
											<td><?php echo $utente['ruolo']; ?></td>
											<td>totale: <?php echo $utente['accessi']; ?><br />
												ultimo: <?php echo date("Y-m-d", $utente['ultimo_accesso']); ?>
											</td>
											<td><a href="<?php echo $PathPanel; ?>users/edit.php?user_id=<?php echo $utente['id']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i> Edit User</a></td>
										</tr>
<?php	
}
?>
									
									</tbody>
                                </table>
								
								</div>
								
								<div class="row">
									<div class="col-sm-12 col-md-5"></div>
									
									<div class="col-sm-12 col-md-7">
									<?php
									if(!is_null($Paginazione_html)) {
										print($Paginazione_html);
									}
									?>
									</div>
								</div>
								
								
								
                            </div>
                        </div>
                    </div>
					
				</div>
<?php
}
?>