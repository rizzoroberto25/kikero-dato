<?php
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;

if(!empty($_GET['show_response']) && $_GET['show_response'] == 1 && !empty($_SESSION['Response'])) {
	$responseType = "danger";
	if($_SESSION['Response']['result']) {
		$responseType = "success";
		$responseMessage = "Modifiche utente saltvate";
	} else {
		$responseMessage = $Lingua->Translations['admin']->user->errors->{$_SESSION['Response']['error']};
	}
?>
				<div class="row">
					<div class="col-12">
						<div class="sufee-alert alert with-close alert-<?php echo $responseType; ?> alert-dismissible fade show" role="alert">
							<?php echo $responseMessage; ?>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
						</div>
					</div>
				</div>
<?php	
}

//UTILITY::codePrint($utente, "Utente");
?>
				
				<button type="button" class="btn-outline-link btn-sm margin-bottom" onclick="location.href='<?php echo $PathPanel; ?>users/lista.php?page=<?php echo $page; ?>'" data-margin_bottom="20">
					<i class="fa fa-users"></i> Torna alla lista
				</button>
				
				<div class="row">
					<form name="f_user" class="parent-form kk-validation" method="post" action="<?php echo $PathPanel;?>users/actions.php" enctype="multipart/form-data">
					<input type="hidden" name="action" value="update" />
					<input type="hidden" name="token" value="<?php echo $tokenForm; ?>" />
					<input type="hidden" name="user_id" value="<?php echo $utente->id;?>" />
					<input type="hidden" name="page" value="<?php echo $page; ?>" />
                    <div class="col-12">
					
						<div class="card">
                            <div class="card-header">
                                <strong>Modifica Utente</strong>
                            </div>
							
							<div class="card-body card-block">
								
								<div class="row">
									<div class="form-group col-12 col-sm-6">
										
										<label class="form-control-label">Email *</label>
										<div class="input-group">
											<div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
											<input type="email" class="form-control required regexp_val" data-regexp="email" name="email" value="<?php echo $utente->email;?>">
										</div>
										<!--<small class="form-text text-muted">ex. 999-99-9999</small>-->
										<div class="error-message alert alert-danger danger"></div>
									</div>
									
									<div class="form-group col-12 col-sm-6">
										<label class=" form-control-label">Nickname</label>
										<div class="input-group">
											<div class="input-group-addon"><i class="fa fa-user"></i></div>
											<input type="text" class="form-control regexp_val" data-regexp="username" name="nick" value="<?php echo $utente->nick;?>">
										</div>
										<!--<small class="form-text text-muted">ex. 999-99-9999</small>-->
										<div class="error-message alert alert-danger danger"></div>
									</div>
									
								</div>
								
								
								<div class="row">
									<div class="form-group col-12 col-sm-6">
										<label for="select" class=" form-control-label">Ruolo *</label>
										
											<select name="ruolo" class="form-control required">
												<option value="">Assegna un ruolo</option>
											<?php
											foreach($listaRuoli as $ruolo) {
												$selected = "";
												if($ruolo['id'] == $utente->ruolo){
													$selected = " selected='selected'";
												}
											?>
												<option value="<?php echo $ruolo['id']; ?>"<?php echo $selected; ?>><?php echo $ruolo['role']; ?></option>
											<?php
											}
											?>
											</select>
										
										<div class="error-message alert alert-danger danger"></div>
									</div>
									
									<div class="form-group col-6 col-sm-3">
									
										<label for="select" class=" form-control-label">Attivo</label>
										<br />
										<label class="switch switch-text switch-success"><input type="checkbox" name="attivo" value="1" class="switch-input" <?php if($utente->attivo) echo "checked=\"true\""; ?>> <span data-on="On" data-off="Off" class="switch-label"></span> <span class="switch-handle"></span></label>
										
									</div>
									
									<div class="form-group col-6 col-sm-3">
										
										<label for="select" class=" form-control-label">Bannato</label>
										<br />
										<label class="switch switch-text switch-danger"><input type="checkbox" name="banned" value="1" class="switch-input" <?php if($utente->banned) echo "checked=\"true\""; ?>> <span data-on="SI" data-off="NO" class="switch-label"></span> <span class="switch-handle"></span></label>
										
									</div>
								</div>
								
								<div class="row">
									<div class="form-group col-12 col-sm-6">
									
										<label for="file-input" class=" form-control-label">Password</label>
										<!--<input type="file" name="avatar" class="form-control-file foto_file_header" />-->
										<input type="password" class="form-control regexp_val" data-regexp="password" name="password">
										
										<div class="error-message alert alert-danger danger"></div>
										
									</div>
									
									<div class="form-group col-12 col-sm-6">
										<label for="file-input" class=" form-control-label">Ripeti Password</label>
										<input type="password" class="form-control repeating" data-repeat="password" name="rip_password">
										
										<div class="error-message alert alert-danger danger"></div>
									</div>
									
								</div>
								
								<div class="row">
									<div class="form-group col-12 col-sm-6">
									
										<label for="file-input" class=" form-control-label">Avatar</label>
										<!--<input type="file" name="avatar" class="form-control-file foto_file_header" />-->
										
										<div class="clearfix image_reader_parent">
											<div class="col-md-3" >

												<img class="image_reader" src="<?php echo $UserImg;?>" style="width:100px; padding:10px; border:1px solid #666;" />
												
											</div>
											<div class="col-md-p" style="padding-top:20px;"><input type="file" class="form-control-file foto_file_header" name="avatar" /></div>
										</div>
									
									</div>
								</div>
								
								
							</div>
							
						</div>
						
						<div class="card-footer">
							<!--<button type="button" class="btn btn-primary btn-sm validate_form_button">-->
							<button type="button" class="btn btn-success btn-sm user-validation">
								<i class="fa fa-floppy-o"></i> Salva le modifiche
							</button>
						</div>
					
					</div>
					</form>
					
				</div>