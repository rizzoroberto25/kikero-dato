var $ = jQuery;
jQuery(document).ready(function($) {
	
	$(".foto_file_header").change(function() {
		var reader = new FileReader();
		var myImg = $(this).closest(".image_reader_parent").find("img.image_reader");
		reader.onload = function (e) {
			// get loaded data and render thumbnail.
			
			myImg.attr("src", e.target.result);
			
		};
		reader.readAsDataURL(this.files[0]);
	});
	
	var margin_directions = ["top", "bottom", "left", "right"];
	
	for(var i=0; i<margin_directions.length; i++) {
		
		var direction = margin_directions[i];
		$(".margin-"+direction).each(function() {
			//codePrint(direction, "Margin "+direction);
			if($(this).data("margin_"+direction) != undefined) {
				var value = parseInt($(this).data("margin_"+direction));
				$(this).css("margin-"+direction, value+"px");
			}
		});
		
	}

	if($(".fancybox").length > 0) {
		$(".fancybox").fancybox({
			padding: 0,
			helpers: {
                overlay: {
                    locked: false
                }
            }
		});
	}

	if($(".page-loader").length>0) {
		$(".page-loader").click(function() {
			page_loader();
		});
	}

	$(".ajax-modal .closer-button").click(function() {
		var ModalId = $(this).closest(".ajax-modal").attr("id");
		var Caller = new AjaxLoader("");
		Caller.setLoader();
	});

	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});

	$(".confirmation-button").click(function() {
		confirmAction(this);
	});

});

function confirmAction(elm) {

	var formName = $(elm).data("formname");
	var myForm;
	if(formName != undefined) {
		myForm = $("form[name='"+formName+"']");
	}
	var TestoConferma = "L'operazione è irreversibile.";
	var myElm = elm;
	if($(elm).data("confirmtext") != undefined) {
		TestoConferma = $(elm).data("confirmtext");
	}

	swal.fire({
		title: "Confermi l'azione ?",
		text: TestoConferma,
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Confermo'
	}).then((result) => {
		
		if (result.value!=undefined && result.value == true) {
			
			if($(myElm).data("isajax")!=undefined && $(myElm).data("isajax")==1) {
				if($(myElm).data("hasloader")!=undefined && $(myElm).data("hasloader")==1) {
					$(myElm).closest(".loader-box").addClass("loader-container");
				}
				
				$.ajax({
					url : myForm.attr('action'),
					type: 'post',
					data: myForm.serializeArray(),
					dataType: 'json',
					success: function(response) {
						codePrint(response, "Responso");
						if(response.result) {
							setTimeout(
								function() {
									if($(myElm).data("actiontype") == "delete") {
										$(myElm).closest(".deleteobject").remove();
									} else {
										$(myElm).closest(".loader-box").removeClass("loader-container");
									}
								}, 1500
							);
						}
					},
					error: function(event) {
						codePrint(event, "Oggetto event");
					}
				});
			}

			if($(myElm).data("action") != undefined) {

				page_loader();

				var actionForm = $(myElm).data("action");
				var token = $(myElm).data("token");
				var parameters = $(myElm).data("parameters");
				
				var action_path = $(myElm).data("url")+"?action="+actionForm+"&token="+token;

				for (var property in parameters) {
					action_path += "&"+property+"="+parameters[property];
				}

				location.href = action_path;

			}
		}
	});

}

class AjaxLoader {

	constructor(url) {
		this.headTitle = "";
		this.ajaxUrl = valrootPanel+url;
		this.ajaxMethod = "get";
		this.ModalId = "AjaxModal";
		this.ModalObject = $("#"+this.ModalId);
		this.AjaxHeaders;
		this.AiaxData;
		this.dataType = "html";
		this.loaderContent = "<div class='content-loader'></div>";
	}

	setObjectModal(modal_Id) {
		this.ModalId = modal_Id;
		this.ModalObject = $("#"+this.ModalId);
	}

	setHeadTitle(title) {
		this.headTitle = title;
		this.ModalObject.find(".modal-header .modal-title").html(this.headTitle);
	}

	setCallHeaders(headers) {
		this.AjaxHeaders = headers;
	}

	setCallMetodh (method) {
		methods = ['get', 'post', 'delete'];
		if (methods.includes(method)) {
			this.ajaxMethod = method;
		}
	}

	setData (data) {
		this.AiaxData = data;
	}

	setDataType (dataType) {
		types = ['json', 'html'];
		if (types.includes(dataType)) {
			this.dataType = dataType;
		}
	}

	setLoader() {
		this.ModalObject.find(".modal-body").html(this.loaderContent);
	}

	call() {
		
		this.ModalObject.find(".modal-body .content-loader").css('display', 'block');
		var ModalObject = this.ModalObject;
		$.ajax({
			headers: this.AjaxHeaders,
			url : this.ajaxUrl,
			type: this.ajaxMethod,
			data: this.AiaxData,
			dataType: this.dataType,
			success: function(response) {
				//codePrint(response);
				setTimeout(function() {
					ModalObject.find(".modal-body").html(response);	
				}, 2000);
			},
			error: function(event) {
				codePrint(event, "Oggetto event");
			}
		});
		 

	}

}

function readTextFile(file, callback) {
	var rawFile = new XMLHttpRequest();
	rawFile.overrideMimeType("application/json");
	rawFile.open("GET", file, true);
	rawFile.onreadystatechange = function() {
		if (rawFile.readyState === 4 && rawFile.status == "200") {
			callback(rawFile.responseText);
		}
	}
	rawFile.send(null);
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function page_loader() {
	$("#pageLoader").css("display", "block");
}
function page_loader_close() {
	$("#pageLoader").css("display", "none");
}

function set_position(elm) {

	page_loader();
	var data_elm = $(elm).closest(".position-manage");
	var url_location = valrootPanel + "action_position.php";
	var position_value = data_elm.find("[name='position']").val();
	
	url_location += "?controller="+data_elm.data("controller");
	url_location += "&action=set_position&token="+data_elm.data("token");
	url_location += "&id="+data_elm.data("id")+"&position_value="+position_value;
	if(data_elm.data("bond_position") == "") {
		url_location += "&bond_position=0";
	} else {
		var bond_position = data_elm.data("bond_position");
		url_location += "&bond_position="+bond_position.field+"&bond_position_value="+bond_position.value;
	}
	
	setTimeout(
		function() {
			location.href = url_location;
		}, 1000
	);
}

function switch_action(swElm, method) {
	var value = 0;
	if($(swElm).is(":checked")==true) {
		value = 1;
	}
	var url_post = $(swElm).data("url");
	var parameters = "action="+$(swElm).data("action")+"&token="+$(swElm).data("token")+"&value="+value;
	var dataParameters = $(swElm).data("parameters"); 
	if(dataParameters!=undefined) {
		for (var property in dataParameters) {
			parameters += "&"+property+"="+dataParameters[property];
		}
	}

	if(method==undefined) {
		method = "POST";
	}
	var postMethod = method;
	setTimeout(function() {
		$.ajax({
			url : url_post,
			type: postMethod,
			data: parameters,
			dataType: "json",
			success: function(response) {
				codePrint(response, "Response");
				page_loader_close();
			},
			error: function(event) {
				codePrint(event, "Oggetto event");
			}
		});
	},1000);
}

function codePrint(object, title) {
	console.log(title);
	console.log(object);
}