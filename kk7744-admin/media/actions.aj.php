<?php
include_once("../HeadersPhp.php");
if(!defined("ROOT")) exit();
include_once(ROOT."Core/Classi/Models/Tmp_Uploads.php");
use Core\Classi\UTILITY;
use Core\Classi\Controllers\UTENTE;
use Core\Classi\Models\TMP_UPLOADS;
use Core\Classi\Models\MEDIA_MODEL;
use Core\Classi\Controllers\IMG as ImgController;

if(strpos($_SERVER['HTTP_REFERER'], DOMINIO) === false) {
	header("Location: ".PATH);
}
/*
$json_post_data = file_get_contents("php://input");
if($json_post_data !== false) {
	$_POST = json_decode($json_post_data);
}
*/

if(!isset($_POST['action']) && !isset($_GET['action'])) {
	header("Location: ".PATH);
}

if(!UTILITY::checkRequestFromMyDomain()) header("Location: ".PATH);

$ruoloActions = "admin,editor";
$Utente = new UTENTE();

if(isset($_GET['action'])) {
	$Action=$_GET['action'];
	$post = $_GET;
} else {
	$Action=$_POST['action'];
	$post = $_POST;
}

if($Action == "upload") {
	//UTILITY::codePrint($UtenteSession, "Session utente"); die();
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$PathPanel."media/lista.php"."?action_not_forbidden=1");
	};

	if(!empty($_FILES)) {

		$nomeFile = UTILITY::adatta_nomeurl($_FILES['file']['name']);

		$temp_file = $_FILES['file']['tmp_name'];
		//TEMPDIR
		$target_file =  TEMPDIR.session_id()."_".time()."_".$nomeFile;
		$token = NULL;
		if(isset($_GET['token'])) {
			$token = $_GET['token'];
		}
		$fields = [
			"session_id" => session_id(),
			"source" => session_id()."_".time()."_".$nomeFile,
			"filename" => $nomeFile,
			"tmp" => $nomeFile,
			"token" => $token,
		];
		$TmpUploads = new TMP_UPLOADS();
		$TmpUploads->store($fields);

		move_uploaded_file($temp_file, $target_file);
		chmod($target_file, 0775);

		exit();
	}

}

if($Action == "new_images") {

	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$PathPanel."media/lista.php"."?action_not_forbidden=1");
	};

	$filters = [
		["field"=>"session_id", "operator"=>"=", "value"=>session_id(),],
		["field"=>"token", "operator"=>"=", "value"=>$_POST['token'],],
	
	];
	$TempModel = new TMP_UPLOADS();
	$records = $TempModel->lista($filters);

	$ImgController = new ImgController();
	$ImgController->storeTMPUploads($records);
	$_SESSION['Response'] = $ImgController->getResponse();

	header("Location: ".$PathPanel."media/lista.php?show_response=1");
	exit();

}

if($Action == "update_imgtag") {
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$PathPanel."media/lista.php"."?action_not_forbidden=1");
	};

	$checkVerbsToken = array("media", ".", $_POST['action']);
	if(!UTILITY::checkFormToken($checkVerbsToken, $_POST['token'])) {
		header("Location: ".PATH);
		exit();
	}

	$imgId = (int) $_POST['img_id'];
	$page = (int) $_POST['page'];
	$tagValue = (string) $_POST['tag_value'];
	$ImgController = new ImgController($imgId);
	$ImgController->update_tag($tagValue);
	$_SESSION['Response'] = $ImgController->getResponse();
	header("Location: ".$PathPanel."media/lista.php?page=".$page."&show_response=1");
}

if($Action == "delete_img") {
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$PathPanel."media/lista.php"."?action_not_forbidden=1");
	};
	$checkVerbsToken = array("media", ".", $_POST['action']);
	if(!UTILITY::checkFormToken($checkVerbsToken, $_POST['token'])) {
		header("Location: ".PATH);
		exit();
	}
	$imgId = (int) $_POST['img_id'];
	$ImgController = new ImgController($imgId);
	$ImgController->remove_img();
	die(json_encode($ImgController->getResponse()));
}

if($Action == "gallery") {
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$_SERVER['HTTP_REFERER']."?action_not_forbidden=1");
	};

	$checkVerbsToken = array("media", ".", $_POST['action']);
	if(!UTILITY::checkFormToken($checkVerbsToken, $_POST['token'])) {
		header("Location: ".PATH);
		exit();
	}

	$medias = explode(",", $post['media_ids']);
	$ImgController = new ImgController();
	$ImgController->save_gallery($post['object'], (int) $post['object_id'], $medias);

	$_SESSION['Response'] = $ImgController->getResponse();
	
	header("Location: ".$_SERVER['HTTP_REFERER']);

}

if($Action == "remove_gallery_img") {
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$_SERVER['HTTP_REFERER']."?action_not_forbidden=1");
	};

	$checkVerbsToken = array("media", ".", $_POST['action']);
	if(!UTILITY::checkFormToken($checkVerbsToken, $_POST['token'])) {
		header("Location: ".PATH);
		exit();
	}

	$img_id = (int) $post['img_id'];
	$MediaModel = new MEDIA_MODEL($img_id);
	$response = ['result'=>$MediaModel->remove()];
	die(json_encode($response));

}

if($Action == "set_positions") {
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$_SERVER['HTTP_REFERER']."?action_not_forbidden=1");
	};
	
	$checkVerbsToken = array("media", ".", $_POST['action']);
	if(!UTILITY::checkFormToken($checkVerbsToken, $_POST['token'])) {
		header("Location: ".PATH);
		exit();
	}
	/*
	foreach($post['elements'] as $id=>$position) {
		$id = (int) $id;
		$position = (int) $position;
		$MediaModel = new MEDIA_MODEL($id);
		$MediaModel->set_pos
	}
	*/
	$MediaModel = new MEDIA_MODEL();
	$MediaModel->setElmsPositions($post['elements']);
	//UTILITY::codePrint($_POST, "Post finale");

	$response = ["result"=>true, ];
	die(json_encode($response));

}

exit();

