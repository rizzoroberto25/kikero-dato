<?php
include_once("../HeadersPhp.php");
use Core\Classi\Models\IMG;
use Core\Classi\Controllers\IMG as ImgController;
use Core\Classi\UTILITY;
include_once(ROOT."Core/Classi/Admin/Pagination_Html.php");
use Core\Classi\Admin\PAGINATION_HTML;

$CP = "media/lista";
$pageTitle = "Gestione Media";

$Img = new IMG();
$ImgController = new ImgController();

$page = 1;
if(isset($_GET['page'])) {
	$page = (int) $_GET['page'];
}
$limit = 12;
if(!is_null($ImgController->getSession('media.limit'))) {
	$limit = $ImgController->getSession('media.limit');
}
if(isset($_GET['limit'])) {
	$limit = (int) $_GET['limit'];
}

$ImgController->setSession(['media' =>['limit'=>$limit]]);
$Img->order = " ORDER BY id DESC";
$Img->limit = $limit;
$Img->page = $page;
$AllImgs = $Img->lista();
$paginazione = $Img->paginate();

$Paginazione_html = NULL;
if(!empty($paginazione) && is_array($paginazione)) {

	$pagination_rules = array(
		'page' => $page,
		'totPages' => $paginazione['tot_pagine'],
		'currentUrl' => $PathPanel."media/lista.php",
		'hasHtaccess' => false,
	);
	$Paginazione = new PAGINATION_HTML($pagination_rules);
	$Paginazione_html = $Paginazione->getHtml();
}

if(!is_null($AllImgs) && is_array($AllImgs)) {
    foreach($AllImgs as $imgId=>$img) {

        $rootToFile = ROOT_UPLOAD."img/";
        if($img['avatar']) {
            $rootToFile .= "avatar/";
        }
        if(!file_exists($rootToFile."800/".$img['filename'])) {
            unset($AllImgs[$imgId]);
            $ImgController = new ImgController($img['id']);
            $ImgController->remove_img();
        }
    }
}

use Core\Classi\CRYPTO;

$tokenStringa = "media.list.".time();
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$tokenDropzone = $Crypto->getToken();

$tokenStringa = "media.delete_img";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$tokenDeleting = $Crypto->getToken();

include_once($RootPanel."template.php");