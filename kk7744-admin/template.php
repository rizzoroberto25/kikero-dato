<?php
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;
use Core\Classi\LINGUA;
$Lingua = new LINGUA(LINGUA_PR);
$Lingua->loadTranslations("admin", "admin");
include_once(ROOT."Core/Classi/Admin/ResponseController.php");
use Core\Classi\Admin\RESPONSE_CONTROLLER;
$ResponseControllerTmp = new RESPONSE_CONTROLLER($Lingua);
if(isset($_SESSION['Messages']) && is_array(($_SESSION['Messages']))) {
    
    $ResponseControllerTmp->setMessages($_SESSION['Messages']);
    unset($_SESSION['Messages']);
}
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?php echo LINGUA_PR; ?>">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DATO - Admin Area</title>
    <meta name="description" content="DATO - Admin Area">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo PATH; ?>img/kikero/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="<?php echo PATH; ?>img/kikero/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="<?php echo PATH; ?>img/kikero/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="<?php echo PATH; ?>img/kikero/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?php echo PATH; ?>img/kikero/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?php echo PATH; ?>img/kikero/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?php echo PATH; ?>img/kikero/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="<?php echo PATH; ?>img/kikero/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="<?php echo PATH; ?>img/kikero/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="<?php echo PATH; ?>img/kikero/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="<?php echo PATH; ?>img/kikero/favicon/mstile-310x310.png" />

    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>vendors/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $PathPanel; ?>vendors/FancyBox/source/jquery.fancybox.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


    <link rel="stylesheet" href="<?php echo $PathPanel; ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo $PathPanel; ?>assets/css/kikero.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
	
	<script>
	var valroot="<?php echo PATH; ?>";
	var valrootPanel = "<?php echo PATH.$SlugPanel.'/'; ?>"
	</script>

    
    <script src="<?php echo $PathPanel; ?>vendors/jquery/dist/jquery.min.js"></script>
    <script> var $ = jQuery; </script>
    <script src="<?php echo $PathPanel; ?>vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo $PathPanel; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo $PathPanel; ?>assets/js/main.js"></script>


    <script src="<?php echo $PathPanel; ?>vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="<?php echo $PathPanel; ?>assets/js/dashboard.js"></script>
    <script src="<?php echo $PathPanel; ?>assets/js/widgets.js"></script>
    <script src="<?php echo $PathPanel; ?>vendors/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="<?php echo $PathPanel; ?>vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="<?php echo $PathPanel; ?>vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo $PathPanel; ?>vendors/FancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="<?php echo $PathPanel; ?>vendors/FancyBox/source/jquery.fancybox.js"></script>
	
	
	<script type="text/javascript" src="<?php echo $PathPanel; ?>vendors/swal/sweetalert2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo $PathPanel; ?>vendors/kk-validation/<?php echo $_SESSION['lingua']; ?>/validationMessages.js"></script>
	<script type="text/javascript" src="<?php echo $PathPanel; ?>vendors/kk-validation/validation.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	<script src="<?php echo $PathPanel; ?>assets/js/kikero.js"></script>

</head>

<body>
    <div id="pageLoader"></div>
	<?php include_once($RootPanel."template/leftSidebar.php"); ?>

    <!-- Right Panel -->
	<div id="right-panel" class="right-panel">
	
		<!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

                        <div class="dropdown for-notification">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bell"></i>
                                <span class="count bg-danger">5</span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="notification">
                                <p class="red">You have 3 Notification</p>
                                <a class="dropdown-item media bg-flat-color-1" href="#">
                                <i class="fa fa-check"></i>
                                <p>Server #1 overloaded.</p>
                            </a>
                                <a class="dropdown-item media bg-flat-color-4" href="#">
                                <i class="fa fa-info"></i>
                                <p>Server #2 overloaded.</p>
                            </a>
                                <a class="dropdown-item media bg-flat-color-5" href="#">
                                <i class="fa fa-warning"></i>
                                <p>Server #3 overloaded.</p>
                            </a>
                            </div>
                        </div>

                        <div class="dropdown for-message">
                            <button class="btn btn-secondary dropdown-toggle" type="button"
                                id="message"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ti-email"></i>
                                <span class="count bg-primary">9</span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="message">
                                <p class="red">You have 4 Mails</p>
                                <a class="dropdown-item media bg-flat-color-1" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/1.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jonathan Smith</span>
                                    <span class="time float-right">Just now</span>
                                        <p>Hello, this is an example msg</p>
                                </span>
                            </a>
                                <a class="dropdown-item media bg-flat-color-4" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/2.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jack Sanders</span>
                                    <span class="time float-right">5 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </span>
                            </a>
                                <a class="dropdown-item media bg-flat-color-5" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/3.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Cheryl Wheeler</span>
                                    <span class="time float-right">10 minutes ago</span>
                                        <p>Hello, this is an example msg</p>
                                </span>
                            </a>
                                <a class="dropdown-item media bg-flat-color-3" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/4.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Rachel Santos</span>
                                    <span class="time float-right">15 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </span>
                            </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5">
                    <?php include_once($RootPanel."template/profileMenu.php"); ?>

                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->
		
		<!-- Breadcrumb inizio -->
		<div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
		<?php
		if(!isset($pageTitle)) {
			$pageTitle = "Dashboard";
		}
		?>
                        <h1><?php echo $pageTitle; ?></h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
					<!--
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Table</a></li>
                            <li class="active">Basic table</li>
                        </ol>
                    </div>
					-->
                </div>
            </div>
        </div>
		<!-- Breadcrumb fine -->
		
		<div class="content mt-3">
            <div class="animated fadeIn">
			
			<?php
			if(!empty($_SESSION['flash_alert'])) {
				
			?>
				<div class="alerts">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="card">
                                <div class="card-header">
                                    <strong class="card-title">Avviso</strong>
                                </div>
								<div class="card-body">
									<div class="alert alert-<?php echo $_SESSION['flash_alert']['type']; ?>" role="alert">
                                        <?php echo $_SESSION['flash_alert']['message']; ?>
                                    </div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			<?php
				unset($_SESSION['flash_alert']);
			}
			?>
        <?php
        if(!empty($_GET['show_response']) && $_GET['show_response'] == 1 && !empty($_SESSION['Response'])) {
            $ResponseControllerTmp->setResponse($_SESSION['Response']);
            $ResponseControllerTmp->printResponse();
        }
        $ResponseControllerTmp->printMessages();
        ?>
			
		<?php include_once($RootPanel."contents/".$CP.".php"); ?>
			
			</div>
		</div>
	
	</div>

    <!-- Right Panel -->


    <!-- Ajax Modal :: start -->  
    <div class="modal fade ajax-modal" id="AjaxModal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel"></h5>
                    <button type="button" class="close closer-button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="content-loader"></div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closer-button" data-dismiss="modal"><i class="fa fa-times-circle"></i> Chiudi</button>
                    <button type="button" class="btn btn-success saving-button"><i class="fa fa-floppy-o"></i> Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Ajax Modal :: end -->
	
    <script>
        (function($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script>
	
<?php
if(isset($_GET['show_response'])) {
	
	if(!isset($swalMessage)) {
		$swalMessage = false;
	}
	
	if(isset($url_to_replace)) {
		
		$url=$url_to_replace;
	} else {
		$url=$_SERVER['REQUEST_URI'];
		
		$explode=explode("?", $url);
		$url=$explode[0];
	}
	
	if($swalMessage) {
	
?>
<script>
swal({
   type:"<?php echo $message_alert['type'];?>",
   title:"<?php echo $message_alert['titolo'];?>",
   text:"<?php echo $message_alert['testo'];?>",
   html: true,
   allowOutsideClick: true
}, function() {
	history.replaceState({foo: "bar"}, $("head title"), "<?php echo $url;?>")
});
</script>
<?php
	} else {
?>
<script>
	jQuery(document).ready(function($) {
		history.replaceState({foo: "bar"}, $("head title"), "<?php echo $url;?>");
	});
</script>	
<?php		
	}
	unset($_SESSION['Response']);
	
}
if(isset($_SESSION['flash'])) {
	unset($_SESSION['flash']);
}	
?>

</body>

</html>
