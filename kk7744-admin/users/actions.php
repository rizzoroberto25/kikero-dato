<?php
include_once("../HeadersPhp.php");
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;
use Core\Classi\Controllers\UTENTE;

$tokenFirstVerb = "users";
include_once("../action_checks.php");

//die("Controllo token superato");
$ruoloActions = "admin";
$Utente = new UTENTE();

if($Action == "store") {
	//UTILITY::codePrint($UtenteSession, "Session utente"); die();
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$PathPanel."users/create.php"."?action_not_forbidden=1");
	};
	//UTILITY::codePrint($post, "Dati post"); die();
	if(empty($post['nick'])) {
		$post['nick'] = $post['email'];
	}
	
	$avatar = NULL;
	if(is_uploaded_file($_FILES['avatar']['tmp_name'])) {
		$avatar = $_FILES;
	}
	
	$Utente->storeUtente($post, $avatar);
	$Response = $Utente->getResponse();
	$_SESSION['Response'] = $Response;
	//UTILITY::codePrint($Response, "Store response"); die();
	//die("Dentro action");
	
	if($Response['esito']) {
		header("Location: ".$PathPanel."users/edit.php?user_id=".$Response['data']->id."&show_response=1");
		exit();
	}
	
	$_SESSION['flash']['post'] = $post;
	header("Location: ".$PathPanel."users/create.php"."?show_response=1");
	exit();
}

if($Action == "update") {
	if(!$UtenteSession->ctl_ruolo($ruoloActions)) {
		header("Location: ".$PathPanel."users/lista.php"."?action_not_forbidden=1");
	};
	$post = $_POST;
	
	$utente_id = (int) $post['user_id'];
	$Utente = new UTENTE($utente_id);
	
	$post['attivo'] = 0;
	if(!empty($_POST['attivo']) && $_POST['attivo'] == 1) {
		$post['attivo'] = 1;
	}
	$post['banned'] = 0;
	if(!empty($_POST['banned']) && $_POST['banned'] == 1) {
		$post['banned'] = 1;
	}
	
	if(empty($post['password'])) {
		unset($post['password']);
	}
	
	$avatar = NULL;
	if(is_uploaded_file($_FILES['avatar']['tmp_name'])) {
		$avatar = $_FILES;
	}
	
	
	$Utente->updateUtente($post, $avatar);
	$_SESSION['Response'] = $Utente->getResponse();
	/*
	UTILITY::codePrint($_POST, "Post update");
	die("Chiudu");
	*/
	header("Location: ".$PathPanel."users/edit.php?user_id=".$utente_id."&show_response=1");
	exit();
}

die("Fine script");
