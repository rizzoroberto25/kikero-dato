<?php
include_once("../HeadersPhp.php");

use Core\Classi\Controllers\UTENTE;
use Core\Classi\CRYPTO;

$Utente = new UTENTE();
$listaRuoli = $Utente->rolesList();
$CP = "users/create";

$tokenStringa = "users.store";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$tokenForm = $Crypto->getToken();

$pageTitle = "Utenti";

include_once($RootPanel."template.php");