<?php
include_once("../HeadersPhp.php");
if(empty($_GET['user_id'])) {
	exit();
}
$userId = (int) $_GET['user_id'];
use Core\Classi\Controllers\UTENTE;
use Core\Classi\UTILITY;
$Utente = new UTENTE($userId);
if(is_null($Utente->getUtente())) {
	
	$_SESSION['flash_alert'] = array(
		"type" => "warning",
		"message" => "Non esiste un utente con questo ID",
	);
	/*
	die($_SERVER['HTTP_REFERER']);
	ob_start();
	*/
	UTILITY::codePrint($_SERVER, "Variabili server");
	//die("valore: " . $_SERVER['HTTP_REFERER']);
	header("Location: ".$_SERVER['HTTP_REFERER']);
}

//UTILITY::codePrint($Utente->getUtente()->email);
$CP = "users/edit";
$listaRuoli = $Utente->rolesList();

use Core\Classi\CRYPTO;
$tokenStringa = "users.update";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$tokenForm = $Crypto->getToken();


$page = 1;
if(isset($_GET['page'])) {
	$page = (int) $_GET['page'];
}

$utente = $Utente->getUtente();
$Avatar = $Utente->getAvatar();
$UserImg = $PathPanel."images/user.png";
if(!empty($Avatar) && file_exists(ROOT_UPLOAD."img/avatar/600/".$Avatar)) {
	$UserImg = PATH_UPLOAD."img/avatar/600/".$Avatar;
}
$pageTitle = $utente->id.": ".$utente->email;
include_once($RootPanel."template.php");
