<?php
include_once("../HeadersPhp.php");
use Core\Classi\Controllers\UTENTE;
include_once(ROOT."Core/Classi/Admin/Pagination_Html.php");
use Core\Classi\Admin\PAGINATION_HTML;

$Utente = new UTENTE();

$page = 1;
if(isset($_GET['page'])) {
	$page = (int) $_GET['page'];
}
$limit = 10;
if(!empty($Utente->getSession('users.limit'))) {
	$limit = $Utente->getSession('users.limit');
}
if(isset($_GET['limit'])) {
	$limit = (int) $_GET['limit'];
}
$Utente->setSession(['users'=>['limit'=>$limit]]);

$Utente->setPaginationRules($limit, $page);
$listaUtenti = $Utente->utenti_tutti();
$paginazione = $Utente->getPagination(); //die("Arrivo qui 24");

$Paginazione_html = NULL;
if(!empty($paginazione) && is_array($paginazione)) {

	$pagination_rules = array(
		'page' => $page,
		'totPages' => $paginazione['tot_pagine'],
		'currentUrl' => $PathPanel."users/lista.php",
		'hasHtaccess' => false,
	);
	$Paginazione = new PAGINATION_HTML($pagination_rules);
	$Paginazione_html = $Paginazione->getHtml();
}

$CP = "users/lista";
$pageTitle = "Lista Utenti";

include_once($RootPanel."template.php");