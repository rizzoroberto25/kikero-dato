<?php
if(!defined("ROOT")) exit();
use Core\Classi\Controllers\UTENTE;
use Core\Classi\UTILITY;
$user_id = NULL;
if(!empty($_SESSION['user']['id'])) {
	$user_id = $_SESSION['user']['id'];
}
$sessionData = array();
$UtenteSession = new UTENTE($user_id);
if(!empty($_SESSION['logged'])) {
	//$UtenteSession->setSession("logged", $_SESSION['logged']);
	$sessionData['logged'] = $_SESSION['logged'];
}
if(!empty($_SESSION['user'])) {
	//$UtenteSession->setSession("user", $_SESSION['user']);
	$sessionData['user'] = $_SESSION['user'];
}
$UtenteSession->setSession($sessionData);
