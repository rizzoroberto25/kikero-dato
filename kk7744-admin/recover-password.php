<?php
include("../config.php");
include_once("check-logged.php");
if($UtenteSession->ctl_ruolo(RUOLI_DEFAULT)) {
	
	header("Location:".PATH.$SlugPanel);
}

use Core\Classi\UTILITY;
use Core\Classi\LINGUA;

$Lingua = new LINGUA($_SESSION['lingua']);
$Lingua->loadTranslations("admin", "admin"); 
//UTILITY::codePrint($Lingua, "Oggetto lingua"); die("Per Viola");
include(ROOT.$SlugPanel."/contents/recover-password.php");