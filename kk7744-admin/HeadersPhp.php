<?php
include_once(__DIR__ . "/../config.php");
use Core\Classi\UTILITY;

$GLOBALS['PathPanel'] = PATH.$SlugPanel."/";
$RootPanel = ROOT.$SlugPanel."/";

include_once($RootPanel."check-logged.php");
 
if(!$UtenteSession->isLogged() || !$UtenteSession->ctl_ruolo(RUOLI_DEFAULT)) {
	header("Location:".$PathPanel."login.php");
}
$fotoUserSession = $PathPanel."images/user.png";
/*
if(!empty($UtenteSession->Session['user']['foto']) && file_exists(ROOT."uploads/400/".$UtenteSession->Session['user']['foto'])) {
	$fotoUserSession  = PATH_UPLOADS."uploads/img/avatar/600/".$UtenteSession->Session['user']['foto'];
}
*/
if(!empty($UtenteSession->Session['user']['avatarImg']) && file_exists(ROOT_UPLOAD."img/avatar/300/".$UtenteSession->Session['user']['avatarImg'])) {
	$fotoUserSession = PATH_UPLOAD."img/avatar/300/".$UtenteSession->Session['user']['avatarImg'];
}
include_once(ROOT."Core/Classi/Admin/Html.php");

use Core\Classi\LINGUA;
$SiteLanguages = LINGUA::languagesList();
