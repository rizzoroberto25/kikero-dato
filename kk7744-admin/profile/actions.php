<?php
include_once("../HeadersPhp.php");
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;
use Core\Classi\Controllers\UTENTE;
use Core\Classi\CRYPTO;

if(strpos($_SERVER['HTTP_REFERER'], DOMINIO) === false) {
	header("Location: ".PATH);
}

if(!isset($_POST['action']) && !isset($_GET['action'])) {
	header("Location: ".PATH);
}

if(!UTILITY::checkRequestFromMyDomain()) header("Location: ".PATH);

$checkVerbsToken = array("profile", ".", $_POST['action']);
if(!UTILITY::checkFormToken($checkVerbsToken, $_POST['token'])) {
	//die("Controllo token fallito");
	header("Location: ".PATH);
	exit();
}


$Utente = new UTENTE($UtenteSession->Session['user']['id']);
//UTILITY::codePrint($UtenteSession->Session['user'], "Utente di sessione");

if(isset($_GET['action'])) {
	$Action = $_GET['action'];
	$post = $_GET;
} else {
	$Action = $_POST['action'];
	$post = $_POST;
}

if($Action == "update") {

    $post = $_POST;

    $post['attivo'] = $Utente->Model->attivo;
    $post['banned'] = $Utente->Model->banned;
    $post['ruolo'] = $Utente->Model->ruolo;
    $avatar = NULL;
	if(is_uploaded_file($_FILES['avatar']['tmp_name'])) {
		$avatar = $_FILES;
	}
	
	
    $Utente->updateUtente($post, $avatar); //UTILITY::codePrint($Utente->Model, "Model dopo update"); die();
    $Response = $Utente->getResponse();
    if(isset($Response['data']['avatar_filename'])) {

        $_SESSION['user']['avatarImg'] = $Response['data']['avatar_filename'];
    }
    $_SESSION['Response'] = $Response;
    

	header("Location: ".$PathPanel."profile/edit.php?show_response=1");
	exit();
}

if($Action == "update_password") {

	$Utente->updatePassword(UTILITY::proteggiDB($_POST['old_password']), UTILITY::proteggiDB($_POST['new_password']));
	$_SESSION['Response'] = $Utente->getResponse();
	//UTILITY::codePrint($_SESSION['Response'], "Session response"); die();
	header("Location: ".$PathPanel."profile/password-edit.php?show_response=1");
	exit();
}

die("Fine script");