<?php
include_once("../HeadersPhp.php");
use Core\Classi\Controllers\UTENTE;
use Core\Classi\UTILITY;

//UTILITY::codePrint($UtenteSession, "Utente di sessione");
$CP = "profile/password-edit";

use Core\Classi\CRYPTO;
$tokenStringa = "profile.update_password";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$tokenForm = $Crypto->getToken();

$pageTitle = "Gestisci Password";

include_once($RootPanel."template.php");