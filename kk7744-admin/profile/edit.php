<?php
include_once("../HeadersPhp.php");
use Core\Classi\Controllers\UTENTE;
use Core\Classi\UTILITY;

//UTILITY::codePrint($UtenteSession, "Utente di sessione");
$CP = "profile/edit";

use Core\Classi\CRYPTO;
$tokenStringa = "profile.update";
$Crypto = new CRYPTO($tokenStringa);
$Crypto->setToken();
$tokenForm = $Crypto->getToken();

$Utente = new UTENTE($UtenteSession->Session['user']['id']);
$Avatar = $Utente->getAvatar();
//$Avatar = $UtenteSession->AvatarImg;
$UserImg = $PathPanel."images/user.png";
if(!empty($Avatar) && file_exists(ROOT_UPLOAD."img/avatar/600/".$Avatar)) {
	$UserImg = PATH_UPLOAD."img/avatar/600/".$Avatar;
}

$pageTitle = "Il tuo profilo";

include_once($RootPanel."template.php");