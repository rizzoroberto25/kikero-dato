jQuery(document).ready(function ($) {
	var regularExpressions = {
			
		"alphabetic" : "^[a-zA-Z ']+$",
		//"email" : "^[\w\.\-]+@\w+[\w\.\-]*?\.\w{1,4}$",
		"email" : "^([ ]{0,1})*[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+[\.]([a-z0-9-]+)+(\.[_a-z0-9-]+)*([a-z]{2,3})*([ ]{0,1})$",
		"alphanumeric" : "^[a-zA-Z ']+$",
		"username" : "^[A-Za-z0-9]{8,15}$",
		//"password" : "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[_\$£%!#@?])([a-zA-Z0-9_\$£%!#@?]{8,15})$"
		"password" : "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[_\$£%!#@?])([A-Za-z0-9_\$£%!#@?]{8,15})$"
		
	}

	function getValidationResponse(response) {
		// required, errors, success
		if(response != "success") {
			Swal.fire(ValidationMessages.alerts[response]);
		} else {
			Swal.fire({
				type: 'success',
				title: 'SUBMIT',
				text: 'Invio la form'
			});
		}
	}


	function KK_formValidation() {
		
		this.regExp = regularExpressions,
		this.requireds = 0,
		this.errors = 0,
		this.theForm,
		
		this.setRegularExpressions = function(RegExp) {
			this.regExp = RegExp;
		}
		
		this.checkRequired = function(field) {
			
			var padre_elm = field.closest(".form-group");
			
			if(field.attr("type") == "checkbox") {
				
				if(field.prop("checked") == false) {
					
					padre_elm.addClass("has-error");
					//field.addClass("has-error");
					padre_elm.find(".error-message").html(ValidationMessages.fields.required);
					padre_elm.find(".error-message").css("display", "block");
					this.requireds++;		
				}
				
			} else {
				
				if(field.val() == "" || field.val()=="-") {
					
					padre_elm.addClass("has-error");
					padre_elm.find(".error-message").html(ValidationMessages.fields.required);
					padre_elm.find(".error-message").css("display", "block");
					this.requireds++;
				}
			}
			
			parentObj = this;
			field.focus(function() {
				parentObj.clearInput($(this));
			});
			
		},
		
		this.checkExpr = function(field) {
			
			if(field.hasClass("regexp_val")) {
				
				var padre_elm = field.closest(".form-group");
				
				var regExpType = field.data("regexp");
				var fieldRegExp = new RegExp(this.regExp[regExpType]);
				res = fieldRegExp.test(field.val());
				
				if(!res) {
					padre_elm.addClass("has-error");
					padre_elm.find(".error-message").html(ValidationMessages.fields[regExpType]);
					padre_elm.find(".error-message").css("display", "block");
					this.errors++;
				}
				
				parentObj = this;
				field.focus(function() {
					parentObj.clearInput($(this));
				});			
				
			} else {
				return true;
			}
			
		},
		
		this.clearForm = function() {
			parentObj = this;
			this.theForm.find(".form-group").each(function() {
				
				if($(this).hasClass("has-error")) {
					parentObj.clearInput($(this).find(".form-control"));
				}
				
			});
		},
		
		this.clearInput = function(myInput) {
			if(myInput.closest(".form-group").hasClass("has-error")) {
				myInput.closest(".form-group").find(".error-message").css("display", "none");
				myInput.closest(".form-group").find(".error-message").html("");
				myInput.closest(".form-group").closest(".form-group").removeClass("has-error")
			}
		},
		
		this.validate = function(myForm) {
			
			this.theForm = myForm;
			this.clearForm();
			
			parentObj = this;
			myForm.find(".required").each(function() {
				parentObj.checkRequired($(this));
			});
			
			if(this.requireds > 0) {
				return getValidationResponse("required");
			}
			
			parentObj = this;
			myForm.find(".regexp_val").each(function() {
				if($(this).val() != "") {
					parentObj.checkExpr($(this));
				}
			});
			
			if(this.errors > 0) {
				return getValidationResponse("errors");
			}
			
			//return getValidationResponse("success");
			//this.theForm.submit();
			return true;
		}
		
	}

	$(function() {
		$("form.kk-validation .validate_form_button").click(function() {
			var myForm = $(this).closest("form.kk-validation");
			Validation = new KK_formValidation();
			Validation.validate(myForm);
		});
		
		$("form.kk-validation .user-validation").click(function() {
			
			ctl_user_form();		
			
		});
		
		
	});
	
	
	/*****************************************
	Custom functions aggiuntive
	*****************************************/
	
	function ctl_user_form() {
		
		var Validation = new KK_formValidation();
		var myForm = $("form[name='f_user']");
		
		if(Validation.validate(myForm) === true) {
			
			password_val = myForm.find("input[name='password']").val(); 
			password_repeat_val = myForm.find("input[name='rip_password']").val();

			if(!(password_val == password_repeat_val)) {
				
				padre_elm = myForm.find("input[name='password']").closest(".form-group");
				padre_elm.addClass("has-error");
				padre_elm.find(".error-message").html(ValidationMessages.fields['password_ripeti_password']);
				padre_elm.find(".error-message").css("display", "block");
				
				padre_elm = myForm.find("input[name='rip_password']").closest(".form-group");
				padre_elm.addClass("has-error");
				padre_elm.find(".error-message").html(ValidationMessages.fields['password_ripeti_password']);
				padre_elm.find(".error-message").css("display", "block");
				
				return;
			
			} else {
				myForm.submit();
			}
			
		}
		
	}

});