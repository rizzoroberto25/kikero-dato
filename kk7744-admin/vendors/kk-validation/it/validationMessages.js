var ValidationMessages = {
	"fields" : {
		"required" : "Campo obbligatorio",
		"alphabetic" : "Utilizzare solo caratteri alfabetici e spazi",
		"alphanumeric" : "Utilizzare solo caratteri alfabetici e numeri",
		"email" : "La casella email non \u00E8 corretta",
		"username" : "Sono validi solo caratteri alfabetici e numerici e vanno utilizzati dagli 8 ai 15 caratteri",
		"password" : "Per la password vanno utilizzati dagli 8 ai 15 caratteri, di cui almeno una minuscola, una maiuscola, un carattere numerico e uno dei seguenti caratteri speciali (_\$£%!#@?)",
		"password_ripeti_password" : "Password e ripeti password non corrispondono"
	},
	
	"alerts" : {
		"required" : {
			"type" : "error",
			"title" : "",
			"text" : "Alcuni campi obbligatori sono rimasti vuoti"
		},
		"errors" : {
			"type" : "error",
			"title" : "",
			"text" : "Ci sono alcuni errori nei campi"
		}
	}
}