<?php
include("../../config.php");
if(!isset($_SESSION['user']['login']) || $_SESSION['user']['login']!=1) {
	die("Azione non autorizzata");
}
$upload_folder = ROOT."temp_foto/";
if (!empty($_FILES)) {
	$Stringhe=new STRINGHE();
  $temp_file = $_FILES['file']['tmp_name'];
  $target_path = $upload_folder;
  $target_file =  $upload_folder . session_id()."_".time()."_".$Stringhe->adatta_nomeurl($_FILES['file']['name']);
  
  $token=NULL;
  if(isset($_GET['token'])) {
  	$token=$_GET['token'];
  }
  
  $query="INSERT INTO sess_uploads(sessione, source, filename, tmp, data, token) VALUES(\"".session_id()."\", \"".session_id()."_".time()."_".$_FILES['file']['name']."\", \"".$_FILES['file']['name']."\", \"".time()."\", \"".date("Y-m-d", time())."\", \"".$token."\")";
  $db=new DB($query);
  $db->action();   
 
  if( file_exists( $target_path ) ) {
    move_uploaded_file($temp_file, $target_file);
	chmod($target_file, 0777);
  } else {
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
  }
}

?>