<?php
namespace Core\Classi;

abstract class PAGINATION {
	
	public $page;
	public $totPages;
	public $currentUrl;
	public $hasHtaccess;
	
	public function __construct(array $pagination_rules) {
		if(!empty($pagination_rules) && is_array($pagination_rules)) {
			
			$this->page = $pagination_rules['page'];
			$this->totPages = $pagination_rules['totPages'];
			$this->currentUrl = $pagination_rules['currentUrl'];
			$this->hasHtaccess = $pagination_rules['hasHtaccess'];
		
		}
	}
	
	abstract public function getHtml();
	
}