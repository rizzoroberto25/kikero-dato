<?php
namespace Core\Classi;

class DATO {

	//public $nome;
	public $dato;
	public $regular_exp;
	
	public function __construct($dato = NULL, $regular_exp = NULL) {
		$this->dato = $dato;
		$this->regular_exp = $regular_exp;
	}
	
	public static function validateDate($date, $format = 'Y-m-d H:i:s') {
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	public static function is_email($email) {
		
		return filter_var($email, FILTER_VALIDATE_EMAIL);
		
	}
	
	public function isRegulerExp() {
		if(empty($this->regular_exp) || empty($this->dato)) {
			return false;
		}
		
		//UTILITY::codePrint(preg_match($this->regular_exp, $this->dato), "Esito preg_match"); die("Fine");
		return preg_match($this->regular_exp, $this->dato);
	}

	public static function adesso_db() {
		$adesso=date("Y-m-d h:i:s", time());
		return $adesso;
	}

	public static function dbTmst_to_UXTmst($db_timestamp) {
		try {
			$date = new DateTime($db_timestamp);
			$date->getTimestamp();
		} catch (Exception $e) {
			return false;
		}
	}

	public static function UXTmst_to_dbTmst($timestamp) {
		try {
			$date = new DateTime();
			$date->setTimestamp($timestamp);
			return $date->format('Y-m-d H:i:s');
		} catch (Exception $e) {
			return false;
		}
	}
	
}
?>