<?php
namespace Core\Classi;
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;
use Core\Classi\LINGUA;

abstract class CONTROLLER {
	
	public $Model;
	protected $EspressioniRegolari;
	public $Response;
	public $Session;
	protected $PaginationRules;
	public $Pagination;
	public $Messages = [];
	public $MessagesTypes = ["success", "danger", "warning", ];
	public $text_prefix = "";
	
	protected function setModel($Model) {
		$this->Model = $Model;
		$this->EspressioniRegolari = $this->Model->getEspressioniRegolari();
		if(is_array($_SESSION)) {
			$this->setSession($_SESSION);
		} else {
			$this->setSession([]);
		}
	}
	
	protected function getModel() {
		if(empty($this->Model)) return NULL;
		return $this->Model;
	}
	
	protected function checkRegularExpressions($data) {
		
		$checkExpressions = $this->Model->check_regular_expressions($data);
		return $checkExpressions;
		
	}
	
	protected function setResponse($response = NULL) {
		if(!empty($response) && is_array($response)) {
			$this->Response = $response;
		} else {
			$this->Response = array(
				"esito" => false,
				"error" => NULL,
				"data" => NULL,
			);
		}
	}
	
	public function getResponse() {
		return $this->Response;
	}
	
	public function setSession($sessionData) {
		if(empty($this->Session)) $this->Session = array();
		if(is_array($sessionData)) {
			foreach($sessionData as $key=>$data) {
				if(!is_array($data)) {
					$this->Session[$key] = $data;
					$_SESSION[$key] = $this->Session[$key];
				} else {
					foreach($data as $kdata=>$vdata) {
						$this->Session[$key][$kdata] = $vdata;
						$_SESSION[$key][$kdata] = $vdata;
					}
				}
			}
		} else {
			return false;
		}
		return true;
	}

	public function getSession($key = NULL) {
		if(is_null($key)) {
			return $this->Session;
		} else {

			$keys = explode(".", $key);
			$sessionValue = $this->Session;
			foreach($keys as $item) {
				$sessionValue = $sessionValue[$item];
				if(!isset($sessionValue)) return NULL;
				//if(!is_array($sessionValue)) return $sessionValue;
			}

			return $sessionValue;
		}
	}
	
	public function setPaginationRules(int $limit, int $page) {
		if(!$limit > 0 || !$page > 0) return false;
		$this->PaginationRules['limit'] = $limit;
		$this->PaginationRules['page'] = $page;
		$this->Model->limit = $limit;
		$this->Model->page = $page;
		return true;
	}
	
	public function getPagination() {
		if(!is_null($this->PaginationRules) && is_array($this->PaginationRules)) {
			return $this->Pagination = $this->Model->paginate();
		}
		return false;
	}
	
	public function setLanguage() {
		
	}

	public function publish_record($value) {
		if(empty($this->Model->id)) {
			$this->setResponse(["result"=>false, "error"=>"unset_model", "data"=>NULL]);
			return;
		}
		$post_data = [
			"published" => $value,
		];
		$Response = $this->Model->update($post_data);
		$this->setResponse($Response);
		return;
	}

	public function pushMessage (string $message, string $type):bool {
		if(empty($message)) return false;
		if(!in_array($type, $this->MessagesTypes)) {
			return false;
		}
		
		//$this->setSession(['Messages'=>["type"=>$type, "message"=>$message]]);
		$_SESSION['Messages'][] = ["type"=>$type, "message"=>$message];
		return true;
	}

	public function unsetMessage($key) {
		if(!empty($key)) {
			unset($this->Messages[$key]);
			return true;
		}
		return false;
	}

	public function getMessages() {
		return $this->Messages;
	}

	public function storeObject(array $post, $files = NULL) {

		$storeData = [];

		$Testo = new TESTO($this->text_prefix);

        foreach($post['testi'] as $chiave=>$testi) {
            $item = $Testo->storeTextes($testi);
            //${$chiave} = (int) $item['data']['text_id']; 
			$storeData['testi'][$chiave] = (int) $item['data']['text_id'];
        }

        $img = 0;
        if($post['media_id']>0) {
            $img = (int) $post['media_id'];
        } else {
            if(!is_null($files)) {
                $Img = new IMG();
                $Img->storeImg($files, $this->Model->table."_image"); 
                $ResponseImg = $Img->getResponse();
                if(!$ResponseImg['result']) {
                    $this->pushMessage($ResponseImg['error'], "danger");
                }
                $img = (int) $ResponseImg['data']->id;
                $this->pushMessage("img.messages.immagine_caricata", "success");
                //$this->pushMessage("test.prova", "warning");
            }
        }

		$storeData['img'] = $img;

		return $storeData;

	}

	public function updateObject(array $post, $files = NULL) {

        $updateData = [];
        $Testo = new TESTO($this->text_prefix);

        foreach($post['testi'] as $chiave=>$testi) {
            $testo_id = $this->Model->{$chiave};
            if(is_numeric($testo_id) && $testo_id>0) {
                $item = $Testo->updateTextes($testo_id, $testi);
            }
        }

        $img = $this->Model->img;
        if($post['media_id']>0) {
            $img = (int) $post['media_id'];
        } else {
            if(!is_null($files)) {
                $Img = new IMG();
                $Img->storeImg($files, $this->Model->table."_image"); 
                $ResponseImg = $Img->getResponse();
                if(!$ResponseImg['result']) {
                    $this->setResponse($ResponseImg);
                    return;
                }
                $img = (int) $ResponseImg['data']->id;
            }
        }
        $updateData['img'] = $img;

        return $updateData;

    }
	
}