<?php

/**
 * Smart, easy and simple Image Manipulation
 * 
 * @author Alessandro Coscia, Milano, Italy, php_staff@yahoo.it
 * http://www.codicefacile.it/smartimage
 * @copyright LGPL
 * @version 0.8.9
 *
 */

if ( !function_exists('version_compare') || version_compare( phpversion(), '5', '<' ) )
	include_once( 'SmartImage.php4.class.php' ) ;
else
	include_once( 'SmartImage.php5.class.php' ) ;

?>