<?php
include_once(ROOT."Core/Classi/Ext/smarty/libs/Smarty.class.php");

class RENDER extends SMARTY {
	
	public $theme;
	public $themePath;
	public $render = array();
	public $page;
	public $Metas;
	const SitePath = PATH;
	
	public function __construct($theme = "default") {
		
		parent::__construct();
		
		$this->theme = $theme;
		$this->themePath = PATH."themes/".$this->theme."/";
		$this->page = $page;
		$this->pushRender("themePath", $this->themePath);
		$this->pushRender("sitePath", self::SitePath);
		
		$this->template_dir = ROOT."themes/".$this->theme."/";
		//$this->template_dir = ROOT."Core/smarty_dirs/templates/";
		$this->config_dir = ROOT."Core/smarty_dirs/configs/";
		$this->cache_dir = ROOT."Core/smarty_dirs/cache/";
		$this->compile_dir = ROOT."Core/smarty_dirs/templates_c/";
		
		$this->Metas['language'] = LINGUA_PR;
		$this->pushRender("Metas", $this->Metas);
	}
	
	public function getRender($page) {
		if(empty($page)) return false;
		
		$this->page = $page;
		$this->assign("render", $this->render); 
		
		$this->display($this->page.".tpl");
	}
	
	public function pushRender($key, $elms) {
		
		$this->render[$key] = $elms;
		
	}
	
	public function setMetas($metas) {
		$this->pushRender("Metas", $metas);
	}

	
}