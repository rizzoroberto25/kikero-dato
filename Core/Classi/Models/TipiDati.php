<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\MODEL;

class TIPIDATI extends MODEL {
	
	public $table = "vedo_tipi_dati";
	
	public function __construct() {
		$this->initModel($this->table);
	}
}
?>