<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\MODEL;

class IMG extends MODEL {
	
	public $table = "img";
	
	public function __construct(int $id = NULL) {
		$this->initModel($this->table);
		$espressioniCampi = array();
		$this->setEspressioniRegolari($espressioniCampi);
		
		if(!is_null($id)) {
			$this->istanzia($id);
		}
	}
	
	public function getImage() {
		if(empty($this->filename)) return NULL;
		return $this->filename;
	}
	
}