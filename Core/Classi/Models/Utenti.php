<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\MODEL;

class UTENTI extends MODEL {
	
	public $table = "utenti";
	
	public function __construct(int $id = NULL) {
		$this->initModel($this->table);
		$espressioniCampi = array(
			//"email" => "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+[\.]([a-z0-9-]+)+(\.[_a-z0-9-]+)*([a-z]{2,3})$/",
			"email" => "/^[\w\.\-]+@\w+[\w\.\-]*?\.\w{1,4}$/",
			"password" => "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[_\$£%!#@?])([a-zA-Z0-9_\$£%!#@?]{8,15})$/",
			//"password" => "/^([a-z]+)([A-Z]+)([0-9]+)([_\$£%!#@?]+)([a-zA-Z0-9_\$£%!#@?]{8,15})$/",
			"nick" => "/^[a-zA-Z0-9_@\.-]+$/",
			"lingua" => "/^[a-z]{2}$/",
		);
		$this->setEspressioniRegolari($espressioniCampi);
		
		if(!is_null($id)) {
			$this->istanzia($id);
		}
	}

}
?>