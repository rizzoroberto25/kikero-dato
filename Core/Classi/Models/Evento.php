<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\MODEL;

class EVENTO extends MODEL {

    public $table = "eventi";

    public function __construct(int $id = NULL) {
        $this->initModel($this->table);
        if(!is_null($id)) {
			$this->istanzia($id);
		}
    }
    
    public function listaEventi(array $filtri = NULL, $lingua) {
        
        $query = "
            SELECT e.*, et.testo as titolo
            FROM eventi as e
            JOIN eventi_testi as et ON e.titolo=et.id
            WHERE e.id>0 AND et.lingua='".$lingua."'
        ";

        if(!is_null($filtri) && count($filtri)>0) {
			foreach($filtri as $filtro) {
				
				//$query .= " AND ".$filtro['field']."".$filtro['operator']."\"".$filtro['value']."\"";
                $query .= " AND ".$filtro;
				
			}
		}
        $query .= " ORDER BY e.position";
        $this->setQueryString($query);
        return $this->doQuery();

    }
    

}