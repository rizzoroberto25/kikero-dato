<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\MODEL;

class GEO_PLACE extends MODEL {
	
    public $table = "geo_places_";
    public $filters = [];
	
	public function __construct($nation = GEO_DEFAULT_NATION) {
        $this->table .= $nation;
        $this->initModel($this->table);
    }

    public function searchRegioni(string $stringa) {

        if(empty($stringa)) return NULL;

        $filters = [
            " AND feature_code='ADM1'",
            " AND name LIKE(\"%".$stringa."%\") ",
        ];

        $this->limit = false;

        $this->geoSetFilters($filters);
        return $this->geoSearch($filters);

    }

    public function searchComuni(string $stringa, $provincia=NULL) {

        if(empty($stringa)) return NULL;

        $filters[0] = " AND feature_code='ADM3'";
        $filters[1] = " AND name LIKE(\"".$stringa."%\") ";
        if(strlen($stringa) > 4) {
            $filters[1] = " AND name LIKE(\"%".$stringa."%\") ";
        }

        if(!is_null($provincia)) {
            $filters[2] = " AND admin2_code='".$provincia."'";
        }
        $this->limit = false;

        $this->geoSetFilters($filters);
        return $this->geoSearch($filters);

    }

    public function geoSearchPlaces(string $stringa, $addedFilters=["regione"=>NULL,"provincia"=>NULL,"comune"=>NULL,]) {

        $filters[0] = " AND name LIKE(\"".$stringa."%\") ";
        if(strlen($stringa) > 5) {
            $filters[0] = " AND name LIKE(\"%".$stringa."%\") ";
        }
        if(!is_null($addedFilters['regione'])) {
            $filters[] = " AND admin1_code='".$addedFilters['regione']."'";
        }
        if(!is_null($addedFilters['provincia'])) {
            $filters[] = " AND admin2_code='".$addedFilters['provincia']."'";
        }
        if(!is_null($addedFilters['comune'])) {
            $filters[] = " AND admin3_code='".$addedFilters['comune']."'";
        }

        $this->limit = 10;
        $this->geoSetFilters($filters);
        return $this->geoSearch($filters);
    }

    public function geoSetFilters(array $filters) {
        $this->filters = $filters;
    }

    private function geoSearch() {
        $lista = $this->listaQuery($this->filters);
        return $lista;
    }

}