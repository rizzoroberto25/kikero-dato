<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\ATTRIBUTE;

class ATTRIBUTO_CATALOGO extends ATTRIBUTE {
	
	public $table = "catalogo_attributi";
	public $tableTranslations = "catalogo_attributi_nomi";
	public $tablePivot = "catalogo_attributi_values";
	public $campoPivot = "prodotto_id";
	
	public function __construct($id = NULL, $lingua = NULL) {
		parent::__construct($id, $lingua);
	}
}