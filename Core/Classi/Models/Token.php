<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\MODEL;
use Core\Classi\CRYPTO;
use Core\Classi\DATO;

class TOKEN extends MODEL {
	
	public $table = "tokens";
	
	public function __construct(int $id = NULL) {
		$this->initModel($this->table);
		
		if(!is_null($id)) {
			$this->istanzia($id);
		}
	}

	public function isExpired() {
		
		if(empty($this->id) || !$this->id > 0) return true;

		if(!is_null($this->deleted_at)) return true;
		
		$timestampScadenza = $this->getScadenza();
		$timestampNow = time();

		if($timestampScadenza > $timestampNow) {
			return true;
		}

		return false;
	}

	public function getScadenza($asUnix = true) {

		if(empty($this->id) || !$this->id > 0) return false;
		if(!is_null($this->deleted_at)) return false;
		$timestampCreation = DATO::dbTmst_to_UXTmst($this->created_at);

		if($asUnix) {
			return $timestampCreation + $this->validita;
		}

		$timestampScadenza = $timestampCreation + $this->validita;
		return DATO::UXTmst_to_dbTmst($timestampScadenza);
		
	}

}