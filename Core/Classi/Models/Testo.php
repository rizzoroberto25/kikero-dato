<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\DB;
use Core\Classi\MODEL;
use Core\Classi\LINGUA;
use Core\Classi\UTILITY;

class TESTO extends MODEL {

    public $table = "testi";
    public $prefix = "";
    public $id;
    public $testo;
    public $lingua;
    public $Lingua;

    public function __construct(string $prefix = '', $lingua = NULL) {
        if(!empty($prefix)) {
            $this->prefix = $prefix."_";
        }
        
        $this->table = $this->prefix.$this->table;
        if(!$this->checkIfExistsTable($this->table)) die();

        $this->initModel($this->prefix.$this->table);

        $this->lingua = $lingua;
        $this->Lingua = new LINGUA($this->lingua);
		if(!is_null($this->lingua)) {
			
			if($this->Lingua->isSiteLanguage()) {
				$this->lingua = $lingua;
			}
		}
    }

    public function setLingua(string $lingua) {
        if(empty($lingua)) return false;
        $this->Lingua = new LINGUA($lingua);
        if($this->Lingua->isSiteLanguage()) {
            $this->lingua = $lingua;
        }
    }

    public function getMaxId() {

        $max_id = 0;

        $query = "SELECT MAX(id) as maxid FROM ".$this->table;
        $this->setQueryString($query);
        $res = $this->doQuery();
        if($res !== false) {
            $max_id = $res[0]['maxid'];
        }

        return $max_id;
    }

    public function getNextId() {
        $max_id = $this->getMaxId();
        $max_id++;
        return $max_id;
    }

    public function getTextes(int $id, $lingua = false) {
        if(!$id>0) return false;
        $filters[] = ["field"=>"id", "operator"=>"=", "value"=>$id, ];
        if($lingua === true && !empty($this->lingua)) {
            $filters[] = ["field"=>"lingua", "operator"=>"=", "value"=>$this->lingua, ];   
        }
        return $this->lista($filters);
    }


    public function storeTextes(array $textes) {
        
        $nextId = $this->getNextId();
        $languages = $this->Lingua->languagesList();
        $langs = [];
        foreach($languages as $lang) {
            $langs[$lang['lingua']] = $lang['attivo'];
        }
        
        if(count($textes)>0) { 
            
            foreach($textes as $lang=>$testo) {
                if(array_key_exists($lang, $langs)) {
                    $this->store(["id"=>$nextId, "lingua"=>$lang, "testo"=>$testo], false);
                }
            }
            
            return ['result'=>true, "data"=>['text_id'=>$nextId]];
        }

        return ['result'=>false, 'error'=>'no_textes_sent'];

    }

    public function updateTextes(int $testo_id, array $textes) {
        
        if(!$testo_id>0) return false;

        $this->id = $testo_id;
        $languages = $this->Lingua->languagesList();
        $langs = [];
        foreach($languages as $lang) {
            $langs[$lang['lingua']] = $lang['attivo'];
        }

        if(count($textes)>0) {
            foreach($textes as $lang=>$testo) {
                if(array_key_exists($lang, $langs)) {
                    $this->update(["testo"=>$testo], "lingua='".$lang."'");
                }
            }
        }

        return ['result'=>false, 'error'=>'no_textes_sent'];
    }

    public function getTesto(int $id) {
        if(!$id > 0) {
            return false;
        }
        $filters[] = ["field"=>"id", "operator"=>"=", "value"=>$id];
        if(!is_null($this->lingua)) {
            $filters[] = ["field"=>"lingua", "operator"=>"=", "value"=>$this->lingua];
        }
        $testi = $this->lista($filters);
        if($testi === false) return false;
        return $testi[0]['testo'];
    }

    public function removeTestoByID(int $id) {
        if(!$id > 0) {
            return false;
        }
        $filters[] = ["field"=>"id", "operator"=>"=", "value"=>$id, ];
        return $this->removeWhere($filters);
    }

}