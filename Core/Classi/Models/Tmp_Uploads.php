<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\MODEL;
use Core\Classi\UTILITY;

class TMP_UPLOADS extends MODEL {
	
    public $table = "tmp_uploads";

    public function __construct(int $id = NULL) {
		$this->initModel($this->table);
		$espressioniCampi = array();
		$this->setEspressioniRegolari($espressioniCampi);
		
		if(!is_null($id)) {
			$this->istanzia($id);
		}
    }
    
    public function getFromSessionToken($session_id, $token) {
        if(empty($session_id) || empty($token)) return NULL;
        $filtri = [
            ["field"=>"session_id", "operator"=>"=", "value"=>$session_id],
            ["field"=>"token", "operator"=>"=", "value"=>$token],
        ];
        $rows = $this->lista($filtri);
        if(!is_null($rows)) {
            return $rows[0];
        }
    }
    
}