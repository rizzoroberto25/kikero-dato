<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\MODEL;

class ANAGRAFICA extends MODEL {
	
	public $table = "anagrafica";
	
	public function __construct() {
		$this->initModel($this->table);
	}
}
?>