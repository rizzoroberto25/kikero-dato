<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\MODEL;
use Core\Classi\UTILITY;

class MEDIA_MODEL extends MODEL {

    public $table = "media_model";
    public $acceptedMedia;

    public function __construct(int $id = NULL) {
		$this->initModel($this->table);
		$espressioniCampi = array();
		$this->setEspressioniRegolari($espressioniCampi);
		
		if(!is_null($id)) {
			$this->istanzia($id);
        }
        $this->acceptedMedia = ["image", "video", ];
    }

    public function setMediaType($mediaType):bool {
        if(in_array($mediaType, $this->acceptedMedia)) {
            $this->media_type = $mediaType;
            return true;
        }
        return false;
    }

    public function getByMedia($mediaType, $mediaID=NULL) {
        //if(!$this->setMediaType($mediaType)) return NULL;
        $filters[] = [
            "field" => "media_type",
            "operator" => "=",
            "value" => $mediaType,
        ];
        if(!is_null($mediaID) && is_integer($mediaID)) {
            $this->media_id = $mediaID;
            array_push($filters, ["field" => "media_id", "operator" => "=", "value" => $mediaID, ]);
        }
        return $this->lista($filters);
    }

    
    public function setModelField($model):bool {
        if($this->checkIfExistsTable($model)) {
            $this->model = $model;
            $this->bondPosition = "model";
            $this->bondPositionValue = $this->model;
            return true;
        }
        return false;
    }

    public function getByModel($model, $modelID=NULL) {
        if(!$this->setModelField($model)) return NULL;
        $filters[] = [
            "field" => "model",
            "operator" => "=",
            "value" => $model,
        ];
        if(!is_null($modelID) && is_integer($modelID)) {
            $this->model_id = $modelID;
            array_push($filters, ["field" => "model_id", "operator" => "=", "value" => $modelID, ]);
        }
        $this->order = "ORDER BY position ASC";
        return $this->lista($filters);
    }

    public function storeRecord($record) {  
        $response = array(
			"result" => false,
			"error" => "",
			"data" => NULL,
		);
        if(empty($record['model'])) {
            $response['error'] = "unconsistent_data_type";
            return $response;
        }
        if(!$this->setModelField($record['model'])) {
            $response['error'] = "unconsistent_data_type";
            return $response;
        }
        
        return $this->store($record);
    }

    private function removeRecords($records) {
        if(!is_null($records)) {
            foreach($records as $record) {
                $res = $this->istanzia($record['id']);
                if(!is_null($res)) { 
                    $this->remove();
                }
            }
        }
    }

    public function removeMediaId($media, int $media_id) {
        $response = array(
			"result" => false,
			"error" => "",
			"data" => NULL,
        );
        if(empty($media)) {
            $response['error'] = "unconsistent_data_type";
            return $response;
        }
        $filtri = [
                ["field"=>"media_type", "operator"=>"=", "value"=>$media],
                ["field"=>"media_id", "operator"=>"=", "value"=>$media_id],
        ];
        $records = $this->lista($filtri);
        $this->removeRecords($records);
        $response['result'] = true;
        $response['data'] = $records;
        return $response;
    }

    public function removeModelId($model, int $model_id) {
        $response = array(
			"result" => false,
			"error" => "",
			"data" => NULL,
        );
        if(empty($model) || !$this->setModelField($model)) {
            $response['error'] = "unconsistent_data_type";
            return $response;
        }
        $filtri = [
                ["field"=>"model", "operator"=>"=", "value"=>$model],
                ["field"=>"model_id", "operator"=>"=", "value"=>$model_id],
        ];
        $records = $this->lista($filtri);
        $this->removeRecords($records);
        $response['result'] = true;
        $response['data'] = $records;
        return $response;
    }


}