<?php
namespace Core\Classi\Models;
if(!defined("ROOT")) exit();
use Core\Classi\MODEL;

class USER_ROLES extends MODEL {
	
	public $table = "user_roles";
	
	public function __construct(int $id = NULL) {
		$this->initModel($this->table);
		$espressioniCampi = array(
			"role" => "/^[a-zA-Z0-9]{1,20}$/",
			
		);
		$this->setEspressioniRegolari($espressioniCampi);
		
		if(!is_null($id)) {
			$this->istanzia($id);
		}
	}

}
?>