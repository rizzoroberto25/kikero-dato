<?php
namespace Core\Classi;
if(!defined("ROOT")) exit();
use Core\Classi\DB;
use Core\Classi\CRYPTO;

class UTILITY {
	
	public static function ctl_input($input) {
		
		$input = str_replace("%", "\%", $input);
		$input = str_replace(",", "\,", $input);
		$input = str_replace(";", "\;", $input);
		$input = str_replace("-", "\-", $input);
		
		return $input;
		
	}
	
	public static function validDate($date) {
		
		if(preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $date, $matches)) 
		{
			if(checkdate($matches[2], $matches[3], $matches[1]))
			{ 
				return true;
			}
		}
		
		return false;
	}
	
	
	public static function codePrint($oggetto, $titolo=NULL, $visibile="block") {
	
		echo "<div style='display:".$visibile."'>";
		if(self::hasValue($titolo)) {
			
			echo "<h1>".$titolo."</h1>";
			
		}
		echo "<pre>";
		print_r($oggetto);
		
		echo "</pre>";
		echo "</div>";

		echo "<h1>DEBUG</h1>";
		echo "<pre>";
		print_r(debug_backtrace());
		echo "</pre>";
		
	}
	
	public static function ctl_integer_data($input) {
		if(!is_numeric($input)) {
			return false;
		}
		
		$input = (int) $input;
		
		if(!$input > 0) {
			return false;
			
		}

		return $input;
	}
	
	public static function proteggiDB($stringa, $is_email = false) {
		
		if(empty($stringa)) return $stringa;
		//if($is_email && DATO::is_email($stringa)) return $stringa;
		
		if(!$is_email) {
			$DB = new DB(NULL);
			$DB->get_conn();
			$return = mysqli_real_escape_string($DB->connessione, self::ctl_input($stringa));
			$DB->closeConn();
			return $return;
		
		} else {
			if(DATO::is_email($stringa)) {
				return $stringa;
			} else {
				
				return false;
				
			}
		}
		
	}
	
	public static function hasValue($value, $is_numeric = false) {
		
		if(empty($value) || is_null($value) || $value == "") {
			return false;
		}
		
		if($is_numeric) {
			$value = (int) $value;
			if(!$value>0) {
				return false;
			}
		}
		
		return true;
		
	}

	public static function adatta_nomeurl($val, $isUrl=false) {
	
		$stringa=urldecode($val);
		
		$stringa=str_replace("'", "-", $stringa);
		$stringa=str_replace("à", "a", $stringa);
		$stringa=str_replace("ò", "o", $stringa);
		$stringa=str_replace("ù", "u", $stringa);
		$stringa=str_replace("ì", "i", $stringa);
		$stringa=str_replace("è", "e", $stringa);
		$stringa=str_replace("é", "e", $stringa);
		
		$stringa=str_replace("/", "-", $stringa);
		$stringa=str_replace("\"", "-", $stringa);
		$stringa=str_replace(", ", "-", $stringa);
		$stringa=str_replace(" ,", "-", $stringa);
		$stringa=str_replace(",", "-", $stringa);
		$stringa=str_replace(" ", "-", $stringa);
		if($isUrl===true) {
			$stringa=str_replace(".", "-", $stringa);
		}
		
		return $stringa;
	}
	
	public static function checkFormToken($formData = array(), $token) {
		
		$stringa = "";
		foreach($formData as $dato) {
			$stringa .= $dato;
		}
		
		//$stringa .= ".".session_id();
		$Crypto = new CRYPTO($stringa);
		$Crypto->setToken();
		$realToken = $Crypto->getToken();
		
		/*
		echo "Stringa: " . $stringa . "<br />";
		echo "Real Token: " . $realToken . "<br />";
		echo "Token inviato: " . $token . "<br />";
		die("Fine confronto");
		*/
		if($realToken == $token) {
			return true;
		}
		
		return false;
		
	}
	
	public static function checkRequestFromMyDomain() {
		$check = false;
		
		if(isset($_SERVER['HTTP_REFERER'])) {
			$RefererHost = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
			
			if(DOMINIO == $RefererHost) {
				$check = true;
			}
		}
		
		return $check;
	}

	public static function isValidJson(string $string):bool {
		return is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
	}

	public static function verifyCaptcha($response, $remoteip, $chiave) {
		// Preparazione stringa di richiamo URL reCAPTCHA con
		// i valori dei parametri calcolati + la chiave segreta
	   
		$url  = "https://www.google.com/recaptcha/api/siteverify";
		$url .= "?secret="  .urlencode(stripslashes($chiave));
		$url .= "&response=".urlencode(stripslashes($response));
		$url .= "&remoteip=".urlencode(stripslashes($remoteip));
	   
		// Esecuzione chiamata HTTP in GET e prelievo del file
		// JSON di ritorno con conversione object e ritorno
	   
		$response = file_get_contents($url);
		$response = json_decode($response,true);
	   
		return (object) $response;
	}

	public static function isValidEmail($email):bool {
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}

}
?>