<?php
namespace Core\Classi;
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;

class DB {
	
	public $query;
	public $limit = false;
	
	public $table;
	public $id;
	
	public $connessione;
	
	public function __construct($limit=false) {
		//$this->query = $q;
		if($limit!==false) {
			$this->limit = $limit;
		}
		
	}
	
	private function conn() {
		
        $this->connessione = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DB) or die("Connessione al server DB fallita");
		mysqli_set_charset($this->connessione, "utf8");

	}
	
	public function get_conn() {
		$this->conn();
	}
	
	public function get($table, int $id) {
		
		if(empty($table) || empty($id)) return "dati_insufficienti";
		
		/**********
		Controllare che $id sia un campo integer
		***********/
		
		$this->table = $table;
		$this->id = $id;
		
		$this->query = "SELECT * FROM ".$this->table." WHERE id=".$this->id;
		$res = $this->do_query();
		
		if(is_null($res)) return NULL;
		
		return $res[0];
	}
	
	public function getFromField($table, $field, $value) {
		if(empty($table) || empty($field) || empty($value)) return "dati_insufficienti";
		
		$this->table = $table;
		$field = UTILITY::proteggiDB($field);
		$value = UTILITY::proteggiDB($value);
		
		$this->query = "SELECT * FROM ".$this->table." WHERE ".$field."=\"".$value."\"";
		$res = $this->do_query();
		
		if(is_null($res)) return NULL;
		
		return $res[0];
	}

	public function do_query($page=false) {
		
		$this->conn();
		
		if($page!==false) {
			$this->paginazione($page);
		}  //if(!mysqli_query($conn, $this->query)) die($this->query);
		 
		if(!$ese=mysqli_query($this->connessione, $this->query)) die("Errore query: " . $this->query);
		$results=array();
		$i=0;
		while($ext=mysqli_fetch_assoc($ese)) {
			$results[$i]=$ext;
			$i++;
		}
		if($i==0) $results = NULL;
		//mysqli_close($this->connessione);
		$this->closeConn();
		
		return $results;	
	}
	
	private function paginazione($page) {
	
		if($page>1) {
			$offset=($page-1)*$this->limit;	
		} else {
			$offset=0;	
		}
		
		$this->query.=" LIMIT ".$offset.", ".$this->limit;

	}
	
	public function do_action($lastId = false) {
		
		$conn=$this->conn();
		
		$result=false;
		if(mysqli_query($this->connessione, $this->query)) {
			if(!$lastId) {
				$result=true;
			} else {
				$result=mysqli_insert_id($this->connessione);
			}
		} else {
			die("Errore query: ".$this->query);
		}
		//mysqli_close($this->connessione);
		$this->closeConn();
		return $result;
	}
	
	public function closeConn() {
		if(!empty($this->connessione)) {
			mysqli_close($this->connessione);
		}
	}
	
}	
?>