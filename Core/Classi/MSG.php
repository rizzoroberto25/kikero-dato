<?php
namespace Core\Classi;
if(!defined("ROOT")) exit();

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Core\Classi\UTILITY;

class MSG {

	public $email_destinatario;
	private $email_assistenza;
	private $email_webmaster;
	private $host;
	private $smtp_auth;
	private $auth_user;
	private $auth_password;
	public $email_from;
	public $type;
	public $numero_telefono;
	
	public $messaggio="";
	public $oggetto="";
	
	public $mailer;
	
	public $testMode;
	
	public $logo;
	
	public function __construct($messaggio, $type="email") {

		$this->type=$type;
				
		if(is_array($messaggio)) {
			$this->messaggio=$messaggio['corpo'];
			$this->oggetto=$messaggio['oggetto'];
		} else {
			$this->messaggio=$messaggio;
		}
		
		if($this->type=="email") {
			
			$this->email_from = EMAIL_ADMIN;
			$this->email_assistenza = "info@genin.it";
			$this->email_webmaster = "webmaster@genin.it";
			$this->host = EMAIL_HOST;
			$this->smtp_auth = EMAIL_SMTP_AUTH;
			$this->auth_user = EMAIL_SMTP_USER;
			$this->auth_password = EMAIL_SMTP_PASSWORD;
			
			$this->mailer=new PHPMailer();
		}
		
		$this->testMode = AREA_TEST;
		//$this->logo="<img src=\"data:image/png;base64,".LogoBase64."\" />";	
        $this->logo="";
	}
	
	public function email($destinatari, $copie=false) {
		
		$this->mailer->From     = $this->email_from;
		$this->mailer->FromName = NOME_SITO;
		$this->mailer->Port 	= EMAIL_SMTP_POST; 
		
		if($this->testMode) {
		
			echo "Destinatario: ".$this->email_webmaster."<br />";
			echo "Vero destinatario<br />";
			//echo $destinatari;
			UTILITY::codePrint($destinatari, "Lista destinatari");
			echo "<br />";
			
			$this->mailer->AddAddress($this->email_webmaster);
		} else {
			
			if(is_array($destinatari)) {
				foreach($destinatari as $d) {
					//die("Invio l'email al vero destinatario: ".$d);
					$this->mailer->AddAddress($d);
				}
			} else {
				$this->mailer->AddAddress($destinatari);
			} 
		}
		
		if($copie) {
			if(!$this->testMode) {
				$this->mailer->AddBCC($this->email_from);
			}
			$this->mailer->AddBCC($this->email_assistenza);
		}
				
		$this->mailer->WordWrap = 50;                              // set word wrap
		$this->mailer->IsHTML(true);
									// send as HTML
		
		$this->mailer->Host = $this->host;
		# se il server smtp richiede autorizzazione:
		$this->mailer->SMTPAuth = $this->smtp_auth;
		$this->mailer->Username = $this->auth_user;
		$this->mailer->Password = $this->auth_password;
			
									
		$this->mailer->Subject  =  $this->oggetto;
		$this->mailer->Body     =  $this->logo."<br /><br />".$this->messaggio;
		$this->mailer->AltBody  =  $this->messaggio;
		//STRINGHE::codePrint($this, "Oggetto Mailer"); die();
		if(!$this->mailer->Send()) {
			
			echo 'Mailer Error: ' . $this->mailer->ErrorInfo;
		
			die("Errore invio");
			return false;
		} else {
			return true;
		}
		
	}	

}