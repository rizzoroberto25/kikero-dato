<?php
namespace Core\Classi;
if(!defined("ROOT")) exit();
include_once(ROOT."Core/Classi/Services/Geo_Global.php");
use Core\Classi\UTILITY;
use Core\Classi\Services\GEO_GLOBAL;
use Core\Classi\LINGUA;

class GEO {

    public $latitude;
    public $longitude;
    public $query;
    public $country;
    public $region;
    public $state;
    public $city;
    public $Object;
    protected $language;

    public function __construct(array $data_object = []) {
        if(count($data_object)>0) {
            foreach($data_object as $k=>$v) {
                $this->{$k} = $v;
            }
        }
        $this->language = "it";
    }

    public function setLanguage(string $language) {
        $Lingua = new LINGUA($language);
        if($Lingua->isSiteLanguage()) {
            $this->language = $language;
            return true;
        }
        return false;
    }

    // Funzioni di GeoGlobal :: start

    public function search_from_string (string $query, bool $country_data = false) {
        $filters = [
            "country" => $this->country,
            "region" => $this->region,
            "state" => $this->state,
            "city" => $this->city,
        ];
        $GeoGlobal = new GEO_GLOBAL($this->language);
        $this->Object = $GeoGlobal->forward($query, $filters, $country_data);
    }

    public function search_from_coords (string $latitude, string $longitude, bool $country_data = false) {

    }

    // Funzioni di GeoGlobal :: end

}