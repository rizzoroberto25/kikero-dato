<?php
namespace Core\Classi;
if(!defined("ROOT")) exit();

use Core\Classi\DB;
use Core\Classi\MODEL;
use Core\Classi\UTILITY;
use Core\Classi\LINGUA;

abstract class ATTRIBUTE extends MODEL {
	
	public $lingua;
	public $tableNames;
	public $modelTranslations;
	public $modelPivot;
	public $Lingua;
	
	
	public function __construct($id = NULL, $lingua = NULL) {

		$this->lingua = NULL;
		if(!is_null($lingua)) {
			$this->Lingua = new LINGUA($lingua);
			if($this->Lingua->isSiteLanguage()) {
				$this->lingua = $lingua;
			}
		}
		
		$this->tableNames = $this->table."_nomi";
		$this->initModel($this->tableNames);
		$this->modelTranslations = new MODEL($this->tableTranslations);
		$this->modelPivot = new MODEL($this->tablePivot);
		
		if(!is_null($id)) {
			$id = (int) $id;
			$this->id = $id;
			$this->istanzia($this->id);
		}
		
	}
	
	public function istanzia($id) {
		
		if(empty($id)) return false;
		$id = (int) $id;
		$res['data'] = parent::istanzia($id);
		if(!is_null($res['data'])) {

			$filtri = array();
			if(!is_null($this->lingua)) {
				
				$filtri[0] = array(
					
					"field" => "lingua",
					"operator" => "=",
					"value" => $this->lingua,
					
				);
				
			}
			
			$res['translations'] = $this->modelTranslations->lista($filtri);
			
		}
		
		return $res;
		
	}
	/*
	public function objectAttributes($oggetto_id) {
		
		if(empty($oggetto_id)) return false;
		$oggetto_id = (int) $oggetto_id;
		$filtri = array(
			0 => array(
				"field" => $this->campoPivot,
				"operator" => "=",
				"value" => $oggetto_id,
			),
		);
		
		if(!is_null($this->lingua)) {
				
			$filtri = array(
				1 => array(
					"field" => "lingua",
					"operator" => "=",
					"value" => $this->lingua,
				),
			);
			
		}
		
		return $this->modelPivot->lista($filtri);
	}
	*/
	public function store($data) {
		
		/****************************************
		
		$data['translations'] = array(
			
			"it" => "stringa italiana",
			"en" => "english string",
			
		);
		
		****************************************/
		
		$responseBase = parent::store($data['base']);
		if($responseBase === false) {
			return $responseBase;
		}
		
		$lingue = LINGUA::languagesList();
		$lingueSito = array();
		foreach($lingue as $lingua_item) {
			array_push($lingueSito, $lingua_item['lingua']);
		}
		
		foreach($data['translations'] as $lingua=>$translation) {
			if(in_array($lingua, $lingueSito)) {
				$elms = array(
					"id" => $this->id,
					"lingua" => $lingua,
					"nome" => $translation,
				);
				$this->modelTranslations->store($elms);
			}
		}
		
	}
	
}

