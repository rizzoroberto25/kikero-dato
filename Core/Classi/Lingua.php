<?php
namespace Core\Classi;
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;
use Core\Classi\DB;

class LINGUA {

	public $lingua;
	public $attivo;
	public $admin;
	public static $csvFile = LINGUA_CSV_FILE_PATH;
	public $Translations;
	public $DocPath;
	
	public static $baseQuery = "SELECT * FROM lingue";
	
	public function __construct($lingua = NULL) {
		
		$this->lingua = UTILITY::proteggiDB($lingua);
		if(!is_null($lingua)) {
			$this->istanzia();
		}
		
		$this->Translations = array();
		$this->DocPath = ROOT."docs/";
		
	}
	
	protected static function getLang($lingua) {

		if(!UTILITY::hasValue($lingua)) return false;
		$query = "SELECT * FROM lingue WHERE lingua='".$lingua."'";
		$db = new DB($query);
		$db->query = $query;
		$res = $db->do_query();
		
		if($res === false) return false;
		
		return $res[0];

	}
	
	public function istanzia() {
		
		if(!UTILITY::hasValue($this->lingua)) {
			$this->lingua = false;
			return;
		}
		
		$row = self::getLang($this->lingua);
		
		if($row === false) {
			$this->lingua = false;
			return;
		}
		
		foreach($row as $k=>$v) {
			$this->{$k} = $v;
		}
		
	}
	
	public static function worldLanguages() {
		$languages = array();
		$csvFile = fopen(ROOT."uploads/docs/csv/lingue-sigle.csv", "r");
		
		while($row = fgetcsv($csvFile, 1000, ";")) {
			$languages[$row[0]] = strtolower($row[1]);
		}
		
		fclose($csvFile);
		
		return $languages;
	}
	
	public static function languagesList() {
		
		$query = "SELECT * FROM lingue";
		$db = new DB($query);
		$db->query = $query;
		$res = $db->do_query();
		
		return $res;
		
	}
	
	public function isActive() {
		
		if(!UTILITY::hasValue($this->attivo)) return false;
		
		$active = false;
		if($this->attivo == 1) {
			$active = true;
		}
		
		return $active;
		
	}
	
	public function isSiteLanguage() {
		if(empty($this->lingua)) return false;
		$query = "SELECT * FROM lingue WHERE lingua='".$this->lingua."'";
		$db = new DB();
		$db->query = $query;
		$res = $db->do_query();
		if($res === false) return false;
		if(count($res)>0) return true;
		return false;
	}
	
	public static function ctlStringLanguage($language) {
		$ctl = false;
		$languages = self::worldLanguages();
		if(in_array($language, $languages)) {
			$ctl = true;
		}
		return $ctl;
	}
	
	public function loadTranslations($key, $doc) {
		if(empty($key) || empty($doc)) {
			return false;
		}
		if(empty($this->lingua)) {
			die("Nessuna lingua");
			return false;
		}
		$docFile = $this->DocPath."translations/".$doc."/".$this->lingua.".json";
		if(!file_exists($docFile)) {
			return false;
		}
		$contents_file = file_get_contents($docFile);
		$this->Translations[$key] = json_decode($contents_file);
	}

}
?>