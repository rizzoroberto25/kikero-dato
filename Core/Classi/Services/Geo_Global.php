<?php
namespace Core\Classi\Services;

use Core\Classi\CURL;

class GEO_GLOBAL extends CURL {

    public $geoPlace;
    public $latitude;
    public $longitude;
    public $mapUrl;
    private $apiKey = POSITION_STACK_API_KEY;
    const BaseUrl = "http://api.positionstack.com/v1/";
    public $filters;
    private $output = "json";
    public $language;

    public function __construct($language = "it", $latitude = NULL, $longitude = NULL) {
        parent::__construct([]);
        $this->endpoint = self::BaseUrl;
        $this->language = $language;
        $this->filters = NULL;
        $available_outputs = [
            "json", "xml", "geojson",
        ];
    }

    public function setOutput(string $output) {
        if(!in_array($output)) {
            return false;
        }
        $this->output = $output;
        return true;
    }

    public function setFilters(array $filters = [], bool $country_module) {
        $this->filters['language'] = $this->language;
        if(!empty($filters['country'])) {
            $this->filters['country'] = $filters['country'];
        }
        if(!empty($filters['region'])) {
            $this->filters['region'] = $filters['region'];
        }
        if(!empty($filters['state'])) {
            $this->filters['region'] = $filters['state'];
        }
        if(!empty($filters['city'])) {
            $this->filters['region'] = $filters['city'];
        }
        if($country_module) {
            $this->filters['country_module'] = $country_module;
        }
        $this->filters['output'] = $this->output;
    }

    private function filtersToQuery() {
        if(!empty($this->filters) && is_array($this->filters)) {
            foreach($this->filters as $filter=>$value) {
                $this->endpoint .= "&".$filter."=".$value; 
            }
        }
    } 

    public function forward(string $query, $ext_filters, $want_country_data = false) {
        if(!strlen($query)>2) return '{"error":{"code":"validation_error","message":"Request failed with validation error","context":{"query":{"type":"invalid_query","message":"query must have at 3 character\u0027s"}}}}';
        $this->endpoint .= "forward?access_key=".$this->apiKey."&query=".$query;
        $this->setFilters($ext_filters, $want_country_data);
        $this->filtersToQuery();
        $Response = $this->callCurl($this->endpoint);
        return $Response;
    }

}