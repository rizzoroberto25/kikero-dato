<?php
namespace Core\Classi\Services;
if(!defined("ROOT")) exit();
include_once(ROOT."Core/Classi/Curl.php");
use Core\Classi\CURL;
use Core\Classi\UTILITY;

class ArcGIS extends CURL {

    private $token;
    public $GeoEndpoint;

    public function __construct() {

        parent::__construct();

        $this->token = NULL;
        if(isset($_SESSION['ArcGIS']['access_token'])) {
            $this->token = $_SESSION['ArcGIS']['access_token'];
        }

        $this->setToken();
        $this->GeoEndpoint = ArcGIS_GEO_ENDPOINT;
        
    }

    private function setToken() {
        $endpoint = "https://www.arcgis.com/sharing/rest/oauth2/token";
        $time = time();

        $postData = [
            "client_id" => ArcGIS_CLIENT_ID,
            "client_secret" => ArcGIS_CLIENT_SECRET,
            "grant_type" => "client_credentials",
        ];
        
        $this->isJsonPost = false;
        //$this->setHeaders($headers);
        $this->setMethod("POST");
        $this->callAPI($endpoint, $postData);

        $_SESSION['ArcGIS'] = $this->curlResponse;
        $this->token = $this->curlResponse->access_token;

        UTILITY::codePrint($this->curlResponse, "Curl response");

    }

    public function searchAddress(string $stringa) {
        if(is_null($this->token)) return false;
        $headers = [
            "Authorization: Bearer ".$this->token,
        ];
        $this->setHeaders($headers);
        $endpoint = $this->GeoEndpoint."findAddressCandidates";
    }

    public function searchCities(string $stringa) {
        if(is_null($this->token)) return false;
        $headers = [
            "Authorization: Bearer ".$this->token,
        ];
        $this->setHeaders($headers);
        $endpoint = $this->GeoEndpoint."findAddressCandidates";
        $postData = [
            
        ];
    }

}   