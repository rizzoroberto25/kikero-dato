<?php
namespace Core\Classi\Services;
if(!defined("ROOT")) exit();
include_once(ROOT."Core/Classi/Curl.php");
use Core\Classi\CURL;
use Core\Classi\UTILITY;

class HERE extends CURL {

    private $credentials;
    private $user_id;
    private $token;

    public function __construct() {

        parent::__construct();

        $this->user_id = HERE_user_id;
        $docCredentials = explode("\n", file_get_contents(ROOT."env/here.credentials.properties"));

        $this->token = NULL;
        if(isset($_SESSION['Here']['token'])) {
            $this->token = $_SESSION['Here']['token'];
        }

        foreach($docCredentials as $row) {
            if(!empty($row)) {
                $split = explode(" = ", $row);
                $this->credentials[$split[0]] = trim($split[1]);
            }
        }

        if($this->credentials['here.user.id'] != $this->user_id) die("Unauthorized");

        $this->setToken();
        UTILITY::codePrint($this->credentials, "Credentials");

    }

    private function setToken() {
        $endpoint = "https://account.api.here.com/oauth2/token";
        $time = time();
        $signatureString = CODEAPP.$time;
        $signature = hash_hmac('sha256', $signatureString, $this->credentials['here.access.key.secret']);
        
        $headers = [
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: OAuth oauth_consumer_key=".$this->credentials['here.access.key.id'].", oauth_nonce=".$signatureString.", oauth_signature=".$signature.", oauth_signature_method=HMAC-SHA256, oauth_timestamp=".$time.", oauth_version=1.0",
            //"Authorization: OAuth",
        ];
        
        $postData = [
            "client_id" => $this->credentials['here.client.id'],
            "client_secret" => $this->credentials['here.access.key.secret'],
            "grant_type" => "client_credentials",
        ];
        
        $this->isJsonPost = false;
        $this->setHeaders($headers);
        $this->setMethod("POST");
        $this->callAPI($endpoint, $postData);

        UTILITY::codePrint($this->curlResponse, "Curl response");

    }

}   