<?php 
namespace Core\Classi\Admin;
use Core\Classi\Admin\HTML;

class FIXED_BUTTONS_CONTROLLER extends HTML {

    public function __construct() {
        parent::__construct();
        $this->setHtml("<div id='pulsanti_fixed'>");
    }

    public function addSearchButton($elms) {
        $html = "<a class='actionButton' href='javascript:void(0)' onclick=\"search_opener();\" data-toggle=\"modal\" data-target=\"#AjaxModal\"><i class='fa fa-search'></i></a>";
        $this->addHtml($html);

        $html = "
            <script>
            function search_opener() {
                var Caller = new AjaxLoader('".$elms['url']."');
                Caller.setData(\"\");
                Caller.setHeadTitle(\"".$elms['title']."\");
                Caller.call();
            }

            </script>
        ";

        $this->addHtml($html);
    }

    public function addPlusButton($url) {
        $html = "<a class='actionButton' href='".$url."'><i class='fa fa-plus'></i></a>";
        $this->addHtml($html);
    }

    public function addRemoveFiltersButton($url) {
        $html = "<a class='actionButton' href='".$url.".php?remove_filters' style='background-color:#FF6600;'><i class='fa fa-search-minus'></i></a>";
        $this->addHtml($html);
    }

    public function getHtmlButtons() {
        $this->addHtml("</div>");
        return $this->getHtml();
    }

    public function printHtmlButtons() {
        $this->addHtml("</div>");
        $this->printHtml();
    }

}