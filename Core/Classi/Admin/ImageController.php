<?php 
namespace Core\Classi\Admin;
use Core\Classi\Admin\HTML;
use Core\Classi\CRYPTO;

class IMAGE_CONTROLLER extends HTML {

    public $fieldName;
    public $imgCounter;
    public $image_preview;

    public function __construct($image_preview=NULL) {
        $this->imgCounter = 0;
        if(is_null($image_preview)) {
            $image_preview = PATH."img/kikero/bw-logo_segnaposto.png";
        }
        $this->image_preview = $image_preview;
    }

    public function setFieldName(string $fieldName) {
        $this->fieldName = $fieldName;
    }

    public function printImageUploader(string $fieldName, string $label, $image = NULL, $field_media_id = "media_id") {

        $this->setFieldName($fieldName);
        $this->imgCounter++;

        $fieldID = "image_uploader_".$fieldName."_".$this->imgCounter;
        
        $html = "
            <div class=\"form-group image-uploader\" id='".$fieldID."'>
                <div class=\"col-md-12\"> <label class=\"control-label mb-1\">".$label."</label></div>
                <div class=\"col-md-4\">
                <img class=\"image_reader\" src=\"".$this->image_preview."\" style=\"width:140px;\" />
                </div>
                <div class=\"col-md-8\">
                <input type=\"file\" class='image_selector' name=\"".$fieldName."\" style='margin-top:30px;' />
                </div>

                <div class=\"col-md-12\" style='padding-top:12px;' class='media_selector_container'>
                    <input type='hidden' name='".$field_media_id."' value='0' />
                    <button type=\"button\" class=\"btn btn-primary media_selector_button\" data-toggle=\"modal\" data-target=\"#AjaxModal\">Scegli un'immagine dalla galleria</button>
                </div>
            </div>

            <script>

            $(document).ready(function() {
                $('#".$fieldID."').find('.image_selector').change(function() {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        // get loaded data and render thumbnail.
                        $('#".$fieldID." .image_reader').attr('src', e.target.result);
                        $('#".$fieldID."').find(\"input[name='".$field_media_id."']\").val(0);
                    };
                
                    // read the image file as a data URL.
                    reader.readAsDataURL(this.files[0]);
                });


                $('#".$fieldID." .media_selector_button').click(function() {
                    
                    var Caller = new AjaxLoader('contents/media/aj-media-selector.php');
                    Caller.setData('container_id=".$fieldID."&field_name=".$field_media_id."');
                    Caller.setHeadTitle(\"Media Image Gallery\");
                    Caller.call();
                });



            });

			
            </script>

        ";
        
        $this->setHtml($html);
        $this->printHtml();

    }

    public function print_gallery_controller(string $object, int $object_id, $token, $pictures = NULL) {

        $tokenStringa = "media.set_positions";
        $Crypto = new CRYPTO($tokenStringa);
        $Crypto->setToken();
        $tokenGalleryPosition = $Crypto->getToken();
        
        $tokenStringa = "media.remove_gallery_img";
        $Crypto = new CRYPTO($tokenStringa);
        $Crypto->setToken();
        $tokenGalleryRemoveImg = $Crypto->getToken();

        $html = "
            <form name=\"f_gallery\" class=\"parent-form f_gallery\" method=\"post\" action=\"".PATH_PANEL."media/actions.aj.php\" enctype=\"multipart/form-data\">
            <input type=\"hidden\" name=\"action\" value=\"gallery\" />
            <input type=\"hidden\" name=\"object\" value=\"".$object."\" />
            <input type=\"hidden\" name=\"object_id\" value=\"".$object_id."\" />
            <input type=\"hidden\" name=\"token\" value=\"".$token."\" />
            <input type=\"hidden\" name=\"media_ids\" value=\"\" />
            <div class=\"col-12\">
            
                <div class=\"card\">
                    <div class=\"card-header\">
                        <strong>Galleria</strong>
                    </div>

                    <div class=\"card-body card-block\">

                        <div class=\"col-12 col-md-10\">
                            <div class=\"pictures_content row\" style=\"padding:20px; border:1px solid #666\">
            ";                
            
        if(!empty($pictures) && is_array($pictures) && count($pictures)>0) {
            foreach($pictures as $picture) {
        $html .= "
                                <div class=\"col-6 col-md-2\">
                                    <div class='picture-item' data-id='".$picture['id']."' data-position='".$picture['position']."'>
                                        <img src='".PATH.$picture['path']."' />
                                        <div class='actions clearfix'>
                                            <div class='pull-left action'><a href='javascript:void(0)' onclick='removeGalleryImg(this);'><i class=\"fa fa-trash trash\"></i></a></div>
                                            <div class='pull-right action'><a href='javascript:void(0)'><i class=\"fa fa-arrows\"></i></a></div>
                                        </div>
                                    </div>
                                </div>
        ";
            }
        }    
             
            $html .= "  
                            </div>
                        </div>

                        <div class=\"col-12 col-md-2\">
                            <button type=\"button\" class=\"btn btn-primary gallery_adder\" data-toggle=\"modal\" data-target=\"#AjaxModal\"><i class=\"fa fa-plus\"></i></button>	
                        </div>

                    </div>

                    <div class=\"card-footer\">
                        <!--
                        <button type=\"submit\" class=\"btn btn-success btn-sm page-loader\">
                        <i class=\"fa fa-floppy-o\"></i> Salva gallery
                        </button>
                        -->
                    </div>

                </div>
            </div>
            </form>
        ";

        $html .= "
            <script>
                var form_gallery;

                $('form.f_gallery').find('button.gallery_adder').click(function() {

                    form_gallery = $(this).closest('.parent-form');
                    
                    var GalleryCaller = new AjaxLoader('contents/media/aj-gallery-selector.php');
                    GalleryCaller.setData('container_id=1');
                    GalleryCaller.setHeadTitle(\"Media Image Gallery\");
                    GalleryCaller.call();

                });

                function select_picture4gallery(elm) {
                    console.log('selezionato');
                    var image_source = $(elm).closest('card').find('.image-item img').attr('src');
                }

                function save4gallery(elm) {
                    var input_string = form_gallery.find('input[name=\"media_ids\"]').val();
                    $(elm).closest('.ajax-modal').find('.gallery-list').find('.img-checkbox').each(function() {
                        
                        if($(this).is(':checked')) {
                            input_string += $(this).data('imgid')+',';
                        }
                    });
                    form_gallery.find('input[name=\"media_ids\"]').val(input_string);
                    page_loader();
                    setTimeout(function() {
                        form_gallery.submit();

                    }, 1000);
                    console.log(input_string);
                }

                function removeGalleryImg(elm) {
                    //var token_val = $(elm).closest('.gallery-row-parent').find('input[name=\"token-gallery-delete\"]').val();
                    var token_val = '".$tokenGalleryRemoveImg."';
                    var img_id = $(elm).closest('.picture-item').data('id');
                    var post_data = {
                        token: token_val,
                        img_id: img_id,
                        action: 'remove_gallery_img'
                    }
                    page_loader();
                    setTimeout(function() {
                        $.ajax({
                            url : valrootPanel+'media/actions.aj.php',
                            type: 'post',
                            data: post_data,
                            dataType: \"json\",
                            success: function(response) {
                                codePrint(response, \"Response\");
                                $(elm).closest('.picture-item').closest('.ui-sortable-handle').remove();
                                page_loader_close();
                            },
                            error: function(event) {
                                codePrint(event, \"Oggetto event\");
                            }
                        });
                    },1000);
                }

                $(document).ready(function() {
                   $('.pictures_content').sortable(

                        {
                            containment: '.pictures_content',
                            tollerance: 'pointer',
                            stop: function(event, ui) {
                                //var token_val = $(event.target).closest('.gallery-row-parent').find('input[name=\"token-gallery\"]').val();
                                var token_val = '".$tokenGalleryPosition."';

                                var elements_array = [];
                                var i = 1;
                                $(event.target).find('.picture-item').each(function() {
                                    elements_array[$(this).data('id')] = i;
                                    //$(this).attr('data-position', i);
                                    i++;
                                });
                                
                                var post_data = {
                                    model: \"".$object."\",
                                    model_id: \"".$object_id."\",
                                    action:\"set_positions\",
                                    token:token_val,
                                    elements:elements_array
                                };
                                page_loader();
                                setTimeout(function() {
                                    $.ajax({
                                        url : valrootPanel+'media/actions.aj.php',
                                        type: 'post',
                                        data: post_data,
                                        dataType: \"json\",
                                        success: function(response) {
                                            codePrint(response, \"Response\");
                                            page_loader_close();
                                        },
                                        error: function(event) {
                                            codePrint(event, \"Oggetto event\");
                                        }
                                    });
                                },1000);
                            }
                        }

                   );

                });
            </script>
        ";

        $this->setHtml($html);
        $this->printHtml();

    }

}