<?php 
namespace Core\Classi\Admin;
use Core\Classi\Admin\HTML;

class TESTO_CONTROLLER extends HTML {
    
    public $id;
    public $fields;
    public $languages;
    public $CKcounter;
    public $table;
    public $record;

    public function __construct(array $Languages = []) {
        $this->languages = $Languages;
        $this->CKcounter = 0;
    }

    public function setFields(array $fields) {
        if(!count($fields)>0) return NULL;
    }

    public function printTestiManager(string $key, array $objects): bool {

        if(empty($key)) return false;
        if(!count($objects)>0) return false;

        $html = "<div class=\"default-tab\">";
        $this->setHtml($html);
        
        $html = "<nav><div class=\"nav nav-tabs\" role=\"tablist\">";

        $counter = 0;
        foreach($this->languages as $language) {
            if($language['attivo']) {
                $cssClass = "nav-item nav-link";
                if($counter == 0) {
                    $cssClass .= " active";
                }
                $html .= "
                    <a class=\"".$cssClass."\" data-toggle=\"tab\" href=\"#".$key."-".$language['lingua']."\" role=\"tab\" aria-controls=\"nav-home\" aria-selected=\"true\">".$language['admin']."</a>
                ";
                $counter++;
            }
        }

        $html .= "</div></nav>";
        $this->addHtml($html);

        $this->addHtml("<div class=\"tab-content pl-3 pt-2\">");

        $counter = 0;
        foreach($this->languages as $language) {
            if($language['attivo']) {
                $cssClass = "tab-pane fade";
                if($counter == 0) {
                    $cssClass .= " show active";
                }

                $html = "<div class=\"".$cssClass."\" id=\"".$key."-".$language['lingua']."\" role=\"tabpanel\" aria-labelledby=\"".$key."-".$language['lingua']."-tab\">";
                
                //$html .= "Contenuto ".$language['admin'];
                foreach($objects as $object) {

                    switch($object['type']) {

                        case "cke":
                            $html .= $this->getCKEditor($language['lingua'], $object, $key);
                            break;

                        default:
                            $html .= $this->text_input($language['lingua'], $object);
                            break;
                    }

                }

                $html .= "</div>";

                $this->addHtml($html);
                $counter++;
            }
        }

        $this->addHtml("</div>");


        $html = "</div>";
        $this->addHtml($html);
        $this->printHtml();

        return true;
    }

    public static function getTextFromValues(array $values, $language) {
        $value = "";
        foreach($values as $item) {
            if(!empty($item['lingua']) && $item['lingua'] == $language) {
                if(!empty($item['testo'])) return $item['testo'];
            }
        }
        return $value;
    }

    private function text_input($language, $object) {

        $class = "form-control";
        if($object['required']) {
            $class .=" required";
        }
        $regExpAttr = "";
        if($object['reg_exp'] !== false) {
            $class .=" regexp_val";
            $regExpAttr = " data-regexp='".$object['reg_exp']."'";
        }

        $value = "";
        if(isset($object['values'])) {
            $value = self::getTextFromValues($object['values'] ,$language);
        }

        $html = "
        <div class=\"form-group\">
            <label class=\"control-label mb-1\">".$object['label']."</label>
            <input name=\"testi[".$object['field']."][".$language."]\" type=\"text\" class=\"".$class."\"".$regExpAttr." value=\"".$value."\"> 
            <div class=\"error-message alert alert-danger danger\"></div>
        </div>
        ";

        return $html;
    }

    private function getCKEditor($language, $object, $key) {
        
        $class = "form-control";
        if($object['required']) {
            $class .=" required";
        }

        $this->CKcounter++;

        $value = "";
        if(isset($object['values'])) {
            $value = self::getTextFromValues($object['values'] ,$language);
        }

        $html = "
        <div class=\"form-group\">
            <label class=\"control-label mb-1\">".$object['label']."</label>
            <textarea name=\"testi[".$object['field']."][".$language."]\" id=\"CKE_".$key."_".$this->CKcounter."\" class=\"ck_text\" cols=\"50\" rows=\"4\">".$value."</textarea>
            <div class=\"error-message alert alert-danger danger\"></div>
        </div>

        <script>
            CKEDITOR.replace( 'CKE_".$key."_".$this->CKcounter."');
        </script>

        ";

        return $html;
    }

}