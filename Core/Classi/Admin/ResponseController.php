<?php 
namespace Core\Classi\Admin;
use Core\Classi\Admin\HTML;
use Core\Classi\UTILITY;

class RESPONSE_CONTROLLER extends HTML {

    public $Response;
    public $Messages;
    public $Lingua;

    public function __construct($Lingua) {
        $this->Lingua = $Lingua;
        //UTILITY::codePrint($this->Lingua, "Oggetto Lingua"); die();
    }

    public function setResponse($Response) {
        $this->Response = $Response;
    }
    public function setMessages(array $Messages) {
        //$this->Messages = $Messages;
        if(count($Messages)>0) {
            foreach($Messages as $key=>$message) {
                $this->Messages[$key]['type'] = $message['type'];
                $explode = explode(".", $message['message']);
                $message_object = $this->Lingua->Translations['admin']; 
                
                foreach($explode as $item) {
                    
                    if(empty($message_object->{$item})) {
                        $this->Messages[$key]['message'] = $explode[count($explode)-1];
                    } else {
                        if(is_string($message_object->{$item})) {
                            $this->Messages[$key]['message'] = $message_object->{$item};
                            break;
                        } elseif(is_object($message_object->{$item})) {
                            $message_object = $message_object->{$item};
                        }
                    }
                }
            }
        }
    }

    public function printResponse() {
        if(empty($this->Response)) return false;

        $responseType = "danger";

        if($this->Response['result']) {
            $responseType = "success";
            $responseMessage = "Procedura completata";
        } else {
            //$responseMessage = $Lingua->Translations['admin']->event->errors->{$_SESSION['Response']['error']};
            $explode = explode(".", $this->Response['error']);
            $message_object = $this->Lingua->Translations['admin'];
            foreach($explode as $item) {
                    
                if(empty($message_object->{$item})) {
                    $responseMessage = $explode[count($explode)-1];
                } else {
                    if(is_string($message_object->{$item})) {
                        $responseMessage = $message_object->{$item};
                        break;
                    } elseif(is_object($message_object->{$item})) {
                        $message_object = $message_object->{$item};
                    }
                }
            }
        }

        $this->printMessageBox($responseMessage, $responseType);	

    }

    public function printMessages() {
        if(empty($this->Messages) || !is_array($this->Messages)) return false;

        foreach($this->Messages as $message) {
            $this->printMessageBox($message['message'], $message['type']);
        }
    }

    protected function printMessageBox(string $message, string $type='info') {

        if(empty($message)) return false;
        
?>
                    <div class="row">
                        <div class="col-12">
                            <div class="sufee-alert alert with-close alert-<?php echo $type; ?> alert-dismissible fade show" role="alert">
                                <?php echo $message; ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        </div>
                    </div>
<?php
        return true;
    }
}