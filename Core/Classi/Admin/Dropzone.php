<?php
namespace Core\Classi\Admin;
if(!defined("ROOT")) exit();
use Core\Classi\CONTROLLER;
use Core\Classi\UTILITY;
include_once(ROOT."Core/Classi/Crypto.php");
use Core\Classi\CRYPTO;
use Core\Classi\Models\TMP_UPLOADS;

class DROPZONE {

    public $oggetto;
    public $oggetto_id;
    public $accepted_extensions;
    public $dropzoneAssetsPath;
    public $max_num_files; 

    public function __construct() {
        $this->accepted_extensions = ".png,.jpg,.jpeg,.gif,.PNG,.JPG,.JPEG,.GIF";
        $this->dropzoneAssetsPath = PATH.SLUG_PANEL."/vendors/dropzone-master/";
        $this->max_num_files = false;
    }

    public function setObject(string $oggetto, int $oggetto_id):bool {
        if(empty($oggetto) || !$oggetto_id > 0) return false;
        $this->oggetto = $oggetto;
        $this->oggetto_id = $oggetto_id;
    }

    public function getDropzone($token = NULL) {
?>
            <link rel="stylesheet" href="<?php echo $this->dropzoneAssetsPath; ?>css/basic.css" />
            <link rel="stylesheet" href="<?php echo $this->dropzoneAssetsPath; ?>css/dropzone.css" />
            <script type="text/javascript" src="<?php echo $this->dropzoneAssetsPath; ?>dropzone.js"></script>
            <script type="text/javascript" src="<?php echo $this->dropzoneAssetsPath; ?>dropzone-amd-module.js"></script>
            
            <div id="dropzonearea" class="dropzone"></div>



            <script type="text/javascript">
            
            $(function() {
                Dropzone.autoDiscover = false;
                $("div#dropzonearea").dropzone({ 
                    init: function() {
                    
                        
                        this.on("addedfile", function(file) {
    
                            // Create the remove button
                            var removeButton = Dropzone.createElement("<button class='dropzone_remove'>Remove file</button>");
							
                    		$("#salvataggio_foto").show();
                    
                            // Capture the Dropzone instance as closure.
                            var _this = this;
                    
                            // Listen to the click event
                            removeButton.addEventListener("click", function(e) {
                              // Make sure the button click doesn't submit the form:
                              e.preventDefault();
                              e.stopPropagation();
                    
                              // Remove the file preview.
                              _this.removeFile(file);
                              // If you want to the delete the file on the server as well,
                              // you can do the AJAX request here.
                              var dataAction = "action=remove&filename="+file.name;
                              <?php
                              
                              if(isset($token)) {
                            ?>

                                dataAction += "&token=<?php echo $token; ?>";
                            <?php
                                }
                             ?>
                              
                                  $.ajax({
        
                                      type: "POST",
                            
                                      dataType: "html",
                            
                                      url: "<?php echo PATH.SLUG_PANEL; ?>/media/actions.aj.php",
                            
                                      data: dataAction,
                            
                                      success: function(dati) { console.log("Aggiorno i dati");
                                      		$("#salvataggio_foto").hide();
											/*
											if(this.files.length==0) {
												$("#dropzone_caricato").val("0");
											} */   
                            
                                      },
                            
                                      error: function(richiesta,stato,errori) {
                            
                                           // Visualizzo un messaggio di errore in caso di chiamata fallita
                            
                            
                                      }
                            
                                 });
                              
                            });
                    
                            // Add the button to the file preview element.
                            file.previewElement.appendChild(removeButton);
                            
                        
                        });
                        
						this.on("removedfile", function() {
							/*
							if(this.files.length==0) {
								$("#dropzone_caricato").val("0");
							} */
							$("#dropzone_caricato").val(this.files.length);
						});
						
						this.on("addedfile", function() {
							/*
							if(this.files.length>0) {
								$("#dropzone_caricato").val("1");
							} */
							$("#dropzone_caricato").val(this.files.length);
						});
						
						
                      
                    },
					<?php
					$url_handler = PATH.SLUG_PANEL."/media/actions.aj.php?action=upload";
					if(isset($token)) {
						$url_handler.="&token=".$token;
					}
					?>
                    url: "<?php echo $url_handler; ?>",
                    maxFilesize: <?php echo UPLOAD_MAX_FILESIZE; ?>,
                    acceptedFiles: "<?php echo $this->accepted_extensions; ?>",
					<?php
					if($this->max_num_files!==false) {
					?>
					maxFiles: <?php echo $this->max_num_files; ?>, //rimuovere questa opzione per consentire più file in upload
					
					maxfilesexceeded: function(file) { //rimuovere questa opzione per consentire più file in upload
						this.removeAllFiles();
						this.addFile(file);
						},
					<?php
						
					}
					?>
					
					                                
                     
                });
				
				
            });
             
            </script>




<?php
    }

}
