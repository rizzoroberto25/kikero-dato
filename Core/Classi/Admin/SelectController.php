<?php 
namespace Core\Classi\Admin;
use Core\Classi\Admin\HTML;

class SELECT_CONTROLLER extends HTML {

    public function __construct() {

    }

    public function printSelect(string $key, array $values, array $css_classes = [], array $attrs = []) {
        if(empty($key) || !count($values) > 0) return false;

        $css_class = "form-control";
        if(count($css_classes)>0) {
            foreach($css_classes as $class) {
                $css_class .= " ".$class;
            }
        }

        $html = "<select name=\"".$key."\" class=\"".$css_class."\"";

        if(count($attrs) > 0) {
            foreach($attrs as $kattr=>$vattr) {
                $html .= " ".$kattr."=\"".$vattr."\"";
            }
        }
        $html .= ">";
        $html .= "<option value=''>Select</option>";

        foreach($values as $v) {
            $selected = "";
            if($v['selected'] == true) {
                $selected = " selected='selected'";
            }
            $html .= "<option".$selected." value=\"".$v['value']."\">".$v['label']."</option>";
        }

        $html .= "</select>
            <div class=\"error-message alert alert-danger danger\"></div>
        ";

        $this->setHtml($html);
        $this->printHtml();

        return true;

    }

    public function printMultiSelect(string $key, array $values, array $css_classes = [], array $attrs = []) {
        if(empty($key) || !count($values) > 0) return false;

        $css_class = "form-control standardSelect";
        if(count($css_classes)>0) {
            foreach($css_classes as $class) {
                $css_class .= " ".$class;
            }
        }

        $html = "<select multiple name=\"".$key."[]\" class=\"".$css_class."\"";

        if(count($attrs) > 0) {
            foreach($attrs as $kattr=>$vattr) {
                $html .= " ".$kattr."=\"".$vattr."\"";
            }
        }
        $html .= ">";
        $html .= "<option value=''>Select</option>";

        foreach($values as $v) {
            $selected = "";
            if($v['selected'] == true) {
                $selected = " selected='selected'";
            }
            $html .= "<option".$selected." value=\"".$v['value']."\">".$v['label']."</option>";
        }

        $html .= "</select>
            <div class=\"error-message alert alert-danger danger\"></div>
        ";

        $this->setHtml($html);
        $this->printHtml();

        return true;

    }

}