<?php
namespace Core\Classi\Admin;

abstract class HTML {
    public $Html;
    public function __construct() {
        $this->Html = "";
    }
    protected function setHtml($html) {
        $this->Html = $html;
    }
    protected function addHtml($html) {
        $this->Html .= $html;
    }
    protected function getHtml() {
        return $this->Html;
    }
    protected function printHtml() {
        print($this->Html);
    }
}