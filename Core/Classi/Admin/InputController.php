<?php 
namespace Core\Classi\Admin;
use Core\Classi\Admin\HTML;

class INPUT_CONTROLLER extends HTML {

    public function __construct() {

    }

    public function printInput(string $key, array $css_classes = [], array $attrs = []) {

        if(empty($key)) return false;

        $css_class = "form-control";
        if(count($css_classes)>0) {
            foreach($css_classes as $class) {
                $css_class .= " ".$class;
            }
        }
 
        $html = "
            
                <input class=\"".$css_class."\" name=\"".$key."\"
        ";

        if(count($attrs) > 0) {
            foreach($attrs as $kattr=>$vattr) {
                $html .= " ".$kattr."=\"".$vattr."\"";
            }
        }

        $html .= " />
            <div class=\"error-message alert alert-danger danger\"></div>
        ";

        $this->setHtml($html);
        $this->printHtml();

        return true;

    }

    public function printInputWithIcon(string $key, string $icon, array $css_classes = [], array $attrs = []) {

        if(empty($key) || empty($icon)) return false;

        $css_class = "form-control";
        if(count($css_classes)>0) {
            foreach($css_classes as $class) {
                $css_class .= " ".$class;
            }
        }

        $html = "
            <div class=\"input-group\">
                <div class=\"input-group-addon\"><i class=\"".$icon."\"></i></div>
                <input class=\"".$css_class."\" name=\"".$key."\"
        ";

        if(count($attrs) > 0) {
            foreach($attrs as $kattr=>$vattr) {
                $html .= " ".$kattr."=\"".$vattr."\"";
            }
        }

        $html .= " /></div>
            <div class=\"error-message alert alert-danger danger\"></div>
        ";

        $this->setHtml($html);
        $this->printHtml();

        return true;

    }

}