<?php
namespace Core\Classi\Admin;
use Core\Classi\PAGINATION;

class PAGINATION_HTML extends PAGINATION {
	
	public $PathPanel;
	
	public function getHtml() {
		
		$pagina_sel = $this->page;
		$tot_pagine = $this->totPages;
		$miapagina = $this->currentUrl;
		
		if($this->hasHtaccess !== false) {

			$miapagina.="/";
			
		} else {
			if(strpos($miapagina, "?")!==false) {
				$miapagina .= "&page=";
			} else {
				$miapagina .= "?page=";	
			}
		}
		
		$html = "
			<div class=\"dataTables_paginate paging_simple_numbers\" id=\"bootstrap-data-table-export_paginate\">
			<ul class=\"pagination\">
		";
		
		if($tot_pagine<9) {
			
			for($i=1; $i<=$tot_pagine; $i++) {
				if($i!=$pagina_sel) {
					
					$html .= "<li class=\"paginate_button page-item\"><a href='".$miapagina.$i."' class='page-link pagina'>".$i."</a></li>";
					
				} else {
					$html .= "<li class='paginate_button page-item active'><a href='#' class='page-link pagina' onclick='return false;'>".$i."</a></li>";
				}
			}
			
		} else {
			if($pagina_sel<6) {
				
				for($i=1; $i<=8; $i++) {
					if($i!=$pagina_sel) {
						
						$html .= "<li class=\"paginate_button page-item\"><a href='".$miapagina.$i."' class='page-link pagina'>".$i."</a></li>";	
						
					} else {
						$html .= "<li class='paginate_button page-item active'><a href='javascript:void(0)' class='page-link pagina'>".$i."</a></li>";
					}

				}
				$html .= "<li class='paginate_button page-item' aria-label=\"Last page\"><a href='".$miapagina.$tot_pagine."' class='page-link pagina'><i class=\"fa fa-angle-right\"></i> <i class=\"fa fa-angle-right\"></i></a></li>";
				
			} elseif($pagina_sel>=6 && $pagina_sel<($tot_pagine-3)) {
				$prima_pagina=$pagina_sel-3;
				$ultima_pagina=$pagina_sel+3;

				$html .= "<li class='paginate_button page-item' aria-label=\"First page\"><a href='".$miapagina."1' class='page-link pagina'><i class=\"fa fa-angle-left\"></i> <i class=\"fa fa-angle-left\"></i></a></li>";
				

				for($i=$prima_pagina; $i<=$ultima_pagina; $i++) {
					if($i!=$pagina_sel) {
						$html .= "<li class='paginate_button page-item'><a href='".$miapagina.$i."' class='page-link pagina'>".$i."</a></li>";
							
						
					} else {
						$html .= "<li class='paginate_button page-item active'><a href='#' class='page-link pagina' onclick='return false;'>".$i."</a></li>";
						
					}


				}
				$html .= "<li class='paginate_button page-item' aria-label=\"Last page\"><a href='".$miapagina.$tot_pagine."' class='page-link pagina'><i class=\"fa fa-angle-right\"></i> <i class=\"fa fa-angle-right\"></i></a></li>";
				
				
			} elseif($pagina_sel>=($tot_pagine-3)) {
		
				$html .= "<li class='paginate_button page-item' aria-label=\"First page\"><a href='".$miapagina."1' class='page-link pagina'><i class=\"fa fa-angle-left\"></i> <i class=\"fa fa-angle-left\"></i></a></li>";
				$prima_pagina=$pagina_sel-4;

				for($i=$prima_pagina; $i<=$tot_pagine; $i++) {
					if($i!=$pagina_sel) {
						$html .= "<li class='paginate_button page-item'><a href='".$miapagina.$i."' class='page-link pagina'>".$i."</a></li>";
						
						
					} else {
						$html .= "<li class='paginate_button page-item active'><a href='#' class='page-link pagina' onclick='return false;'>".$i."</a></li>";
						
					}


				}

			}
		}
		
		$html .= "</ul></div>";
		
		return $html;
		
		/*
		return '
		<div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table-export_paginate"><ul class="pagination">
		<li class="paginate_button page-item previous disabled" id="bootstrap-data-table-export_previous"><a href="#" aria-controls="bootstrap-data-table-export" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="bootstrap-data-table-export" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="bootstrap-data-table-export" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="bootstrap-data-table-export" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="bootstrap-data-table-export" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="bootstrap-data-table-export" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item "><a href="#" aria-controls="bootstrap-data-table-export" data-dt-idx="6" tabindex="0" class="page-link">6</a></li><li class="paginate_button page-item next" id="bootstrap-data-table-export_next"><a href="#" aria-controls="bootstrap-data-table-export" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li></ul></div>';
		*/
	}
	
	public function getRowsSelector($options = [5, 10, 20, 50, 100]) {
		
		$html = "
			<form name=\"f_rows_selector\" method='get' action='".$this->PathPanel."'>
		
		";
		
		
		$html .= "</form>";
		
	}
	
}