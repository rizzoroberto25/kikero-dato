<?php
namespace Core\Classi\Admin;

include_once(ROOT."Core/Classi/Models/GeoPlace.php");
use Core\Classi\Models\GEO_PLACE;

class GEO_CONTROLLER extends HTML {

    public $GeoPlace;

    public function __construct($nation = GEO_DEFAULT_NATION) {
        parent::__construct();
        $this->GeoPlace = new GEO_PLACE(GEO_DEFAULT_NATION);
    }

    public function printGeolocationForm($GeoData = NULL) {

        $comune = !empty($GeoData->comune) ? $GeoData->comune : "";
        $comune_geoid = !empty($GeoData->comune_geoid) ? $GeoData->comune_geoid : "";
        $indirizzo = !empty($GeoData->indirizzo) ? $GeoData->indirizzo : "";
        $cap = !empty($GeoData->cap) ? $GeoData->cap : "";
        $luogo = !empty($GeoData->luogo) ? $GeoData->luogo : "";
        $latitudine = !empty($GeoData->latitudine) ? $GeoData->latitudine : "";
        $longitudine = !empty($GeoData->longitudine) ? $GeoData->longitudine : "";

        $html = "
        
            <div class='form-group geolocation comune'>
                <label>Comune</label><br />
                <input type=\"text\" name=\"comune\" class='form-control' placeholder='Cerca' value=\"".$comune."\" />
                <input type=\"hidden\" name='comune_geoid' value='".$comune_geoid."' /> 
            </div>

            <script>

            jQuery(document).ready(function() {
                //console.log(\"Documento pronto\");
                var $ = jQuery;
                var selected_location = '';
                $(\".geolocation input[name='comune']\").autocomplete({

                    source: function (request, response) {
			
                        $.ajax({
                            type: \"GET\",
                            url: valrootPanel+\"contents/geo_aj.php\",
                            data: \"action=cerca_comune&stringa=\"+$(\".geolocation input[name='comune']\").val()+\"&nation=".GEO_DEFAULT_NATION."\",
                            success: function (data) {
                                //codePrint(data, dati);
                                response(data);
                            },
                            dataType: 'json',
                            minLength: 3,
                            delay: 100
                        });
                    },
                    minLength: 3,
                    select: function( event, ui ) {
                        //log( \"Selected: \" + ui.item.value + \" aka \" + ui.item.id );
                        console.log(\"Seleziono: \" + ui.item.id);
                        //selectSearchedUser(ui.item.id, ui.item.label)
                        selected_location = ui.item.value;
                        $(\".geolocation input[name='comune_geoid']\").val(ui.item.id);
                        $(\".geolocation input[name='comune']\").removeClass('is-invalid');
                        $(\".geolocation input[name='comune']\").addClass('is-valid');
                        
                    }



                });

                $(\".geolocation input[name='comune']\").focus(function() {
                    if($(\".geolocation input[name='comune_geoid']\").val() != '') {
                        $(this).val('');
                        $(\".geolocation input[name='comune']\").removeClass('is-valid');
                        $(\".geolocation input[name='comune_geoid']\").val('');
                    }
                });

                $(\".geolocation input[name='comune']\").focusout(function() {
                    if($(\".geolocation input[name='comune']\").val() != \"\") {
                        if($(\".geolocation input[name='comune']\").val() != selected_location || $(\".geolocation input[name='comune_geoid']\").val() == '') {
                            $(\".geolocation input[name='comune']\").removeClass('is-valid');
                            
                            if($(\".geolocation input[name='comune_geoid']\").val() != '') {
                                $(\".geolocation input[name='comune_geoid']\").val('');
                                $(\".geolocation input[name='comune']\").addClass('is-invalid');
                            }
                        }
                    } else {
                        if($(\".geolocation input[name='comune_geoid']\").val() != '') {
                            $(\".geolocation input[name='comune_geoid']\").val('');
                            $(\".geolocation input[name='comune']\").addClass('is-invalid');
                        }
                    }
                });

            });

            /*
            function cerca_comune(elm) {
                
                var stringa = $(elm).val();
                if(stringa.length > 3) {
                    setTimeout(function() {
                        $.ajax({
                            url : valrootPanel+\"contents/geo_aj.php\",
                            type: \"get\",
                            data: \"action=cerca_comune&stringa=\"+stringa+\"&nation=".GEO_DEFAULT_NATION."\",
                            dataType: \"json\",
                            success: function(response) {
                                codePrint(response, 'response comuni');
                                
                            },
                            error: function(event) {
                                codePrint(event, \"Errore chiamata\");
                            }
                        });    
                    }, 200);

                }

               
            }
            */
            </script>

            <div class='form-group geolocation indirizzo'>
                <label>Indirizzo</label><br />
                <input type=\"text\" name=\"indirizzo\" class='form-control' value=\"".$indirizzo."\" />
            </div>

            <div class='form-group geolocation cap'>
                <label>Cap</label><br />
                <input type=\"text\" name=\"cap\" class='form-control' value=\"".$cap."\" />
            </div>

            <div class='form-group geolocation luogo'>
                <label>Location</label><br />
                <input type=\"text\" name=\"luogo\" class='form-control' value=\"".$luogo."\" />
            </div>

            <div class='col-sm-6'>
                <div class='form-group geolocation latitudine'>
                    <label>Latitudine</label><br />
                    <input type=\"text\" name=\"latitudine\" class='form-control' value=\"".$latitudine."\" />
                </div>
            </div>
            <div class='col-sm-6'>
                <div class='form-group geolocation longitudine'>
                    <label>Longitudine</label><br />
                    <input type=\"text\" name=\"longitudine\" class='form-control' value=\"".$longitudine."\" />
                </div>
            </div>
        
        ";

        $this->setHtml($html);
        $this->printHtml();

    }

}