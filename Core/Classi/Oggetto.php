<?php
namespace Core\Classi;
if(!defined("ROOT")) exit();

use Core\Classi\MODEL;
use Core\Classi\UTILITY;
use Core\Classi\DB;
use Core\Classi\LINGUA;

abstract class OGGETTO extends MODEL {
	
	public $transFields;
	public $transTable;
	public $Translations;
	public $Lingua;
	
	public function __construct($transFields) {
		
		if(!empty($transFields) && is_array($transFields)) {
			$this->transFields = $transFields;
		}
		
		$this->initModel($this->table);
		$this->transTable = $this->table .= "_trans";
		
	}
	
	protected function setLingua($lingua) {
		$this->lingua = new LINGUA($lingua);
	}
	
	protected function istanzia($id) {
		
		/***************************************
		Restituisce array
		$oggetto->model = MODEL
		$oggetto->Translations = array(
			$field => array(
				"it" => $traduzione_it,
				"en" => $traduzione_en,
			}
		)
		***************************************/
		
		$id = (int) $id;
		$oggetto['Model'] = parent::istanzia($id);
		/*
		$query = "SELECT * FROM ".$this->transTable." WHERE id=".$this->id;
		$db = new DB();
		$db->query = $query;
		$res = $db->do_query();
		*/
		$res = $this->getTranslations(); 
		
		if(is_null($res)) {
			$this->Translations = NULL;
		} else {
			
			foreach($res as $row) {
				$this->Translations[$row['field']][$row['lingua']] = $row['translation'];
			}
			
		}
		
		$oggetto['Translations'] = $this->Translations;
		return $oggetto;
		
	}
	
	protected function show($id, $lingua) {
		
		$id = (int) $id;
		$oggetto = parent::istanzia($id);
		
		$this->setLingua($lingua);
		if($this->Lingua->isActive()) {

			$res = $this->getTranslations();
			
			foreach($res as $row) {
				$oggetto[$row['field']] = $row['translation'];
			}
			
		}
		
		return $oggetto;
		
	}
	
	protected function getTranslations($id) {
		if(!empty($id)) {
			$this->id = (int) $id;
		}
		if(empty($this->id)) return false;
		
		$query = "SELECT * FROM ".$this->transTable." WHERE id=".$this->id;
		if(!empty($this->Lingua->lingua)) {
			$query .= " AND lingua='".$this->Lingua->lingua."'";
		}
		$db = new DB();
		$db->query = $query;
		$res = $db->do_query();
		
		return $res;
	}
	
}