<?php
namespace Core\Classi;
if(!defined("ROOT")) exit();

use Core\Classi\DATO;
use Core\Classi\DB;

abstract class MODEL {
	
	public $id;
	public $created_at;
	public $updated_at;
	public $deleted_at;
	public $props;
	public $columns;
	public $table;
	
	public $order;
	public $page;
	public $numberForPage;
	public $limit;
	public $dataTypes;
	protected $EspressioniRegolari;
	
	public $position;
	public $bondPosition;
	public $bondPositionValue;
	
	public $queryString;
	
	// public function __construct($table) {
		
		// if(empty($table)) return false;
		// $this->order = " ORDER BY id";
		// $this->limit = false;
		// $this->page = false;
		// $this->props = array();
		// $this->columns = array();
		// $this->db_table_schema();		
		
	// }
	
	public function initModel($table) {
		
		if(empty($this->table)) return false;
		$this->order = " ORDER BY id";
		$this->limit = false;
		$this->page = false;
		$this->props = array();
		$this->columns = array();
		$this->db_table_schema();

		$this->dataTypes = array(
			"varchar" => "string",
			"text" => "string",
			"char" => "string",
			"int" => "integer",
			"bigint" => "integer",
			"tinyint" => "integer",
			"smallint" => "integer",
			"mediumint" => "integer",
			"date" => "date",
			"float" => "float",
			"blob" => "string",
			"timestamp" => "timestamp",
			"longtext" => "string",
			"longblob" => "string",
			"datetime" => "datetime",
		);
		
		$this->numberForPage = 1;
		
	}
	
	protected function setEspressioniRegolari($array = array()) {
		if(!is_array($array)) {
			return false;
		}
		foreach($array as $key => $val) {
			$this->EspressioniRegolari[$key] = $val;
		}
	}
	
	public function getEspressioniRegolari() {
		return $this->EspressioniRegolari;
	}
	
	public function istanzia($id) {
		
		$id = (int) $id;
		$db = new DB();
		$res = $db->get($this->table, $id);
		
		if(!is_null($res)) {
			foreach($res as $k=>$v) {
				$this->{$k} = $v;
			}
		}
		
		return $res;
		
	}
	
	public function getFromField($field, $value) {
		$db = new DB();
		$res = $db->getFromField($this->table, $field, $value);
		if(!is_null($res)) {
			foreach($res as $k=>$v) {
				$this->{$k} = $v;
			}
		}
		
		return $res;
	}
	
	protected function setQueryString($query) {
		$this->queryString = $query;
	}
	
	public function lista($filtri=array()) {
		
		$query = "
			SELECT * FROM ".$this->table."
			WHERE id>0
		";
		
		if(count($filtri)>0) {
			foreach($filtri as $filtro) {
				
				$query .= " AND ".$filtro['field']."".$filtro['operator']."\"".$filtro['value']."\"";
				
			}
		}
		
		$this->setQueryString($query);
		
		$query .= " ".$this->order;
		$db = new DB($this->limit);
		$db->query = $query;
		return $db->do_query($this->page);
		
	}
	
	public function listaQuery($filtri = NULL) {
		
		$query = "
			SELECT * FROM ".$this->table."
			WHERE id>0
		";
		
		if(!is_null($filtri)>0) {
			foreach($filtri as $filtro) {
				
				$query .= " ".$filtro;
				
			}
		}
		
		$this->setQueryString($query);
		
		$query .= " ".$this->order;		
		$db = new DB($this->limit);
		$db->query = $query;
		return $db->do_query($this->page);
		
	}
	
	public function paginate() { // $rows è il numero di righe per pagina
		if(empty($this->queryString)) return false;
		if(empty($this->page) || !is_int($this->page)) return false;
		
		$db = new DB(false);
		$db->query = $this->queryString;
		$res = $db->do_query();
		if(is_null($res)) return false;
		
		$paginazione=array();
		
		$paginazione['tot_righe'] = count($res);
		$paginazione['tot_pagine'] = ceil(count($res) / $this->limit);
		
		return $paginazione;
	}
	
	protected function hasColumn($column) {
		if(empty($column)) return false;
		if(empty($this->columns)) return false;
		if(!in_array($column, $this->columns)) {
			return false;
		}
		return true;
	}
	
	public function store($data, $want_id = true) {
		
		$response = array(
			"result" => false,
			"error" => "",
			"data" => NULL,
		);
		
		if($this->check_input_data($data)) {
			
			$fieldsQuery = "";
			$valuesQuery = "";
			
			foreach($data as $field=>$value) {
				
				$fieldsQuery .= $field.", ";

				if(!$this->hasColumn($field)) {
					$response['error'] = "errors.uncorret_data_type";
					return $response;
				}
				$dataType = $this->props[$field]['DATA_TYPE'];
				
				if(array_key_exists($dataType, $this->dataTypes)) {
					
					switch($this->dataTypes[$dataType]) {
						
						case "string":
							$valuesQuery .= "'".addslashes($value)."', ";
							break;

						case "text":
							$valuesQuery .= "'".addslashes($value)."', ";
							break;
							
						case "integer":
							$valuesQuery .= (int)$value.", ";
							break;
							
						case "float":
							$valuesQuery .= (float)$value.", ";
							break;
							
						case "date":
							$value = !empty($value)?$value:"0000-00-00";
							$valuesQuery .= "'".$value."', ";
							break;

						case "datetime":
							$value = !empty($value)?$value:"0000-00-00 00:00:00";
							$valuesQuery .= "'".$value."', ";
							break;
							
						case "timestamp":
							$valuesQuery .= "'".$value."', ";
							break;
							
						default:
							$valuesQuery .= "'".$value."', ";
							break;
					}
					
				} else {
					
					$valuesQuery .= "'".$value."', ";
				
				}
			}
			
			$fieldsQuery = substr($fieldsQuery, 0, strlen($fieldsQuery)-2);
			$valuesQuery = substr($valuesQuery, 0, strlen($valuesQuery)-2);
			
			$query = "INSERT INTO ".$this->table."(".$fieldsQuery.") VALUES(".$valuesQuery.")"; //die("Query: " . $query);
			$db = new DB();
			$db->query = $query;

			if($want_id===true) {

				$id = $db->do_action("id");
				
				$this->istanzia($id);
				
				$maxPosition = $this->MinMaxPosition("MAX");
				if($maxPosition !== false) {
					$maxPosition++;
					$this->setPosition($maxPosition);
				}
				
				$response = array(
					"result" => true,
					"error" => NULL,
					"data" => $this,
				);
			} else {
				$db->do_action(false);

				$maxPosition = $this->MinMaxPosition("MAX");
				if($maxPosition !== false) {
					$maxPosition++;
					$this->setPosition($maxPosition);
				}
				
				$response = array(
					"result" => true,
					"error" => NULL,
					"data" => NULL,
				);
			}
			
			
		} else {
			//$response['error'] = "uncorret_data_type";
			$response['error'] = "errors.uncorret_data_type";
		}
		
		return $response;
		
	}
	
	public function update($data, $where = NULL) {
		
		if(empty($this->id) || empty($this->table)) { return false; }
		
		$this->id = (int) $this->id;
		$response = array(
			"result" => false,
			"error" => "",
			"data" => NULL,
		);
		
		if(self::check_input_data($data)) {  
			
			$queryUpdates = "";
			
			foreach($data as $field=>$value) {

				if(!$this->hasColumn($field)) {
					//die("Colonna: ".$field);
					//$response['error'] = "uncorret_data_type";
					$response['error'] = "errors.uncorret_data_type";
					return $response;
				}
				$dataType = $this->props[$field]['DATA_TYPE'];
				
				if(array_key_exists($dataType, $this->dataTypes)) {	
					switch($dataType) {
						
						case "string":
							$queryUpdates .= $field . "='".addslashes($value)."', ";
							break;

						case "text":
							$queryUpdates .= $field . "='".addslashes($value)."', ";
							break;
							
						case "integer":
							$queryUpdates .= $field . "=" .(int)$value.", ";
							break;
							
						case "float":
							$queryUpdates .= $field . "=" .(float)$value.", ";
							break;
							
						case "date":
							$value = !empty($value)?$value:"0000-00-00";
							$queryUpdates .= $field . "=" ."'".$value."', ";
							break;

						case "datetime":
							$value = !empty($value)?$value:"0000-00-00 00:00:00";
							$queryUpdates .= $field . "=" ."'".$value."', ";
							break;
							
						case "timestamp":
							$queryUpdates .= $field . "=" ."'".$value."', ";
							break;
							
						default:
							$queryUpdates .= $field . "=" ."'".$value."', ";
							break;
					}
				}
				
			}

			/*	
			if(!$this->hasColumn("updated_at")) {
				$queryUpdates .= " updated_at = '".date("Y-m-d h:i:s", time())."'";
			} else {
				$queryUpdates = substr($queryUpdates, 0, strlen($queryUpdates)-2);
			}
			*/
			//die("Query updates: ".$queryUpdates);
			$queryUpdates = substr($queryUpdates, 0, strlen($queryUpdates)-2);
			$query = "UPDATE ".$this->table." SET ".$queryUpdates."";
			
			if($this->hasColumn("updated_at")) {
				$query .= ", updated_at = ".date("Y-m-d h:i:s", time())."";
			}
			
			$query .= " WHERE id=".$this->id;
			if(!is_null($where)) {
				$query .= " AND ".$where;
			}
			$db = new DB();
			$db->query = $query;
			$res = $db->do_action();
			
			if($res === false) {
				//$response['error'] = "errore_query";
				$response['error'] = "errors.errore_query";
			} else {
				$this->istanzia($this->id);
				$response['result'] = true;
			}
			
		} else {
			//$response['error'] = "uncorret_data_type";
			$response['error'] = "errors.uncorret_data_type";
		}
		
		return $response;
		
	}
	
	public function delete() {
		
		if(empty($this->id) || empty($this->table)) return false;
		
		$this->id = (int) $this->id;
		if(!in_array("deleted_at", $this->props['columns'])) {
			return $this->remove();
		} else {
			$query = "UPDATE ".$this->table." SET deleted_at='".date("Y-m-d h:i:s", time())."' WHERE id=".$this->id;
			$db = new DB();
			$db->query = $query;
			return $db->do_action();
		}
				
	}
	
	public function remove() {
		
		if(empty($this->id) || empty($this->table)) return false;
		
		$this->id = (int) $this->id;
		$query = "DELETE FROM ".$this->table." WHERE id=".$this->id;
		$db = new DB();
		$db->query = $query;
		return $db->do_action();
				
	}

	public function removeWhere($filters) {
		if(!empty($filters)) {

			$base_query = "DELETE FROM ".$this->table." WHERE id>0";
			$query = $base_query;

			if(is_array($filters)) {
				if(count($filters)>0) {
					foreach($filters as $filtro) {
						
						$query .= " AND ".$filtro['field']."".$filtro['operator']."\"".$filtro['value']."\"";
						
					}
				}
			} elseif(is_string($filters)) {
				$query .= $filters;
			}
			
			if($query == $base_query) return false;

			$db = new DB();
			$db->query = $query;
			return $db->do_action();
		}
		return NULL;
	}
	
	protected function db_table_schema() {
		$query = "
			SELECT * from INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME='".$this->table."'
		";
		$db = new DB();
		$db->query = $query;
		$res = $db->do_query();
		
		foreach($res as $column=>$data) {
			$this->props[$data['COLUMN_NAME']] = $data;
			if(!in_array($data['COLUMN_NAME'], $this->columns)) $this->columns[] = $data['COLUMN_NAME'];

		}
	}
	
	/****************************************
	Check sui tipi di dato : START
	
	Es. post
	$post = array(
		"textF" => "Alfredo",
		"dateF" => "2018-10-20",
	);
	****************************************/
	
	protected function check_input_data($post) {
		
		if(!is_array($post)) {
			return false;
		}  
		
		$check = true;
		
		foreach($post as $k=>$v) {
			if(!array_key_exists($k, $this->props)) return false;
			$dataType = $props[$k]['COLUMN_TYPE'];
			
			if(array_key_exists($dataType, $this->dataTypes)) {
				
				$controlField = $this->dataTypes[$dataType];
				
				switch($controlField) {
					
					case "string":
						if(!is_string($v)) { die("string:".$k); $check = false; }
						break;
						
					case "integer":
						if(!is_integer($v)) { die("integer:".$k); $check = false; }
						break;
						
					case "date":
						if(!DATO::validateDate($v)) { die("date:".$k); $check = false; }
						break;
						
					case "float":
						if(!is_float($v)) { die("float:".$k); $check = false; }
						break;
						
					case "timestamp":
						if(!DATO::validateDate($v)) { die("timestamp:".$k); $check = false; }
						break;
				}
				
			}
		}
		
		return $check;
		
	}
	
	/****************************************
	Check sui tipi di dato : END
	****************************************/
	
	
	/****************************************
	Check sulle espressioni regolari : START
	****************************************/
	
	public function check_regular_expressions($data) {
		
		if(!is_array($this->EspressioniRegolari) || count($this->EspressioniRegolari) == 0) {
			return array("check" => true);
		}
		
		$expressionErrors = array();
		
		foreach($data as $k=>$v) {
			if(array_key_exists($k, $this->EspressioniRegolari) && isset($data[$k])) {
				$Dato = new DATO($v, $this->EspressioniRegolari[$k]);
				if(!$Dato->isRegulerExp()) { 
					echo "check fallito: ".$k."<br />";
					array_push($expressionErrors, $k);
				}
			}
		}
		
		if(count($expressionErrors) > 0) {
			$check = false;
		} else {
			$check = true;
		}
		
		return array("check" => $check, "fields" => $expressionErrors);
		
	}
	
	/****************************************
	Check sulle espressioni regolari : END
	****************************************/
	
	
	/*********************************************
	Gestione ordine posizionamento - START
	**********************************************/
	
	public function setPosition(int $val) {
		
		if(empty($this->id)) return false;
		if(empty($val)) return false;
		if(!$this->hasColumn("position")) return false;
		
		$new_position = $val;
		$operator_search = ">=";
		if($new_position > $this->position) {
			$new_position++;
			$operator_search = ">";
		}
		$values = ["position" => $new_position];
		$this->update($values);
		
		$filtri = array(
			0 => array(
				"field" => "position",
				"operator" => $operator_search,
				"value" => $val,
			),
			1 => array(
				"field" => "id",
				"operator" => "!=",
				"value" => $this->id,
			),
		);
		if(!empty($this->bondPosition) && $this->hasColumn($this->bondPosition) && !empty($this->bondPositionValue)) {
			$filtri[2] = array(
				"field" => $this->bondPosition,
				"operator" => "=",
				"value" => $this->bondPositionValue,
			);
		}
		$res = $this->lista($filtri);
		
		if($res !== false && count($res)>0) {
			foreach($res as $row) {
				$position = $row['position'] + 1;
				$query = "UPDATE ".$this->table." SET position = ".$position."";
				if($this->hasColumn("updated_at")) {
					$query .= ", updated_at = ".date("Y-m-d h:i:s", time())."";
				}
				$query .= " WHERE id=".$row['id'];
				$db = new DB();
				$db->query = $query;
				if(!$db->do_action()) return false;
			}
		}
		$this->reorderPositions();
		
	}

	public function setElmsPositions(array $elms) {
		if(!count($elms)>0) return false;
		if(!$this->hasColumn("position")) return false;
		foreach($elms as $id=>$position) {
			if(empty($position)) continue;
			$id = (int) $id;
			$position = (int) $position;
			$query = "UPDATE ".$this->table." SET position = ".$position." WHERE id=".$id;
			$db = new DB();
			$db->query = $query;
			if(!$db->do_action()) return false;
		} 
	}
	
	public function reorderPositions() {
		if(!$this->hasColumn("position")) return false;
		
		$filtri = array();
		if(!empty($this->bondPosition) && $this->hasColumn($this->bondPosition) && !empty($this->bondPositionValue)) {
			$filtri[0] = array(
				"field" => $this->bondPosition,
				"operator" => "=",
				"value" => $this->bondPositionValue,
			);
		}
		$this->order = "ORDER BY position ASC";
		$res = $this->lista($filtri);
		if(count($res) > 0) {
			$position = 1;
			foreach($res as $row) {
				
				$query = "UPDATE ".$this->table." SET position = ".$position."";
				if($this->hasColumn("updated_at")) {
					$query .= ", updated_at = ".date("Y-m-d h:i:s", time())."";
				}
				$query .= " WHERE id=".$row['id'];
				$db = new DB();
				$db->query = $query;
				
				if(!$db->do_action()) return false;
				
				$position++;
			}
		}
	}
	
	public function MinMaxPosition($cosa) {
		
		if(!$this->hasColumn("position")) {
			return false; 
		}
		if($cosa != "MIN" && $cosa != "MAX") {
			return false;
		}
		
		$query = "SELECT ".$cosa."(position) as result FROM ".$this->table." WHERE id>0";
		if(!empty($this->bondPosition) && $this->hasColumn($this->bondPosition) && !is_null($this->bondPositionValue)) {
			$query .= " AND ".$this->bondPosition."='".$this->bondPositionValue."'";
		}
		$db = new DB();
		$db->query = $query;
		$res = $db->do_query();
		
		if($res === false) return 0;
		return $res[0]['result'];
	}
	
	/*********************************************
	Gestione ordine posizionamento - END
	**********************************************/

	protected function checkIfExistsTable($table_name):bool {
		$db = new DB();
		$db->query = 'select 1 from '.$table_name.' LIMIT 1';
		$res = $db->do_query();
		
		if($res === false) return false;
		return true;
	}

	protected function doQuery() {
		if(empty($this->queryString)) return false;
		$db = new DB($this->limit);
		$db->query = $this->queryString;
		return $db->do_query($this->page);
	}

}
?>