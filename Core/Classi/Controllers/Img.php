<?php
namespace Core\Classi\Controllers;
if(!defined("ROOT")) exit();
use Core\Classi\Models\IMG as IMG_MODEL;
use Core\Classi\CONTROLLER;
use Core\Classi\UTILITY;
use Core\Classi\DB;
use Core\Classi\UPLOAD;
use Core\Classi\Models\MEDIA_MODEL;

class IMG extends CONTROLLER {

	const IMAGE_EXTENSIONS = "jpg,jpeg,png,gif";
	
	public function __construct(int $id = NULL) {
		$this->setResponse();
		$this->setModel(new IMG_MODEL($id));
	}
	
	public function getImage() {
		/*
		if(empty($this->Model->filename)) return NULL;
		return $this->Model->filename;
		*/
		return $this->Model->getImage();
	}

	public function getTheImage($width, $is_avatar = false) {
		
		if(empty($this->Model->id)) {
			return NULL;
		}
		
		$width_resizes = explode(",", IMG_WIDTH_RESIZES);
		if(!in_array($width, $width_resizes)) {
			return NULL;
		}

		$Image = $this->getImage();
		$Path = "uploads/".PROJECT_UPLOAD_FOLDER."/img/";
		if($is_avatar) {
			$Path .= "avatar/";
		}
		$Path .= $width."/";
		
		if(!self::isImage(ROOT.$Path.$Image)) { die(ROOT.$Path.$Image);
			return NULL;
		}
		
		return $Path.$Image;

	}
	
	public function storeImg($files, $input_file_name, $tag = "", $is_avatar = 0) {
		
		$post_img=false;
		
		if(is_uploaded_file($files[$input_file_name]['tmp_name'])) {
			$temp = ROOT."temp_foto/".session_id()."_".$files[$input_file_name]['name'];
			$source_img = $files[$input_file_name]['tmp_name'];
			$foto_name = UTILITY::adatta_nomeurl($files[$input_file_name]['name']);
			$post_img = true;	
		} else {
			$responseStore = array(
				"result" => false,
				"error" => "file.errors.no-file",
				"data" => NULL,
			);
			$this->setResponse($responseStore);
			return;
		}
		
		if($post_img===true) {
			
			copy($source_img, $temp);
			$Upload = new UPLOAD("img");
			if($Upload->ctl_tipo_file($temp)) {
				
				$nome_img = time()."_".$foto_name;
				
				$store_data = array(
					"filename" => $nome_img,
					"avatar" => $is_avatar,
					"tag" => UTILITY::proteggiDB($tag),
					"original_file_name" => UTILITY::proteggiDB($files[$input_file_name]['name']),
				);
				$responseStore = $this->Model->store($store_data);
				$this->setResponse($responseStore);
				
				if($this->getResponse['esito'] === false) return;
				
				$dir = ROOT_UPLOAD."img/";
				if($is_avatar == 1) {
					$dir .= "avatar/";
				}
				$Upload->ritaglia_foto($temp, $nome_img, $dir);
				unlink($temp);
				
				return;
				
			} else {
				$response = array(
					"result" => false,
					"error" => "file.errors.no-file",
					"data" => NULL,
				);
				$this->setResponse($responseStore);
				return;
			}
			
		} else {
			$response = array(
				"result" => false,
				"error" => "file.errors.no-file",
				"data" => NULL,
			);
			$this->setResponse($responseStore);
			return;
		}
		
	}
	
	public function remove_img() {
		
		$response = array(
			"result" => false,
			"error" => "",
			"data" => NULL,
		);
		
		if(empty($this->Model->id)) {
			$response['error'] = "img_not_instantiated";
			$this->setResponse($response);
			return;
		}
		
		$filePath = $this->getImage();
		$RootOfImage = ROOT_UPLOAD."img/";
		if($this->Model->avatar) {
			$RootOfImage .= "avatar/"; 
		}
		
		$Upload = new UPLOAD("img");
		
		foreach($Upload->get_pictures_dirs() as $dir) {
			if(file_exists($RootOfImage.$dir."/".$filePath)) {
				unlink($RootOfImage.$dir."/".$filePath);
			}
		}
		
		if(!$this->Model->remove()) {
			$response["error"] = "deleting_db_error";
		} else {
			$response['result'] = true;
		}

		$this->setResponse($response);
	}

	public function storeTMPUploads(array $records, $is_avatar = false) {

		$response = array(
			"result" => false,
			"error" => "",
			"data" => NULL,
		);
		if(count($records) == 0) {
			$response['error'] = "no_temp_records";
			$this->setResponse($responseStore);
			return;
		}

		$Upload = new UPLOAD("img");

		$avatar = 0;
		if($is_avatar) {
			$avatar = 1;
		}

		foreach($records as $tmp) {
			$thisTime = time();
			$tempFile = TEMPDIR.$tmp['source'];
			$fileInfo = pathinfo($tempFile);
			$fileNameImg = $thisTime."_".UTILITY::adatta_nomeurl($tmp['filename']);
			if($Upload->ctl_tipo_file($tempFile)) {
				$store_data = array(
					"filename" => $fileNameImg,
					"avatar" => $avatar,
					"tag" => NULL,
					"original_file_name" => UTILITY::adatta_nomeurl($tmp['filename']),
					"file_info" => $fileInfo['extension'],
					"file_type" => mime_content_type ($tempFile),
				);
				$responseStore = $this->Model->store($store_data);
				if($responseStore['esito'] === false) {
					$this->setResponse($responseStore );
					return;
				} 

				$dir = ROOT_UPLOAD."img/";
				if($is_avatar == 1) {
					$dir .= "avatar/";
				}
				$Upload->ritaglia_foto($tempFile, $fileNameImg, $dir);
				//unlink($tempFile);

			} else {
				$response = array(
					"result" => false,
					"error" => "no-file",
					"data" => NULL,
				);
				$this->setResponse($responseStore);
				return;
			}
		} 

		$response['result'] = true;
		$this->setResponse($response);
		return;
	}

	public function update_tag(string $tagString) {
		if(empty($this->Model->id)) {
			$response = array(
				"result" => false,
				"error" => "no-img",
				"data" => NULL,
			);
			$this->setResponse($responseStore);
			return;
		}
		$this->setResponse($this->Model->update(["tag"=>UTILITY::proteggiDB($tagString)]));
		
		return;
	}

	public function save_gallery(string $model, int $model_id, array $medias) {
		$MediaModel = new MEDIA_MODEL();
		if(count($medias)>0) {
			foreach($medias as $media_id) {
				$media_id = (int) $media_id;
				if($media_id>0) {
					$post = [
						"media_id" => $media_id,
						"model" => $model,
						"model_id" => $model_id,
					];
					$response = $MediaModel->storeRecord($post);
				}
				
			}
		} else {
			$response = array(
				"result" => false,
				"error" => "no-img",
				"data" => NULL,
			);
			$this->setResponse($response);
		}
	}

	public function getModelGallery(string $model, int $model_id, $img_resize="crop") {
		if(!$model_id > 0) {
			return false;
		}
		$MediaModel = new MEDIA_MODEL();
		$Gallery = $MediaModel->getByModel($model, $model_id);
		
		if(empty($Gallery) || is_null($Gallery) || $Gallery === false) return false;
		
		foreach($Gallery as $k=>$item) {
			//$this->Model->id = $item['media_id'];
			$this->setModel(new IMG_MODEL($item['media_id']));
			$avatar = false;
			if($this->Model->avatar == 1) {
				$avatar = true;
			}
			$ImagePath = $this->getTheImage($img_resize, $avatar);
			if(!empty($ImagePath)) {
				$Gallery[$k]['path'] = $ImagePath;
			} else {
				unset($Gallery[$k]);
			}
			
		}

		return $Gallery;
	}

	public static function isImage(string $filePath):bool {
		
		if(!file_exists($filePath)) return false;
		
		$valid_estensions = explode(",", self::IMAGE_EXTENSIONS);
		$info = pathinfo($filePath);

		if(in_array(strtolower($info['extension']), $valid_estensions)) {
			return true;
		}
		return false;
	}
	
}