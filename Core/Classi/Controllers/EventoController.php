<?php
namespace Core\Classi\Controllers;
if(!defined("ROOT")) exit();
use Core\Classi\CONTROLLER;
use Core\Classi\UTILITY;
use Core\Classi\Models\EVENTO;
use Core\Classi\Models\TESTO;
use Core\Classi\Controllers\IMG;

class EVENTO_CONTROLLER extends CONTROLLER {

    public $prefix = "eventi";
    public $textesFields;

    public function __construct(int $id = NULL) {
		$this->setResponse();
		$this->setModel(new EVENTO($id));
        $this->textesFields = ["titolo", "sottotitolo", "testo", ];
	}

    public function storeEvento(array $post, $files = NULL) {

        $Testo = new TESTO($this->prefix);

        foreach($post['testi'] as $chiave=>$testi) {
            $item = $Testo->storeTextes($testi);
            ${$chiave} = (int) $item['data']['text_id']; 
        }

        $img = 0;
        if($post['media_id']>0) {
            $img = (int) $post['media_id'];
        } else {
            if(!is_null($files)) {
                $Img = new IMG();
                $Img->storeImg($files, "event_image"); 
                $ResponseImg = $Img->getResponse();
                if(!$ResponseImg['result']) {
                    $this->pushMessage($ResponseImg['error'], "danger");
                }
                $img = (int) $ResponseImg['data']->id;
                $this->pushMessage("img.messages.immagine_caricata", "success");
                //$this->pushMessage("test.prova", "warning");
            }
        }

        $location = "";
        $geoDataJson = "";
        if(!empty($post['comune_geoid']) && is_numeric($post['comune_geoid'])) {
            $geo_commune_id = (int) $post['comune_geoid'];
            $location = (string) $post['comune'];
            $geoData = [
                "comune" => $location,
                "comune_geoid" => $geo_commune_id,
                "indirizzo" => (string) $post['indirizzo'],
                "cap" => (string) $post['cap'],
                "luogo" => (string) $post['luogo'],
                "latitudine" => (string) $post['latitudine'],
                "longitudine" => (string) $post['longitudine'],
            ];
            $geoDataJson = json_encode($geoData);
        }
        
        $date_from = !empty($post['date_from'])?date("Y-m-d H:i:s", strtotime($post['date_from'])):"";
        $date_to = !empty($post['date_to'])?date("Y-m-d H:i:s", strtotime($post['date_to'])):"";
        $postData = [
            "titolo" => $titolo,
            "sottotitolo" => $sottotitolo,
            "testo" => $descrizione,
            "img" => $img,
            "location" => $location,
            "geo" => $geoDataJson,
            "link" => (string)$post['link'],
            "date_from" => $date_from,
            "date_to" => $date_to,
        ];

        $storeResult = $this->Model->store($postData);
        $this->setResponse($storeResult);

    }

    public function updateEvento(array $post, $files = NULL) {

        $Testo = new TESTO($this->prefix);
        
        foreach($post['testi'] as $chiave=>$testi) {
            $testo_id = $this->Model->{$chiave};
            if(is_numeric($testo_id) && $testo_id>0) {
                $item = $Testo->updateTextes($testo_id, $testi);
            }
        }

        $img = $this->Model->img;
        if($post['media_id']>0) {
            $img = (int) $post['media_id'];
        } else {
            if(!is_null($files)) {
                $Img = new IMG();
                $Img->storeImg($files, "event_image"); 
                $ResponseImg = $Img->getResponse();
                if(!$ResponseImg['result']) {
                    $this->setResponse($ResponseImg);
                    return;
                }
                $img = (int) $ResponseImg['data']->id;
            }
        }

        $location = "";
        $geoDataJson = "";
        if(!empty($post['comune_geoid']) && is_numeric($post['comune_geoid'])) {
            $geo_commune_id = (int) $post['comune_geoid'];
            $location = (string) $post['comune'];
            $geoData = [
                "comune" => $location,
                "comune_geoid" => $geo_commune_id,
                "indirizzo" => (string) $post['indirizzo'],
                "cap" => (string) $post['cap'],
                "luogo" => (string) $post['luogo'],
                "latitudine" => (string) $post['latitudine'],
                "longitudine" => (string) $post['longitudine'],
            ];
            $geoDataJson = json_encode($geoData);
        }

        $date_from = !empty($post['date_from'])?date("Y-m-d H:i:s", strtotime($post['date_from'])):"";
        $date_to = !empty($post['date_to'])?date("Y-m-d H:i:s", strtotime($post['date_to'])):"";

        $postData = [
            "img" => $img,
            "location" => $location,
            "geo" => $geoDataJson,
            "link" => (string)$post['link'],
            "published" => $post['published'],
            "date_from" => $date_from,
            "date_to" => $date_to,
        ];

        $updateResult = $this->Model->update($postData);
        $this->setResponse($updateResult);

    }

    public function deleteEvento() {
        $Testo = new TESTO($this->prefix);
        foreach($this->textesFields as $textfield) {
            if($this->Model->{$textfield}>0) {
                $Testo->removeTestoByID($this->Model->{$textfield});
            }
        }
        
        $Response = $this->Model->remove();
        if($Response === false) {
            $this->setResponse(["result"=>false,"error"=>"errore_query","data"=>NULL,]);
        } else {
            $this->setResponse(["result"=>true,"error"=>NULL,"data"=>NULL,]);
        }
        return $Response;
    }

}