<?php
namespace Core\Classi\Controllers;
if(!defined("ROOT")) exit();
use Core\Classi\Models\UTENTI;
use Core\Classi\CONTROLLER;
include_once(ROOT."Core/Classi/Crypto.php");
use Core\Classi\CRYPTO;
use Core\Classi\DATO;
use Core\Classi\UTILITY;
use Core\Classi\DB;
use Core\Classi\Models\USER_ROLES as USER_ROLES_MODEL;
use Core\Classi\Controllers\IMG;

class UTENTE extends CONTROLLER {

	public const PasswordExpr = REG_EXP_PASSWORD;
	public $User_roles;
	public $AvatarImg;
	
	public function __construct(int $id = NULL) {
		$this->setResponse();
		$this->setModel(new UTENTI($id));
		if(!is_null($id)) {
			$this->AvatarImg = $this->getAvatar();
		}
	}
	
	
	public function login($username, $password) {
		
		if(empty($username) || empty($password)) return "empty";
		
		$Dato = new DATO($username, $this->EspressioniRegolari['nick']);
		if(!$Dato->isRegulerExp()) { 
			
			return "dato_errato_nick"; 
		}
		
		
		$Dato = new DATO($password, $this->EspressioniRegolari['password']);
		if(!$Dato->isRegulerExp()) {
			
			return "dato_errato_password"; 
		}
		
		$username = UTILITY::proteggiDB($username);
		
		$query = "SELECT * FROM utenti WHERE email=\"".$username."\" OR nick=\"".$username."\"";
		$db = new DB();
		$db->query = $query;
		$res = $db->do_query();
		
		if(is_null($res)) {
			
			return "failed";
		}
		
		$this->Model->istanzia($res[0]['id']);
		if(CRYPTO::codifica_password($password) !== $this->Model->password) {
			return "failed";
		} else {
			
			$this->User_roles = new USER_ROLES_MODEL($this->Model->ruolo);
			$res[0]['ruolo'] = $this->User_roles->role;
			$this->AvatarImg = $this->getAvatar();
			$res[0]['avatarImg'] = $this->AvatarImg; 
			
			$this->setSession(array("logged"=>true, "user"=>$res[0]));
			return "OK";
		}
	}
	
	public function getUtente() {
		//return $this->getModel();
		$Utente = $this->getModel();
		if(empty($Utente->id)) return NULL;
		return $Utente;
	}
	
	protected function checkPassword($password) {
		$Dato = new DATO($password, self::PasswordExpr);
		
	}
	
	public function storeUtente($post, $avatar = NULL) {
		
		$responseRegExp = $this->checkRegularExpressions($post);
		if($responseRegExp['check'] == false) {
			$response = array(
				"esito" => false,
				"error" => "RegExp",
				"data" => $responseRegExp['fields'],
			);
			
			$this->setResponse($response);
			return;
		}
		
		$res = $this->Model->getFromField("email", $post['email']);
		if(!is_null($res)) {
			$response = array(
				"esito" => false,
				"error" => "user_esistente",
				"data" => "",
			);
			
			$this->setResponse($response);
			return;
		}
		
		if(array_key_exists("password", $post)) {	
			$realPassword = $post['password'];
		} else {
			
			$realPassword = CRYPTO::genera_password();

		}
		$post['password'] = $realPassword;
		//die($post['password']);
		foreach($post as $k=>$v) {
			$post[$k] = UTILITY::proteggiDB($v);
			if($k == "password") {
				$post[$k] = CRYPTO::codifica_password($post[$k]);
			}
		}
		
		$avatar_value = 0;
		if(!is_null($avatar)) {
			$Img = new IMG();
			$Img->storeImg($avatar, "avatar", "avatar", 1);
			$responseImg = $Img->getResponse();
			if($responseImg['result']) {
				
				$avatar_value = $responseImg['data']->id;
			}
		}
		
		$params = array(
			"email" => $post['email'],
			"nick" => $post['nick'],
			"password" => $post['password'],
			"ruolo" => $post['ruolo'],
			"attivo" => $post['attivo'],
			"foto" => $avatar_value,
		);

		$responseStore = $this->Model->store($params);
		
		$this->setResponse($responseStore);
		return;
				
	}
	
	public function updateUtente($post, $avatar = NULL) {
		
		$responseRegExp = $this->checkRegularExpressions($post);
		if($responseRegExp['check'] == false) {
			$response = array(
				"esito" => false,
				"error" => "RegExp",
				"data" => $responseRegExp['fields'],
			);
			
			$this->setResponse($response);
			return;
		}
		
		foreach($post as $k=>$v) {
			$post[$k] = UTILITY::proteggiDB($v);
			if($k == "password") {
				$post[$k] = CRYPTO::codifica_password($post[$k]);
			}
		}

		/******** Verifico che non esistano altri utenti con la stessa casella email :: start */

		$UtentiModel = new UTENTI();
		$res = $UtentiModel->getFromField("email", $post['email']);
		
		if(!is_null($res) && $res['id'] != $this->Model->id) {
			
			$response = array(
				"esito" => false,
				"error" => "user_esistente",
				"data" => "",
			);
			
			$this->setResponse($response);
			return;
		}
		
		/******** Verifico che non esistano altri utenti con la stessa casella email :: end */

		$avatar_value = $this->Model->foto;
		if(!is_null($avatar)) {
			
			$avatar_default = $avatar_value;
			
			$Img = new IMG();
			$Img->storeImg($avatar, "avatar", "avatar", 1);
			$responseImg = $Img->getResponse();
			if($responseImg['result']) {
				
				$avatar_value = $responseImg['data']->id;
				$avatar_filename = $responseImg['data']->filename;

			}
			
			if(is_integer($avatar_default)) {
				$Img = new IMG($avatar_default);
				$Img->remove_img();
			}
			
		}
		/*
		UTILITY::codePrint($this->Model, "Oggetto DB");
		UTILITY::codePrint($post, "POST Controller");
		*/
		$params = array(
			"email" => $post['email'],
			"nick" => $post['nick'],
			"ruolo" => $post['ruolo'],
			"attivo" => $post['attivo'],
			"banned" => $post['banned'],
			"foto" => $avatar_value,
		);
		
		if(array_key_exists("password", $post)) {
			$params['password'] = $post['password'];
		}
		
		//UTILITY::codePrint($params, "Parametri"); die();
		$response = $this->Model->update($params);
		if(isset($avatar_filename)) {
			$response['data']['avatar_filename'] = $avatar_filename;
 		}
		
		$this->setResponse($response);
		return;
		
	}
	public function updatePassword($oldPassword, $newPassword) {

		if(empty($oldPassword) || empty($newPassword)) {
			
			$response = array(
				"esito" => false,
				"error" => "uncorret_data_type",
				"data" => "",
			);
			
			$this->setResponse($response);
			return;
		}

		$oldPassword = CRYPTO::codifica_password($oldPassword);
		if($this->Model->password != $oldPassword) {
			$response = array(
				"esito" => false,
				"error" => "uncorrect_ol_password",
				"data" => "",
			);
			$this->setResponse($response);
			return;
		}

		$responseRegExp = $this->checkRegularExpressions(['password'=>$newPassword]);
		if($responseRegExp['check'] == false) {
			$response = array(
				"esito" => false,
				"error" => "RegExp",
				"data" => $responseRegExp['fields'],
			);
			
			$this->setResponse($response);
			return;
		}

		$newPassword = CRYPTO::codifica_password($newPassword);
		$params = ['password'=>$newPassword];
		$response = $this->Model->update($params);
		
		$this->setResponse($response);
		return;
	}
	
	private function get_ruolo_utente($utente) {
		if(empty($utente)) return false;
		
		$this->User_roles = new USER_ROLES_MODEL();
		
		if(is_array($utente)) {
			//UTILITY::codePrint($utente);
			foreach($utente as $k=>$v) {
				$ruolo = $this->User_roles->istanzia($v['ruolo']);
				$utente[$k]['ruolo'] = $ruolo['role'];
			}
			
		} else {
			$this->User_roles->istanzia($utente['role']);
			$utente['ruolo'] = $ruolo['ruolo'];
		}
		return $utente;
	}
	
	public function getAvatar() {
		
		if(empty($this->Model->id)) return false;
		if(!($this->Model->foto > 0)) return NULL;
		
		$Img = new IMG($this->Model->foto);
		
		return $Img->getImage();
		
	}
	
	public function utenti_attivi() {
	
		$filtri = array(
			array(
				"field" => "attivo",
				"operator" => "=",
				"value" => 1,
			),
			array(
				"field" => "banned",
				"operator" => "=",
				"value" => 0,
			),
			array(
				"field" => "deleted",
				"operator" => "=",
				"value" => 0,
			),

		);
		
		$utenti = $this->Model->lista($filtri);
		$utenti = $this->get_ruolo_utente($utenti);
		
		return $utenti;
	}
	
	public function utenti_tutti($filtri = array()) {
		
		if(!$this->ctl_ruolo("admin")) {
			$response = array(
				"esito" => false,
				"error" => "not_authorized",
				"data" => NULL,
			);
		}
		
		$utenti = $this->Model->lista($filtri);
		$utenti = $this->get_ruolo_utente($utenti);
		return $utenti;
		
	}
	
	public function ctl_ruolo($ruoli) {
		//UTILITY::codePrint($this->Session, "Oggetto sessione");
		if(empty($this->Session['logged'])) return false;

		if(!$this->Session['logged']) return false;
		if($this->Session['user']['super'] == 1) return true;
		if(empty($this->Session['user']['ruolo'])) return false;
		if(strpos($ruoli, $this->Session['user']['ruolo']) === false) return false;
		
		return true;
		
	}
	
	public function isLogged() {
		$check = false;
		if($this->Session["logged"]) {
			if(!empty($this->Session['user']['id'])) {
				$check = true;
			}
		}
		return $check;
	}
	
	public function logout() {
		$this->setSession(array("logged"=>false, "user"=>NULL));
		return true;
	}
	
	public function rolesList() {
		$this->User_roles = new USER_ROLES_MODEL();
		return $this->User_roles->lista();
	}

}