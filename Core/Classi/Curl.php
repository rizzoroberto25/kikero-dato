<?php
namespace Core\Classi;
if(!defined("ROOT")) exit();
use Core\Classi\UTILITY;

class CURL {
    private $userAgent =  NULL;
    private $httpHeaders = NULL;
    public $httpResponseCode = NULL;
    private $requestMethod = NULL;
    private $allowedMethods = NULL;
    private $endpoint;
    public $curlResponse;
	public $isSendingBlue;
	public $isJsonPost;
	public $PostIsString;
 
    public function __construct($options = []) {
       $this->userAgent = NOME_SITO;
       $this->allowedMethods = [
           "GET", "POST", "PUT", "DELETE", "PATCH",
       ];
	   $this->isSendingBlue = false;
	   $this->isJsonPost = false;
	   $this->setOptions($options);
	   $this->PostIsString = false;
    }

    public function getUserAgent(){
        return $this->userAgent;
    }
    public function setHeaders(array $headers) {
        $this->httpHeaders = $headers;
    }

    public function setUserAgent($userAgent){
        $this->userAgent = $userAgent;
    }

    public function setMethod($method) {
        
        if(in_array($method, $this->allowedMethods)) {
            $this->requestMethod = $method;
        }

    }
	
	public function setOptions(array $options) {
		foreach($options as $key=>$option) {
			$this->{$key} = $option;
		}
	}

    public function callCurl($url, $htaccess_credentials=NULL) { // Per chiamate GET
        $this->endpoint = $url;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url,
        ));

        if(!empty($this->userAgent)){
            curl_setopt($curl, CURLOPT_USERAGENT,  $this->userAgent);
        }

        if(!empty($htaccess_credentials)){
            curl_setopt($curl, CURLOPT_USERPWD,  $htaccess_credentials);
        }

        if(!is_null($this->httpHeaders) && is_array($this->httpHeaders)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $this->httpHeaders);
        }
		if($this->isSendingBlue) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($curl, CURLOPT_ENCODING, "");
			curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
			curl_setopt($curl, CURLOPT_TIMEOUT, 30);
			curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);	
		}

        $response = curl_exec($curl);
        $this->httpResponseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        
        $this->curlResponse = json_decode($response);
        return $response;
    }
    
    public function callAPI($url, $data=NULL, $htaccess_credentials=NULL) { // Per chiamate POST, PUT o DELETE
	
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_POST, true);
		if(!is_null($data)) {
			if($this->isJsonPost === false) {
				if(!$this->PostIsString) {
					curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
				} else {
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				}
			} else {
				curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
			}
		}
		curl_setopt($curl, CURLOPT_REFERER, $_SERVER['HTTP_HOST']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if(!empty($this->userAgent)){
            curl_setopt($curl, CURLOPT_USERAGENT,  $this->userAgent);
        }
        
        if(!empty($htaccess_credentials)){
            curl_setopt($curl, CURLOPT_USERPWD,  $htaccess_credentials);
        }

        if(!is_null($this->httpHeaders) && is_array($this->httpHeaders)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $this->httpHeaders);
        }

        if(!is_null($this->requestMethod)) {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->requestMethod);
        }
		
		if($this->isSendingBlue) {
			curl_setopt($curl, CURLOPT_ENCODING, "");
			curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
			curl_setopt($curl, CURLOPT_TIMEOUT, 30);
			curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		}

        $response = curl_exec($curl);
        $this->httpResponseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        $this->curlResponse = json_decode($response);
        return $response;
    }
    
    public function getHeaders($respHeaders) {
        $headers = array();
        $headerText = substr($respHeaders, 0, strpos($respHeaders, "\r\n\r\n"));
        foreach (explode("\r\n", $headerText) as $i => $line) {
            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                list ($key, $value) = explode(': ', $line);
                $headers[$key] = $value;
            }
        }
        return $headers;
    }
}