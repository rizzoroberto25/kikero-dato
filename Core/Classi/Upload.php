<?php
namespace Core\Classi;
include(ROOT."Core/Classi/Ext/resize_img/cropImage.php");

use Core\Classi\Ext\resize_img\ThumbAndCrop;

class UPLOAD {

	public $tipofile;
	
	public function __construct($tipo) {
	
		$this->tipofile=$tipo;
	
	}
	
	public function ctl_tipo_file($file) {
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		
		$check=false;
		
		switch($this->tipofile) {
			case "img":
				$permessi=array("png", "jpg", "jpeg", "gif");
				break;
			
			case "doc":
				$permessi=array("doc", "docx", "pdf", "xls", "xlsx", "png", "jpg", "jpeg", "gif");
				break;	
		}
		
		if(in_array(strtolower($ext), $permessi)) {
			$check=true;
		}
		
		return $check;
		
	}
	
	public function cropImage($d, $only_resize=false) {
	
		$fileori=ROOT.$d['source'];
		if(!$this->ctl_tipo_file($fileori)) return "file non valido";
		
		$filecrop=$d['destinazione'];
		
		if($exif = exif_read_data($fileori)) {
		
			if(!empty($exif['Orientation'])) {
				switch($exif['Orientation']) {
					case 8:
						$fileori = imagerotate($fileori,90,0);
						break;
					case 3:
						$fileori = imagerotate($fileori,180,0);
						break;
					case 6:
						$fileori = imagerotate($fileori,-90,0);
						break;
				}
			}
		}
	
		$tb = new ThumbAndCrop();
		$tb->openImg($fileori);
		//$newHeight = $tb->getRightHeight($_GET['altezza']);
		$tb->creaThumb($d['w'], $d['h']);
		if($only_resize===false) {
			$tb->setThumbAsOriginal();
			$tb->cropThumb($d['cropW'], $d['cropH'], $d['x'], $d['y']);
			$tb->saveThumb($filecrop);
			$tb->resetOriginal();
		} else {
			$tb->saveThumb($filecrop);
		}
		$tb->closeImg();
		
		return "ok";
	
	}
	
	public function ritaglia_foto($file, $nomefile, $dir=false) {
		
		if($dir===false) {
			$dir=ROOT."uploads/".DOMINIO."/pictures";
		}

		list($width, $height, $type, $attr) = getimagesize($file);
		
		$widths=explode(",", IMG_WIDTH_RESIZES);
		$tb = new ThumbAndCrop();
		$tb->openImg($file);
		foreach($widths as $neww) {

			$dir_copy=$dir.$neww."/";
			if($neww<$width) {
				$newh=$tb->getRightHeight($neww);
			} else {
				$neww=$width;
				$newh=$height;
			}
			
			$tb->creaThumb($neww, $newh);
			$tb->saveThumb($dir_copy.$nomefile);
			$tb->setThumbAsOriginal();
			
		}
		if(is_dir($dir."/crop/")) {
			
			$tb = new ThumbAndCrop();
			$tb->openImg($file);
			
			$x=0;
			$y=0;
			$neww=500;
			$newh=500;
			if($width>$height) {
				$neww=$tb->getRightWidth($neww);
				$x=($neww-$newh)/2;
			}
			if($height>$width) {
				$newh=$tb->getRightHeight($newh);
				$y=($newh-$neww)/2;
			}
			$tb->creaThumb($neww, $newh);
			$tb->setThumbAsOriginal();
			$tb->cropThumb(500, 500, $x, $y);
			$tb->saveThumb($dir."/crop/".$nomefile);
				
		}
		$tb->closeImg();

	}
	
	public function pulisci_temp($dir) {
		$query="SELECT * FROM sess_uploads WHERE sessione=\"".session_id()."\"";
		$db=new DB($query);
		$r=$db->query();
		foreach($r as $file) {
			if(file_exists(ROOT.$dir."/".$file['source'])) {
				unlink(ROOT.$dir."/".$file['source']);
			}
		}
		$query="DELETE FROM sess_uploads WHERE sessione=\"".session_id()."\"";
		$db=new DB($query);
		$db->action();
	}
	
	public function get_pictures_dirs() {
		$dirs=explode(",", IMG_WIDTH_RESIZES);
		$dirs[]="crop";
		$dirs[]="blocchi";
		return $dirs;
	}
	
	public function get_icon($file, $iconSize="") {
		$types=array(
			"png" => "ico_img".$iconSize.".png",
			"jpg" => "ico_img".$iconSize.".png",
			"jpeg" => "ico_img".$iconSize.".png",
			"gif" => "ico_img".$iconSize.".png",
			"doc" => "ico_doc".$iconSize.".png",
			"docx" => "ico_doc".$iconSize.".png",
			"xls" => "ico_xls".$iconSize.".png",
			"xlsx" => "ico_xls".$iconSize.".png",
			"pdf" => "ico_pdf".$iconSize.".png",
		);
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		
		if(!array_key_exists($ext, $types)) return false;
		
		return PATH."img/icons-files/".$types[$ext];
	}
	
	public function rmdir_recursive($dir) {
		foreach(scandir($dir) as $file) {
			if ('.' === $file || '..' === $file) continue;
			if (is_dir($dir.'/'.$file)) rmdir_recursive($dir.'/'.$file);
			else unlink($dir.'/'.$file);
		}
		rmdir($dir);
	}	
	
}
?>