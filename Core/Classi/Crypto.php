<?php
namespace Core\Classi;
if(!defined("ROOT")) exit();

class CRYPTO {
	
	public $iv;
	public $encrypt_method = "AES-256-CBC";
	public $secret_key = CODEAPP;
	public $keyCrypto;
	public $stringa;
	public $output;
	protected $token;
	private $sessionId;
	
	public function __construct($stringa, $iv = 0) {
		
		$this->stringa = $stringa;
		$this->keyCrypto = hash('sha256', $this->secret_key);
		$this->iv = substr(hash('sha256', $this->iv), 0, 16);
		$this->token = false;
		$this->sessionId = session_id();
		
	}
	
	public function encrypt() {
		$output = false;
		$output = openssl_encrypt($this->stringa, $this->encrypt_method, $this->keyCrypto, 0, $this->iv);
        $this->output = base64_encode($output);
		//return $output;
	}
	
	public function decrypt() {
		$this->output = false;
		$this->output = openssl_decrypt(base64_decode($this->stringa), $this->encrypt_method, $this->keyCrypto, 0, $this->iv);
		//return $output;
	}
	
	public static function genera_password() {
	
		// $caratteri=array("q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "n", "m", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0","Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "A", "S", "D", "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N", "N", "M","_", "£", "%", "&", "#",);
		$tipi = array("minuscole", "maiuscole", "numerici", "speciali");
		
		$minuscole = array("q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "n", "m",);
		$maiuscole = array("Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "A", "S", "D", "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N", "N", "M",);
		$numerici = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0",);
		
		$speciali = array("_", "£", "%", "&", "#",);
		
		$caratteri = array_merge($minuscole, $maiuscole, $numerici, $speciali);
		
		$caratteriUsati = array();
		
		$n_array=count($caratteri);
		$pawssword="";
		
		for($i=0; $i<9; $i++) {
			$l = rand(0, $n_array-1);
			$password .= $caratteri[$l];
			
			foreach($tipi as $tipo) {
				if(in_array($caratteri[$l], ${$tipo})) {
					array_push($caratteriUsati, $tipo);
				}
			}
		}
		
		foreach($tipi as $tipo) {
			
			if(!in_array($tipo, $caratteriUsati)) {
				$n_array = count(${$tipo});
				$l = rand(0, $n_array-1);
				$password .= ${$tipo}[$l];
				array_push($caratteriUsati, $tipo);
			}
			
		}
		
		return $password;
		
	}
	
	public static function codifica_password($password) {
		return sha1(PASSWORD_COD1.$password.PASSWORD_COD2);
	}
	
	public function setToken() {
		
		if(empty($this->stringa)) {
			return false;
		}
		//die("Session id: " .$this->sessionId);
		$this->stringa .= ".".$this->sessionId; 
		
		$this->stringa .= $this->secret_key;
		$this->token = sha1($this->stringa);
		
		return true;
		
	}
	
	public function getToken() {
		return $this->token;
	}
	
}
?>