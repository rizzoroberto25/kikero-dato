<?php
if(!defined("ROOT")) exit();

include_once(ROOT."Core/Classi/Ext/Mobile-Detect/Mobile_Detect.php");
$MobileObj = new Mobile_Detect();

include_once(ROOT."Core/Classi/DB.php");
include_once(ROOT."Core/Classi/Dato.php");
include_once(ROOT."Core/Classi/Model.php");
include_once(ROOT."Core/Classi/Controller.php");
include_once(ROOT."Core/Classi/Utility.php");
include_once(ROOT."Core/Classi/Lingua.php");
include_once(ROOT."Core/Classi/Upload.php");
include_once(ROOT."Core/Classi/Pagination.php");
include_once(ROOT."Core/Classi/Curl.php");
include_once(ROOT."Core/Classi/Geo.php");

if(isset($ProjectModels) && is_array($ProjectModels)) {
	foreach($ProjectModels as $model) {
		include_once(ROOT."Core/Classi/Models/".$model.".php");
	}
}
if(isset($ProjectControllers) && is_array($ProjectControllers)) {
	foreach($ProjectControllers as $controller) {
		include_once(ROOT."Core/Classi/Controllers/".$controller.".php");
	}
}
require(ROOT."vendor/autoload.php");
$lingua_principale = "it";
$lingua = $lingua_principale;
if(isset($_GET['lang'])) {
	$lingua_principale = $_GET['lang'];
	$lingua = $_GET['lang'];
}
define("LINGUA_PR", $lingua_principale);
$_SESSION['lingua'] = $lingua;
?>