<?php
/* Smarty version 3.1.33, created on 2019-03-18 10:42:41
  from 'C:\xampp\htdocs\Dato\themes\default\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c8f681130d240_39780487',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a2837c52fcbc4f114ffc21a371f000af6207ab52' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Dato\\themes\\default\\footer.tpl',
      1 => 1552902155,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c8f681130d240_39780487 (Smarty_Internal_Template $_smarty_tpl) {
?>		
		<!-- start footer Area -->
		<footer class="footer-area section-gap">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-6 col-sm-6">
						<div class="single-footer-widget">
							<h4>About Me</h4>
							<p>
								We have tested a number of registry fix and clean utilities and present our top 3 list on our site for your convenience.
							</p>
							<p class="footer-text"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<?php echo '<script'; ?>
>document.write(new Date().getFullYear());<?php echo '</script'; ?>
> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
		<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
						</div>
					</div>
					<div class="col-lg-5 col-md-6 col-sm-6">
						<div class="single-footer-widget">
							<h4>Newsletter</h4>
							<p>Stay updated with our latest trends</p>
							<div class="" id="mc_embed_signup">
								 <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get">
								  <div class="input-group">
									<input type="text" class="form-control" name="EMAIL" placeholder="Enter Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email Address '" required="" type="email">
									<div class="input-group-btn">
									  <button class="btn btn-default" type="submit">
										<span class="lnr lnr-arrow-right"></span>
									  </button>    
									</div>
										<div class="info"></div>  
								  </div>
								</form> 
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-6 col-sm-6 social-widget">
						<div class="single-footer-widget">
							<h4>Follow Me</h4>
							<p>Let us be social</p>
							<div class="footer-social d-flex align-items-center">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-dribbble"></i></a>
								<a href="#"><i class="fa fa-behance"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- End footer Area -->		

		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/vendor/jquery-2.2.4.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/popper.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/vendor/bootstrap.min.js"><?php echo '</script'; ?>
>
		<!--
		<?php echo '<script'; ?>
 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"><?php echo '</script'; ?>
>
		-->
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/easing.min.js"><?php echo '</script'; ?>
>			
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/hoverIntent.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/superfish.min.js"><?php echo '</script'; ?>
>	
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/jquery.ajaxchimp.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/jquery.magnific-popup.min.js"><?php echo '</script'; ?>
>	
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/jquery.tabs.min.js"><?php echo '</script'; ?>
>						
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/jquery.nice-select.min.js"><?php echo '</script'; ?>
>	
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/isotope.pkgd.min.js"><?php echo '</script'; ?>
>			
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/waypoints.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/jquery.counterup.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/simple-skillbar.js"><?php echo '</script'; ?>
>							
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/owl.carousel.min.js"><?php echo '</script'; ?>
>							
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/mail-script.js"><?php echo '</script'; ?>
>	
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
js/main.js"><?php echo '</script'; ?>
>


	</body>
	
</html><?php }
}
