<?php
/* Smarty version 3.1.33, created on 2019-03-18 12:05:23
  from 'C:\xampp\htdocs\Dato\themes\default\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c8f7b7366d463_48735638',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0d16aaabfc348b32f904ebb4ba9f2faf331ba8ce' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Dato\\themes\\default\\header.tpl',
      1 => 1552907109,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c8f7b7366d463_48735638 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="<?php echo $_smarty_tpl->tpl_vars['render']->value['template']['language'];?>
" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title><?php echo $_smarty_tpl->tpl_vars['render']->value['template']['title'];?>
</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
css/linearicons.css">
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
css/bootstrap.css">
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
css/magnific-popup.css">
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
css/jquery-ui.css">				
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
css/nice-select.css">							
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
css/animate.min.css">
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
css/owl.carousel.css">				
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
css/main.css">
	</head>
	<body>	
		  <header id="header">
		    <div class="container main-menu">
		    	<div class="row align-items-center justify-content-between d-flex">
			      <div id="logo">
			        <a href="index.html"><img src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
img/logo.png" alt="" title="" /></a>
			      </div>
			      <nav id="nav-menu-container">
			        <ul class="nav-menu">
					
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['render']->value['template']['Menu'], 'item', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
?>
						
						<li<?php if ($_smarty_tpl->tpl_vars['item']->value['subpages'] != '') {?> class="menu-has-children" <?php }?>>
						<a href="<?php echo $_smarty_tpl->tpl_vars['render']->value['sitePath'];
echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value == $_smarty_tpl->tpl_vars['CP']->value) {?> class="active" <?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>
</a>
						<?php if ($_smarty_tpl->tpl_vars['item']->value['subpages'] != '') {?>
							<ul>
							
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item']->value['subpages'], 'subpage_item', false, 'subpage_key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['subpage_key']->value => $_smarty_tpl->tpl_vars['subpage_item']->value) {
?>
							
								<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['render']->value['sitePath'];
echo $_smarty_tpl->tpl_vars['subpage_item']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['subpage_item']->value['title'];?>
" <?php if ($_smarty_tpl->tpl_vars['subpage_key']->value == $_smarty_tpl->tpl_vars['CP']->value) {?> class="active" <?php }?>><?php echo $_smarty_tpl->tpl_vars['subpage_item']->value['label'];?>
</a>
						
								</li>
							
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
							
							</ul>
						<?php }?>
						</li>
						
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					  
			        </ul>
			      </nav><!-- #nav-menu-container -->		    		
		    	</div>
		    </div>
		  </header><!-- #header -->
	
<?php }
}
