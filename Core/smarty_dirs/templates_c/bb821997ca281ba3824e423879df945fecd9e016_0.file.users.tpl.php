<?php
/* Smarty version 3.1.33, created on 2019-03-18 11:58:59
  from 'C:\xampp\htdocs\Dato\themes\default\users.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c8f79f39beb79_33773722',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bb821997ca281ba3824e423879df945fecd9e016' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Dato\\themes\\default\\users.tpl',
      1 => 1552906733,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5c8f79f39beb79_33773722 (Smarty_Internal_Template $_smarty_tpl) {
?>			<?php $_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
			<!-- start banner Area -->
			<section class="about-banner">
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								<?php echo $_smarty_tpl->tpl_vars['render']->value['Metas']['title'];?>
				
							</h1>	
							
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->	
				<!-- Start portfolio-area Area -->
            <section class="portfolio-area section-gap" id="portfolio">
                <div class="container">
		            <div class="row d-flex justify-content-center">
		                <div class="menu-content pb-70 col-lg-8">
		                    <div class="title text-center">
		                        <h2 class="mb-10"><?php echo $_smarty_tpl->tpl_vars['render']->value['contents']['sottotitolo'];?>
</h2>
		                        <p><?php echo $_smarty_tpl->tpl_vars['render']->value['contents']['paragrafo'];?>
</p>
		                    </div>
		                </div>
		            </div>

                    
                    <div class="filters-content">
                        <div class="row grid">
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['render']->value['contents']['utenti'], 'utente');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['utente']->value) {
?>
														
							
							<div class="single-portfolio col-sm-4 all vector">
                            	<div class="relative">
	                            	<div class="thumb">
	                            		<div class="overlay overlay-bg"></div>
	                            		 <img class="image img-fluid" src="<?php echo $_smarty_tpl->tpl_vars['render']->value['sitePath'];?>
img/users/<?php echo $_smarty_tpl->tpl_vars['utente']->value['id'];?>
.jpg" alt="">
	                            	</div>
									<a href="<?php echo $_smarty_tpl->tpl_vars['render']->value['sitePath'];?>
img/users/<?php echo $_smarty_tpl->tpl_vars['utente']->value['id'];?>
.jpg" class="img-pop-up">	
									  <div class="middle">
									    <div class="text align-self-center d-flex"><img src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
img/preview.png" alt=""></div>
									  </div>
								</a>                              		
                            	</div>
								<div class="p-inner">
								    <h4><?php echo $_smarty_tpl->tpl_vars['utente']->value['nome'];?>
 <?php echo $_smarty_tpl->tpl_vars['utente']->value['cognome'];?>
</h4>
									<div class="cat"><?php echo $_smarty_tpl->tpl_vars['utente']->value['ruolo'];?>
</div>
								</div>					                               
                            </div>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            
                            
                        </div>
                    </div>
                    
                </div>
            </section>
            <!-- End portfolio-area Area -->
			
			<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
