<?php
/* Smarty version 3.1.33, created on 2019-03-15 13:11:08
  from 'C:\xampp\htdocs\Dato\themes\default\user.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c8b965c5ddcd1_45635008',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b8dc08e6cb49c2f9309ded92cb4bb42ba5370df4' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Dato\\themes\\default\\user.tpl',
      1 => 1552651859,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:bannerTop.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5c8b965c5ddcd1_45635008 (Smarty_Internal_Template $_smarty_tpl) {
?>			<?php $_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
			<?php $_smarty_tpl->_subTemplateRender("file:bannerTop.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
			<!-- End banner Area -->

			<!-- Start home-about Area -->
			<section class="home-about-area pt-120">
				<div class="container">
					<div class="row align-items-center justify-content-between">
						<div class="col-lg-6 col-md-6 home-about-left">
							<img class="img-fluid" src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
img/about-img.png" alt="">
						</div>
						<div class="col-lg-5 col-md-6 home-about-right">
							<h6>About Me</h6>
							<h1 class="text-uppercase"><?php echo $_smarty_tpl->tpl_vars['render']->value['contents']['utente']->nome;?>
 <?php echo $_smarty_tpl->tpl_vars['render']->value['contents']['utente']->cognome;?>
</h1>
							<p>
								<?php echo $_smarty_tpl->tpl_vars['render']->value['contents']['testo'];?>

							</p>
							<a href="#" class="primary-btn text-uppercase">View Full Details</a>
						</div>
					</div>
				</div>	
			</section>
			<!-- End home-about Area -->
			
			
			
			<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
