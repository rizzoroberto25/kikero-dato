<?php
/* Smarty version 3.1.33, created on 2019-03-15 12:57:31
  from 'C:\xampp\htdocs\Dato\themes\default\bannerTop.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c8b932b5ae722_90499125',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f4844053991501d0135676fb4bd9b7ecd6a41685' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Dato\\themes\\default\\bannerTop.tpl',
      1 => 1552650869,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c8b932b5ae722_90499125 (Smarty_Internal_Template $_smarty_tpl) {
?>			<!-- start banner Area -->
			<section class="banner-area">
				<div class="container">
					<div class="row fullscreen align-items-center justify-content-between">
						<div class="col-lg-6 col-md-6 banner-left">
							<h6>This is me</h6>
							<h1>Roberto Rizzo</h1>
							<p>
								You will begin to realise why this exercise is called the Dickens Pattern with reference to the ghost showing Scrooge some different futures.
							</p>
							<a href="#" class="primary-btn text-uppercase">discover now</a>
						</div>
						<div class="col-lg-6 col-md-6 banner-right d-flex align-self-end">
							<img class="img-fluid" src="<?php echo $_smarty_tpl->tpl_vars['render']->value['themePath'];?>
img/hero-img.png" alt="">
						</div>
					</div>
				</div>					
			</section>
			<!-- End banner Area --><?php }
}
