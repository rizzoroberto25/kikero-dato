<?php
include("config.php");
$CP = "index";
if(!empty($_GET['cp']) && file_exists(ROOT."contents/".$CP.".php")) {
	$CP = $_GET['cp'];
}
include_once(ROOT."contents/".$CP.".php");


$Render = new RENDER($ProjectTheme);
$Render->pushRender("template", include_once(ROOT."themes/".$ProjectTheme."/settings.inc.php")); //print_r($Render); 
//die("Fine render");
$Render->pushRender("CP", $CP);
$Render->pushRender("contents", $contents);
if(!empty($pageMetas)) {
	$Render->setMetas($pageMetas);
}
$Render->getRender($CP);