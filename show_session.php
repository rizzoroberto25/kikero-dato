<?php
include("config.php");
use Core\Classi\UTILITY;

echo "<h3>Session ID</h3>";
echo session_id()."<hr />";

if(isset($_GET['action'])) {
	if($_GET['action'] == "destroy") {
		session_destroy();
	}
	if($_GET['action'] == "logout") {
		$_SESSION['logged'] = false;
		unset($_SESSION['user']);
	}
	
}

UTILITY::codePrint($_SESSION);

echo "<hr />";
echo "IP Server: ".$_SERVER['REMOTE_ADDR'];

//print_r($_SESSION);

?>