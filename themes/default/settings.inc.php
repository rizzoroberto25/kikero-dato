<?php
if(!defined("ROOT")) exit();

if($_SESSION['lingua'] == "en") {
	
	return array(
		"language" => $_SESSION['lingua'],
		"title" => "Cucina Sopraffina",
		
		"Menu" => array(
			
			"home" => array(
				"url" => "it/home",
				"label" => "Home",
				"title" => "DATO :: un framework elementare",
			),
			
			"progetto" => array(
				"url" => "it/progetto",
				"label" => "Il Progetto",
				"title" => "Il progetto dato, come funziona il Framework",
				"subpages" => array(
					"struttura" => array(
						"url" => "it/progetto/struttura",
						"label" => "Struttura di base",
						"title" => "La struttura di base di DATO",
					),
					"smarty" => array(
						"url" => "it/progetto/smarty",
						"label" => "Smarty",
						"title" => "DATO integra Smarty",
					),
				),
			),
			
			"users" => array(
				"url" => "it/utenti",
				"label" => "Utenti registrati",
				"title" => "Utenti registrati e rispettivi ruoli",
			),
			
			"articoli" => array(
				"url" => "it/articoli",
				"label" => "Articoli su DATO",
				"title" => "Tutte le novità",
			),
			
			
		),
	);
	
}

return array(

	"language" => $_SESSION['lingua'],
	"title" => "Cucina Sopraffina",
	
	"Menu" => array(
		
		"home" => array(
			"url" => "it/home",
			"label" => "Home",
			"title" => "DATO :: un framework elementare",
		),
		
		"progetto" => array(
			"url" => "it/progetto",
			"label" => "Il Progetto",
			"title" => "Il progetto dato, come funziona il Framework",
			"subpages" => array(
				"struttura" => array(
					"url" => "it/progetto/struttura",
					"label" => "Struttura di base",
					"title" => "La struttura di base di DATO",
				),
				"smarty" => array(
					"url" => "it/progetto/smarty",
					"label" => "Smarty",
					"title" => "DATO integra Smarty",
				),
			),
		),

		"users" => array(
			"url" => "it/utenti",
			"label" => "Utenti registrati",
			"title" => "Utenti registrati e rispettivi ruoli",
		),
		
		"articoli" => array(
			"url" => "it/articoli",
			"label" => "Articoli su DATO",
			"title" => "Tutte le novità",
		),
		
		
	),

);