			{include file="header.tpl"}
			{include file="bannerTop.tpl"}
			<!-- End banner Area -->

			<!-- Start home-about Area -->
			<section class="home-about-area pt-120">
				<div class="container">
					<div class="row align-items-center justify-content-between">
						<div class="col-lg-6 col-md-6 home-about-left">
							<img class="img-fluid" src="{$render.themePath}img/about-img.png" alt="">
						</div>
						<div class="col-lg-5 col-md-6 home-about-right">
							<h6>About Me</h6>
							<h1 class="text-uppercase">{$render.contents.utente->nome} {$render.contents.utente->cognome}</h1>
							<p>
								{$render.contents.testo}
							</p>
							<a href="#" class="primary-btn text-uppercase">View Full Details</a>
						</div>
					</div>
				</div>	
			</section>
			<!-- End home-about Area -->
			
			
			
			{include file="footer.tpl"}