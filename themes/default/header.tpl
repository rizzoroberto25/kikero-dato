<!DOCTYPE html>
<html lang="{$render.template.language}" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="{$render.themePath}img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>{$render.template.title}</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="{$render.themePath}css/linearicons.css">
		<link rel="stylesheet" href="{$render.themePath}css/font-awesome.min.css">
		<link rel="stylesheet" href="{$render.themePath}css/bootstrap.css">
		<link rel="stylesheet" href="{$render.themePath}css/magnific-popup.css">
		<link rel="stylesheet" href="{$render.themePath}css/jquery-ui.css">				
		<link rel="stylesheet" href="{$render.themePath}css/nice-select.css">							
		<link rel="stylesheet" href="{$render.themePath}css/animate.min.css">
		<link rel="stylesheet" href="{$render.themePath}css/owl.carousel.css">				
		<link rel="stylesheet" href="{$render.themePath}css/main.css">
	</head>
	<body>	
		  <header id="header">
		    <div class="container main-menu">
		    	<div class="row align-items-center justify-content-between d-flex">
			      <div id="logo">
			        <a href="index.html"><img src="{$render.themePath}img/logo.png" alt="" title="" /></a>
			      </div>
			      <nav id="nav-menu-container">
			        <ul class="nav-menu">
					
					{foreach from=$render.template.Menu key=key item=item}
						
						<li{if $item.subpages != ""} class="menu-has-children" {/if}>
						<a href="{$render.sitePath}{$item.url}" title="{$item.title}" {if $key == $CP} class="active" {/if}>{$item.label}</a>
						{if $item.subpages != ""}
							<ul>
							
							{foreach from=$item.subpages key=subpage_key item=subpage_item}
							
								<li>
						<a href="{$render.sitePath}{$subpage_item.url}" title="{$subpage_item.title}" {if $subpage_key == $CP} class="active" {/if}>{$subpage_item.label}</a>
						
								</li>
							
							{/foreach}
							
							</ul>
						{/if}
						</li>
						
					{/foreach}
					  
			        </ul>
			      </nav><!-- #nav-menu-container -->		    		
		    	</div>
		    </div>
		  </header><!-- #header -->
	
