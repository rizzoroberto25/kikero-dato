			{include file="header.tpl"}
			<!-- start banner Area -->
			<section class="about-banner">
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								{$render.Metas.title}				
							</h1>	
							
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->	
				<!-- Start portfolio-area Area -->
            <section class="portfolio-area section-gap" id="portfolio">
                <div class="container">
		            <div class="row d-flex justify-content-center">
		                <div class="menu-content pb-70 col-lg-8">
		                    <div class="title text-center">
		                        <h2 class="mb-10">{$render.contents.sottotitolo}</h2>
		                        <p>{$render.contents.paragrafo}</p>
		                    </div>
		                </div>
		            </div>

                    
                    <div class="filters-content">
                        <div class="row grid">
						{foreach from=$render.contents.utenti item=utente}
														
							
							<div class="single-portfolio col-sm-4 all vector">
                            	<div class="relative">
	                            	<div class="thumb">
	                            		<div class="overlay overlay-bg"></div>
	                            		 <img class="image img-fluid" src="{$render.sitePath}img/users/{$utente.id}.jpg" alt="">
	                            	</div>
									<a href="{$render.sitePath}img/users/{$utente.id}.jpg" class="img-pop-up">	
									  <div class="middle">
									    <div class="text align-self-center d-flex"><img src="{$render.themePath}img/preview.png" alt=""></div>
									  </div>
								</a>                              		
                            	</div>
								<div class="p-inner">
								    <h4>{$utente.nome} {$utente.cognome}</h4>
									<div class="cat">{$utente.ruolo}</div>
								</div>					                               
                            </div>
						{/foreach}
                            
                            
                        </div>
                    </div>
                    
                </div>
            </section>
            <!-- End portfolio-area Area -->
			
			{include file="footer.tpl"}