<?php
include("config.php");
$CP = "user";
$Render = new RENDER();
$metas = array(
	"language" => $_SESSION['lingua'],
	"title" => "Dati Utente",
);
$Render->setMetas($metas);

$corpo = array(
	"titolo" => "Titolo della pagina",
	"testo" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
);

$Render->pushRender("corpo", $corpo);
$Render->getRender($CP);

