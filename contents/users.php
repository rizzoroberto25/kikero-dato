<?php
if(!defined("ROOT")) exit();
use Core\Classi\Controllers\Utente;
use Core\Classi\UTILITY;

$Utente = new UTENTE();
$utenti = $Utente->utenti_attivi();

switch($_SESSION['lingua']) {
	
	case "en":
		
		$pageMetas = array(
			"title" => "Registered Users",
			"language" => "en",
		);
		$contents = array(
			"sottotitolo" => "List of active users",
			"paragrafo" => "Prendere nota degli utenti registrati e attivi. Nella lista è possibile visualizzare anche il ruolo degli utenti",
		);

		break;
		
	default:
		$pageMetas = array(
			"title" => "Utenti registrati",
			"language" => "it",
		);
		$contents = array(
			"sottotitolo" => "Lista degli utenti attivi",
			"paragrafo" => "Take note of registered and active users. The list of users can also be seen in the list",
		);
		break;
	
}

$contents["utenti"] = $utenti;